from dataclasses import dataclass, field
from typing import Generator, List

import netsquid as ns
from pydynaa import EventExpression

from netsquid_netbuilder.run import Program


@dataclass
class ClassicalMessageEventInfo:
    time: float
    peer: str
    msg: str


@dataclass
class ClassicalMessageEventRegistration:
    sent: List[ClassicalMessageEventInfo] = field(default_factory=list)
    received: List[ClassicalMessageEventInfo] = field(default_factory=list)


class ClassicalSenderProtocol(Program):
    def __init__(
        self,
        peer_name: str,
        event_reg: ClassicalMessageEventRegistration,
        messages: List[str],
        send_times: List[float],
    ):
        """
        Protocol that sends classical messages to its peer at the given times.

        :param peer_name: Name of the peer node.
        :param event_reg: Object to register events to.
        :param messages: List of messages to send.
        :param send_times: List of times to send the messages.
         This must in ascending order and equal size to `messages`.
        """
        super().__init__()
        assert len(messages) == len(send_times)
        self.peer_name = peer_name
        self.event_reg = event_reg
        self.messages = messages
        self.send_times = send_times

    def run(self) -> Generator[EventExpression, None, None]:
        socket = self.context.sockets[self.peer_name]

        for send_time, message in zip(self.send_times, self.messages):
            yield self.await_timer(end_time=send_time)
            socket.send(message)

            # Register sending the message
            self.event_reg.sent.append(
                ClassicalMessageEventInfo(ns.sim_time(), self.peer_name, message)
            )


class ClassicalReceiverProtocol(Program):
    def __init__(
        self,
        peer_name: str,
        event_reg: ClassicalMessageEventRegistration,
        listen_delay: float = 0,
    ):
        """
        Protocol that indefinitely waits to receive messages from its peer.

        :param peer_name: The peer node that it will receive messages from.
        :param event_reg: Object to register events to.
        :param listen_delay: Offset before this protocol starts waiting for messages arrival events.
        """
        super().__init__()
        self.peer_name = peer_name
        self.event_reg = event_reg
        self.listen_delay = listen_delay

    def run(self) -> Generator[EventExpression, None, None]:
        socket = self.context.sockets[self.peer_name]
        if self.listen_delay > 0:
            yield self.await_timer(end_time=self.listen_delay)

        while True:
            message = yield from socket.recv()

            # Register receive message event
            self.event_reg.received.append(
                ClassicalMessageEventInfo(ns.sim_time(), self.peer_name, message)
            )
