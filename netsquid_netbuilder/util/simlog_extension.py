import logging
from typing import Dict, Iterable, List, Union

import netsquid as ns

MAGNITUDE_CONVERSION = {
    "ps": ns.PICOSECOND,
    "ns": ns.NANOSECOND,
    "μs": ns.MICROSECOND,
    "ms": ns.MILLISECOND,
    "s": ns.SECOND,
}


class _SimTimeFilter(logging.Filter):
    def __init__(self, magnitude: float, precision: int = None):
        """
        Object to add the simulation time, in specified magnitude and precision, to LogRecord.

        :param magnitude: The magnitude of the simulated time.
        :param precision: The precision for rounding the simulated time (default is None).
        """
        super().__init__()
        self.magnitude = magnitude
        self.precision = precision

    def filter(self, record: logging.LogRecord):
        record.simtime = ns.sim_time(self.magnitude)
        if self.precision:
            record.simtime = round(record.simtime, self.precision)
        return True


def get_netsquid_loggers() -> Dict[str, logging.Logger]:
    """Retrieve all module loggers for netsquid and its extensions.

    .. note::
        This method requires that the extensions contain 'netsquid' in their name.

    :return: All snippet loggers for the netsquid package.
    """
    loggers = {}
    for name, logger in logging.root.manager.loggerDict.items():
        if "netsquid" in name.split(".")[0]:
            loggers[name] = logger

    return loggers


def get_netsquid_root_loggers() -> Dict[str, logging.Logger]:
    """Retrieve all root module loggers for netsquid and its extensions.

    :return: All registered root module loggers for extensions of the netsquid package.
    """
    loggers = {}

    for name, logger in logging.root.manager.loggerDict.items():
        if "netsquid" in name.split(".")[0] and len(name.split(".")) == 1:
            loggers[name] = logger

    return loggers


def add_sim_time_console_logging(
    loggers: Iterable[logging.Logger], magnitude="ns", precision: int = None
):
    """
    Add logging with simulation time included in the log with specified magnitude and precision to the given loggers.

    .. note::
        This method does not replace existing logging. To clear any existing logging, use the
        :meth:`netsquid_netbuilder.util.simlog_extension.clear_logging` method first.

    :param loggers: The loggers that will be affected.
    :param magnitude: The magnitude of the simulated time. Options include "ps" (picoseconds), "ns" (nanoseconds),
     "μs" (microseconds), "ms" (milliseconds), and "s" (seconds) (default is "ns").
    :param precision: The precision for rounding the simulated time (default is None).
    :raises KeyError: If the magnitude is not valid.
    """
    if magnitude not in MAGNITUDE_CONVERSION.keys():
        raise KeyError(f"Invalid magnitude: {magnitude}")

    for logger in loggers:
        formatter = logging.Formatter(f"%(levelname)s:%(simtime)s {magnitude}:%(name)s:%(message)s")
        syslog = logging.StreamHandler()
        syslog.setFormatter(formatter)
        syslog.addFilter(_SimTimeFilter(MAGNITUDE_CONVERSION[magnitude], precision))
        logger.addHandler(syslog)


def set_log_level(loggers: Iterable[logging.Logger], level: Union[int, str]):
    """
    Sets the log level of the given loggers to the specified level.

    :param loggers: The loggers that will be affected.
    :param level: Sets the threshold for the loggers to level.
    """
    for logger in loggers:
        logger.setLevel(level)


def set_propagate_value(loggers: Iterable[logging.Logger], propagate_value: bool):
    """
    Enable or disable propagation of the given loggers.

    :param loggers: The loggers that will be affected.
    :param propagate_value: Change the attribute `propagate`. This determines if events logged to loggers
     will be passed to the handlers of higher level (ancestor) loggers
    """
    for logger in loggers:
        logger.propagate = bool(propagate_value)


def clear_logging(loggers: Iterable[logging.Logger]):
    """Clears any existing logging for the given loggers.

    :param loggers: The loggers that will be affected.
    """
    for logger in loggers:
        logger.handlers = []


def add_sim_time_file_logging(
    loggers: Iterable[logging.Logger], path: str, magnitude: str = "ns", precision: int = None
):
    """Sets up sending the logs to an output file for the given loggers.

    :param loggers: The loggers that will be affected.
    :param path: Location of the output file. This will overwrite an existing file.
    :param magnitude: The magnitude of the simulated time. Options include "ps" (picoseconds), "ns" (nanoseconds),
     "μs" (microseconds), "ms" (milliseconds), and "s" (seconds) (default is "ns").
    :param precision: The precision for rounding the simulated time (default is None).
    """
    if magnitude not in MAGNITUDE_CONVERSION.keys():
        raise KeyError(f"Invalid magnitude: {magnitude}")

    file_handler = logging.FileHandler(path, mode="w")
    formatter = logging.Formatter(f"%(levelname)s:%(simtime)s {magnitude}:%(name)s:%(message)s")
    file_handler.setFormatter(formatter)
    file_handler.addFilter(_SimTimeFilter(MAGNITUDE_CONVERSION[magnitude], precision))

    for logger in loggers:
        logger.addHandler(file_handler)


def setup_default_console_logging(
    level=logging.INFO,
    magnitude="ns",
    precision: int = None,
    additional_loggers: List[logging.Logger] = None,
):
    """
    Setup console logging with sim time console logging to level for all packages with netsquid in their name,
    except for netsquid and the classical routing service, these are set to log level WARNING.

    .. note::
        Netsquid and classical routing service are set to WARNING,
        because at INFO and DEBUG logging they produce relatively large number of log messages that describe low level
        events that are part of a higher level event that is, most of the time, being logged in another module.

    :param level: Sets the threshold for the loggers to level.
    :param magnitude: The magnitude of the simulated time. Options include "ps" (picoseconds), "ns" (nanoseconds),
     "μs" (microseconds), "ms" (milliseconds), and "s" (seconds) (default is "ns").
    :param precision: The precision for rounding the simulated time (default is None).
    :param additional_loggers: Any extra loggers that do not have `netsquid` in their names.
    """
    loggers = get_netsquid_root_loggers()
    logger_list = list(loggers.values())
    if additional_loggers:
        logger_list += additional_loggers

    set_propagate_value(logger_list, propagate_value=False)
    clear_logging(logger_list)
    set_log_level(logger_list, level)
    add_sim_time_console_logging(logger_list, magnitude=magnitude, precision=precision)

    classical_msg_logger = logging.root.manager.loggerDict[
        "netsquid_driver.classical_routing_service"
    ]
    classical_msg_logger.setLevel(logging.WARNING)
    loggers["netsquid"].setLevel(logging.WARNING)
