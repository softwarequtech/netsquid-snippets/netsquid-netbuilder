import numpy
from netsquid.qubits import qubitapi as qapi
from netsquid.qubits.ketstates import bell_states


def calculate_fidelity_epr(dm: numpy.ndarray, bell_index: int) -> float:
    """Calculates the fidelity of a two qubit state density matrix relative to the bell state matching the bell index

    :param dm: The two qubit state density matrix.
    :param bell_index: The bell index to compare the state to.
    :return: Fidelity of the density matrix state compared to the reference state.
    """
    ref_qubit = qapi.create_qubits(2, "reference")
    qapi.assign_qstate(ref_qubit, dm)
    fid = qapi.fidelity(ref_qubit, bell_states[bell_index], squared=True)
    return fid


def fidelity_to_prob_max_mixed(fid: float) -> float:
    """Calculate the probability of a state being maximally mixed given
    the fidelity relative to a state of two perfectly entangled qubits.
    Only applicable in two qubit systems and if the only noise is depolarising noise.
    """
    return (1 - fid) * 4.0 / 3.0


def prob_max_mixed_to_fidelity(prob: float) -> float:
    """Calculate the fidelity, relative to a state of two perfectly entangled qubits,
    given the probability of the state being maximally mixed.
    Only applicable in two qubit systems and if the only noise is depolarising noise.
    """
    return 1 - prob * 3.0 / 4.0
