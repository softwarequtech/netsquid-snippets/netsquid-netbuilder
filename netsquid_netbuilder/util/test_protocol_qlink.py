from dataclasses import dataclass, field
from typing import Generator, List

import netsquid as ns
import numpy as np
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService
from pydynaa import EventExpression
from qlink_interface import (
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqReceive,
    ResCreateAndKeep,
    ResMeasureDirectly,
)

from netsquid_netbuilder.run import Program
from netsquid_netbuilder.util.test_protocol_clink import ClassicalMessageEventInfo


@dataclass
class CreateAndKeepReceiveEventInfo:
    time: float
    node_name: str
    peer_name: str
    result: ResCreateAndKeep
    dm: np.ndarray


@dataclass
class CreateAndKeepSubmitEventInfo:
    time: float
    node_name: str
    peer_name: str
    request: ReqCreateAndKeep
    create_id: int


@dataclass
class CreateAndKeepEventRegistration:
    received_classical: List[ClassicalMessageEventInfo] = field(default_factory=list)
    received_ck: List[CreateAndKeepReceiveEventInfo] = field(default_factory=list)
    submitted_ck: List[CreateAndKeepSubmitEventInfo] = field(default_factory=list)


class CreateAndKeepSenderProtocol(Program):
    def __init__(
        self,
        peer_name: str,
        event_reg: CreateAndKeepEventRegistration,
        n_epr: int = 1,
        minimum_fidelity=0,
    ):
        """
        Protocol to generate EPR pairs between itself and its peer.
        This protocol will create and send the requests for entanglement generation to the EGP.

        :param peer_name: The peer node that it will generate EPR pairs with.
        :param event_reg: Object to register events to.
        :param n_epr: Number of EPR pairs to generate.
        :param minimum_fidelity: Minimum fidelity to request in the entanglement generation request.
        """
        super().__init__()
        self.peer_name = peer_name
        self.event_reg = event_reg
        self.n_epr = n_epr
        self.minimum_fidelity = minimum_fidelity

    def run(self) -> Generator[EventExpression, None, None]:
        node = self.context.node
        socket = self.context.sockets[self.peer_name]
        egp = self.context.egp[self.peer_name]

        # Wait for classical message in order to delay the egp request to peer
        message = yield from socket.recv()
        self.event_reg.received_classical.append(
            ClassicalMessageEventInfo(ns.sim_time(), self.peer_name, message)
        )

        for _ in range(self.n_epr):
            # create request
            request = ReqCreateAndKeep(
                remote_node_id=self.context.node_id_mapping[self.peer_name],
                number=1,
                minimum_fidelity=self.minimum_fidelity,
            )
            create_id = egp.create_and_keep(request)

            self.event_reg.submitted_ck.append(
                CreateAndKeepSubmitEventInfo(
                    ns.sim_time(), node.name, self.peer_name, request, create_id
                )
            )

            # Await request completion
            yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
            response: ResCreateAndKeep = egp.get_signal_result(
                label=ResCreateAndKeep.__name__, receiver=self
            )

            # register event and qubit density matrix
            qubit_mem_pos = response.logical_qubit_id
            qubit = node.qdevice.peek(positions=qubit_mem_pos)[0]
            qubit_dm = ns.qubits.qubitapi.reduced_dm(qubit.qstate.qubits)
            self.event_reg.received_ck.append(
                CreateAndKeepReceiveEventInfo(
                    ns.sim_time(), node.name, self.peer_name, response, qubit_dm
                )
            )

            # Free the qubit and inform EntanglementTrackerService of the discard.
            node.qdevice.discard(qubit_mem_pos)
            node.driver[EntanglementTrackerService].register_local_discard_mem_pos(qubit_mem_pos)


class CreateAndKeepReceiveProtocol(Program):
    def __init__(self, peer_name: str, event_reg: CreateAndKeepEventRegistration, n_epr: int = 1):
        """
        Protocol to generate EPR pairs between itself and its peer.
        This protocol will receive the requests for entanglement generation.

        :param peer_name: The peer node that it will generate EPR pairs with.
        :param event_reg: Object to register events to.
        :param n_epr: Number of EPR pairs to generate.
        """
        super().__init__()
        self.peer_name = peer_name
        self.event_reg = event_reg
        self.n_epr = n_epr

    def run(self) -> Generator[EventExpression, None, None]:
        node = self.context.node
        socket = self.context.sockets[self.peer_name]
        egp = self.context.egp[self.peer_name]

        msg = "test_msg"
        socket.send(msg)

        egp.put(ReqReceive(remote_node_id=self.context.node_id_mapping[self.peer_name]))

        for _ in range(self.n_epr):
            # Wait for a signal from the EGP.
            yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
            response = egp.get_signal_result(label=ResCreateAndKeep.__name__, receiver=self)

            # register event and qubit density matrix
            qubit_mem_pos = response.logical_qubit_id
            qubit = node.qdevice.peek(positions=qubit_mem_pos)[0]
            qubit_dm = ns.qubits.qubitapi.reduced_dm(qubit.qstate.qubits)
            self.event_reg.received_ck.append(
                CreateAndKeepReceiveEventInfo(
                    ns.sim_time(), node.name, self.peer_name, response, qubit_dm
                )
            )

            # Free the qubit and inform EntanglementTrackerService of the discard.
            node.qdevice.discard(qubit_mem_pos)
            node.driver[EntanglementTrackerService].register_local_discard_mem_pos(qubit_mem_pos)


@dataclass
class MeasureDirectlyReceiveEventInfo:
    time: float
    node_name: str
    peer_name: str
    result: ResMeasureDirectly


@dataclass
class MeasureDirectlySubmitEventInfo:
    time: float
    node_name: str
    peer_name: str
    request: ReqMeasureDirectly
    create_id: int


@dataclass
class MeasureDirectlyEventRegistration:
    received_classical: List[ClassicalMessageEventInfo] = field(default_factory=list)
    received_md: List[MeasureDirectlyReceiveEventInfo] = field(default_factory=list)
    submitted_md: List[MeasureDirectlySubmitEventInfo] = field(default_factory=list)


class MeasureDirectlySenderProtocol(Program):
    def __init__(
        self,
        peer_name: str,
        event_reg: MeasureDirectlyEventRegistration,
        n_epr: int = 1,
        minimum_fidelity=0,
    ):
        """
        Protocol to generate EPR pairs that are measured directly between itself and its peer.
        This protocol will create and send the requests for entanglement generation to the EGP.

        :param peer_name: The peer node that it will generate EPR pairs with.
        :param event_reg: Object to register events to.
        :param n_epr: Number of EPR pairs to generate.
        :param minimum_fidelity: Minimum fidelity to request in the entanglement generation request.
        """
        super().__init__()
        self.peer_name = peer_name
        self.event_reg = event_reg
        self.n_epr = n_epr
        self.minimum_fidelity = minimum_fidelity

    def run(self) -> Generator[EventExpression, None, None]:
        node = self.context.node
        socket = self.context.sockets[self.peer_name]
        egp = self.context.egp[self.peer_name]

        # Wait for classical message in order to delay the egp request to peer
        message = yield from socket.recv()
        self.event_reg.received_classical.append(
            ClassicalMessageEventInfo(ns.sim_time(), self.peer_name, message)
        )

        for _ in range(self.n_epr):
            # create request
            request = ReqMeasureDirectly(
                remote_node_id=self.context.node_id_mapping[self.peer_name],
                number=1,
                minimum_fidelity=self.minimum_fidelity,
            )
            create_id = egp.measure_directly(request)

            self.event_reg.submitted_md.append(
                MeasureDirectlySubmitEventInfo(
                    ns.sim_time(), node.name, self.peer_name, request, create_id
                )
            )

            # Await request completion
            yield self.await_signal(sender=egp, signal_label=ResMeasureDirectly.__name__)
            response: ResMeasureDirectly = egp.get_signal_result(
                label=ResMeasureDirectly.__name__, receiver=self
            )

            # register event
            self.event_reg.received_md.append(
                MeasureDirectlyReceiveEventInfo(ns.sim_time(), node.name, self.peer_name, response)
            )


class MeasureDirectlyReceiveProtocol(Program):
    def __init__(self, peer_name: str, event_reg: MeasureDirectlyEventRegistration, n_epr: int = 1):
        """
        Protocol to generate EPR pairs that are measured directly between itself and its peer.
        This protocol will receive the requests for entanglement generation.

        :param peer_name: The peer node that it will generate EPR pairs with.
        :param event_reg: Object to register events to.
        :param n_epr: Number of EPR pairs to generate.
        """
        super().__init__()
        self.peer_name = peer_name
        self.event_reg = event_reg
        self.n_epr = n_epr

    def run(self) -> Generator[EventExpression, None, None]:
        node = self.context.node
        socket = self.context.sockets[self.peer_name]
        egp = self.context.egp[self.peer_name]

        msg = "test_msg"
        socket.send(msg)

        egp.put(ReqReceive(remote_node_id=self.context.node_id_mapping[self.peer_name]))

        for _ in range(self.n_epr):
            # Wait for a signal from the EGP.
            yield self.await_signal(sender=egp, signal_label=ResMeasureDirectly.__name__)
            response: ResMeasureDirectly = egp.get_signal_result(
                label=ResMeasureDirectly.__name__, receiver=self
            )

            # register event
            self.event_reg.received_md.append(
                MeasureDirectlyReceiveEventInfo(ns.sim_time(), node.name, self.peer_name, response)
            )
