from dataclasses import dataclass
from typing import Generator, List

import netsquid as ns
from netsquid import BellIndex
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService
from pydynaa import EventExpression
from qlink_interface import ReqCreateAndKeep, ReqReceive, ResCreateAndKeep

from netsquid_netbuilder.run import Program, ProgramContext


@dataclass
class SchedulerRequest:
    submit_time: float
    sender_name: str
    receiver_name: str
    number: int = 1


@dataclass
class SchedulerCompletionEventInfo:
    node_1: str
    node_2: str
    completion_time: float
    epr_measure_result: int
    response: ResCreateAndKeep


@dataclass
class SchedulerSubmissionEventInfo:
    node_1: str
    node_2: str
    submission_time: float
    create_id: int
    submission_req: ReqCreateAndKeep


class SchedulerEventRegistration:
    def __init__(self):
        self.completions: List[SchedulerCompletionEventInfo] = []
        self.submissions: List[SchedulerSubmissionEventInfo] = []


class SchedulerTestProtocol(Program):
    def __init__(self, event_reg: SchedulerEventRegistration, requests: List[SchedulerRequest]):
        """
        Protocol to test the scheduler. This protocol will accept any entanglement attempts by a third party,
        start entanglement attempts as specified via `requests` and register the completion event in `event_reg`.

        :param event_reg:  Object to register events to.
        :param requests: The requests to start. Must be in ascending order of `submit_time`.
        """
        super().__init__()
        self.event_reg = event_reg
        self.requests = requests
        self.sender_subprotocol = self.EGPSenderSubprotocol(event_reg, requests)
        self.egp_listeners: List[SchedulerTestProtocol.EGPListenerSubprotocol] = []

    def set_context(self, context: ProgramContext):
        super().set_context(context)
        self.setup_egp_listeners()
        self.sender_subprotocol.set_context(context)
        for egp_listener in self.egp_listeners:
            egp_listener.set_context(context)

    def start(self):
        super().start()
        self.sender_subprotocol.start()
        self._enable_receiving_from_all_nodes()
        for egp_listener in self.egp_listeners:
            egp_listener.start()

    def stop(self):
        super().stop()
        self.sender_subprotocol.stop()
        for egp_listener in self.egp_listeners:
            egp_listener.stop()

    def setup_egp_listeners(self):
        for remote_node_name, _ in self.context.egp.items():
            egp_listener = self.EGPListenerSubprotocol(self.event_reg, remote_node_name)
            self.egp_listeners.append(egp_listener)

    def _enable_receiving_from_all_nodes(self):
        for peer, egp in self.context.egp.items():
            request = ReqReceive(remote_node_id=self.context.node_id_mapping[peer])
            egp.put(request)

    class EGPSenderSubprotocol(Program):
        def __init__(self, event_reg: SchedulerEventRegistration, requests: List[SchedulerRequest]):
            """
            Subprotocol that will send the EGP requests for entanglement generation at the time indicated
            by the `submit_time` of the ``SchedulerRequest`` object.

            :param event_reg: Object to register events to.
            :param requests: The requests to send to the EGP. Must be in ascending order of `submit_time`.
            """
            super().__init__()
            self.event_reg = event_reg
            self.requests = requests

        def run(self) -> Generator[EventExpression, None, None]:
            for request in self.requests:
                # Wait until request submit time,
                yield self.await_timer(end_time=request.submit_time)

                # Create and send the request for entanglement generation
                peer = request.receiver_name
                egp = self.context.egp[peer]
                request = ReqCreateAndKeep(
                    remote_node_id=self.context.node_id_mapping[peer], number=request.number
                )
                create_id = egp.create_and_keep(request)

                self.event_reg.submissions.append(
                    SchedulerSubmissionEventInfo(
                        self.context.node.name, peer, ns.sim_time(), create_id, request
                    )
                )

    class EGPListenerSubprotocol(Program):
        def __init__(self, event_reg: SchedulerEventRegistration, remote_node_name: str):
            """
            Protocol that listens for events that entanglement has been established with `remote_node_name` and
            registers these events to `event_reg`.

            :param event_reg: Object to register events to.
            :param remote_node_name: Name of the node that this protocols listens to for successful entanglement.
            """
            super().__init__()
            self.remote_node_name = remote_node_name
            self.result_reg = event_reg

        def run(self):
            node = self.context.node
            egp = self.context.egp[self.remote_node_name]
            remote_node_id = self.context.node_id_mapping[self.remote_node_name]

            while True:
                yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
                response: ResCreateAndKeep = egp.get_signal_result(
                    label=ResCreateAndKeep.__name__, receiver=self
                )
                if response.remote_node_id is not remote_node_id:
                    continue

                qubit_mem_pos = response.logical_qubit_id
                measure_result = node.qdevice.measure(positions=qubit_mem_pos)[0][0]
                node.qdevice.discard(qubit_mem_pos)
                node.driver[EntanglementTrackerService].register_local_discard_mem_pos(
                    qubit_mem_pos
                )

                # Correct the measurement result based on bell state index
                if response.directionality_flag and response.bell_state not in [
                    BellIndex.PHI_PLUS,
                    BellIndex.PHI_MINUS,
                ]:
                    measure_result = int(not measure_result)

                result = SchedulerCompletionEventInfo(
                    node_1=node.name,
                    node_2=self.remote_node_name,
                    completion_time=ns.sim_time(),
                    epr_measure_result=measure_result,
                    response=response,
                )

                self.result_reg.completions.append(result)
