from __future__ import annotations

import itertools
from copy import deepcopy
from typing import List, Union

from netsquid_netbuilder.modules.clinks.default import DefaultCLinkConfig
from netsquid_netbuilder.modules.clinks.instant import InstantCLinkConfig
from netsquid_netbuilder.modules.clinks.interface import ICLinkConfig
from netsquid_netbuilder.modules.long_distance_interface.interface import (
    ILongDistanceInterfaceConfig,
)
from netsquid_netbuilder.modules.qdevices.generic import GenericQDeviceConfig
from netsquid_netbuilder.modules.qdevices.interface import IQDeviceConfig
from netsquid_netbuilder.modules.qlinks.depolarise import DepolariseQLinkConfig
from netsquid_netbuilder.modules.qlinks.interface import IQLinkConfig
from netsquid_netbuilder.modules.qrep_chain_control.interface import IQRepChainControlConfig
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_builder import (
    SwapASAPConfig,
)
from netsquid_netbuilder.modules.scheduler.fifo import FIFOScheduleConfig
from netsquid_netbuilder.modules.scheduler.interface import IScheduleConfig
from netsquid_netbuilder.network_config import (
    CLinkConfig,
    MetroHubConfig,
    MetroHubConnectionConfig,
    NetworkConfig,
    ProcessingNodeConfig,
    QLinkConfig,
    RepeaterChainConfig,
    RepeaterNodeConfig,
)


def create_single_node_network(qdevice_typ: str, qdevice_cfg: IQDeviceConfig) -> NetworkConfig:
    """
    Create a network configuration with a single processing node.

    This function creates a network configuration with a single processing node
    named "Alice" and the specified quantum device type and configuration.

    :param qdevice_typ: Type of the quantum device.
    :param qdevice_cfg: Configuration of the quantum device.

    :return: Network configuration with a single processing node.
    """
    network_config = NetworkConfig(processing_nodes=[], qlinks=[], clinks=[])

    node = ProcessingNodeConfig(
        name="Alice", qdevice_typ=qdevice_typ, qdevice_cfg=deepcopy(qdevice_cfg)
    )
    network_config.processing_nodes.append(node)

    return network_config


def create_2_node_network(
    qlink_typ: str,
    qlink_cfg: IQLinkConfig,
    clink_typ: str = "instant",
    clink_cfg: ICLinkConfig = None,
    qdevice_typ: str = "generic",
    qdevice_cfg: IQDeviceConfig = None,
) -> NetworkConfig:
    """
    Create a network configuration with two processing nodes connected by a quantum and classical link.

    This function creates a network configuration with two processing nodes named "Alice" and "Bob".
    The nodes are connected by the specified link type and configuration.

    :param qlink_typ: Type of the qlink connecting the nodes.
    :param qlink_cfg: Configuration of the qlink connecting the nodes.
    :param clink_typ: Type of the classical communication link between the nodes (default is "instant").
    :param clink_cfg: Configuration of the classical communication link. (optional if default `typ` is used)
    :param qdevice_typ: Type of the quantum device for each node (default is "generic").
    :param qdevice_cfg: Configuration of the quantum device for each node. (optional if default `typ` is used)

    :return: Network configuration with two processing nodes connected by a quantum and classical link.
    """
    node_names = ["Alice", "Bob"]
    return create_complete_graph_network(
        node_names, qlink_typ, qlink_cfg, clink_typ, clink_cfg, qdevice_typ, qdevice_cfg
    )


def create_complete_graph_network(
    node_names: List[str],
    qlink_typ: str,
    qlink_cfg: IQLinkConfig,
    clink_typ: str = "instant",
    clink_cfg: ICLinkConfig = None,
    qdevice_typ: str = "generic",
    qdevice_cfg: IQDeviceConfig = None,
) -> NetworkConfig:
    """
    Create a complete graph network configuration.

    This function creates a network configuration with nodes connected in a complete graph topology,
    where each node is connected to every other node with both quantum links and classical links.

    :param node_names: List of str with the names of the nodes. The amount of names will determine the amount of nodes.
    :param qlink_typ: str specification of the qlink model to use for quantum links.
    :param qlink_cfg: Configuration of the qlink model.
    :param clink_typ: str specification of the clink model to use for classical communication. (default is "instant")
    :param clink_cfg: Configuration of the clink model. (optional if default `typ` is used)
    :param qdevice_typ: str specification of the qdevice model to use for quantum devices (default is "generic").
    :param qdevice_cfg: Configuration of qdevice. (optional if default `typ` is used)
    :return: StackNetworkConfig object with a complete graph network.
    """
    network_config = NetworkConfig(processing_nodes=[], qlinks=[], clinks=[])

    assert len(node_names) > 0

    for node_name in node_names:
        qdevice_cfg = GenericQDeviceConfig.perfect_config() if qdevice_cfg is None else qdevice_cfg
        node = ProcessingNodeConfig(
            name=node_name, qdevice_typ=qdevice_typ, qdevice_cfg=deepcopy(qdevice_cfg)
        )
        network_config.processing_nodes.append(node)

    for s1, s2 in itertools.combinations(node_names, 2):
        qlink = QLinkConfig(node1=s1, node2=s2, typ=qlink_typ, cfg=deepcopy(qlink_cfg))
        network_config.qlinks.append(qlink)

        clink = CLinkConfig(node1=s1, node2=s2, typ=clink_typ, cfg=deepcopy(clink_cfg))
        network_config.clinks.append(clink)

    return network_config


def create_complete_graph_network_simplified(
    node_names: List[str],
    qlink_noise: float = 0,
    qdevice_noise: float = 0,
    qdevice_depolar_time: float = 0,
    qdevice_op_time: float = 0,
    clink_delay: float = 0.0,
    link_delay: float = 0.0,
) -> NetworkConfig:
    """
    Create a complete graph network configuration.
    This is a simplified version of the method of :meth:`create_complete_graph_network` that
    has made a selection of model types to use and exposes the most important parameters of these models.

    This function creates a network configuration with nodes connected in a complete graph topology,
    where each node is connected to every other node with both quantum links and classical links.

    :param node_names: List of str with the names of the nodes. The amount of names will determine the amount of nodes.
    :param qlink_noise: A number between 0 and 1 that indicates how noisy the generated EPR pairs are.
    :param qdevice_noise: A number between 0 and 1 that indicates how noisy the qubit operations on the nodes are.
    :param qdevice_depolar_time: The timescale [ns] for depolarising noise for qubits on the memory.
    :param qdevice_op_time: The time [ns] for various operations, such as gates, measurements and qubit initialization.
    :param clink_delay: The time [ns] it takes for the classical message to arrive.
    :param link_delay: The time [ns] it takes for an EPR pair to be generated.
    :return: StackNetworkConfig object with a complete graph network.
    """

    qdevice_cfg = GenericQDeviceConfig.perfect_config()

    qdevice_cfg.two_qubit_gate_depolar_prob = qdevice_noise
    qdevice_cfg.single_qubit_gate_depolar_prob = qdevice_noise

    qdevice_cfg.T1 = qdevice_depolar_time
    qdevice_cfg.T2 = qdevice_depolar_time

    qdevice_cfg.measure_time = qdevice_op_time
    qdevice_cfg.init_time = qdevice_op_time
    qdevice_cfg.single_qubit_gate_time = qdevice_op_time
    qdevice_cfg.two_qubit_gate_time = qdevice_op_time

    qlink_cfg = DepolariseQLinkConfig(
        fidelity=1 - qlink_noise * 3 / 4, t_cycle=link_delay, prob_success=1
    )

    clink_cfg = DefaultCLinkConfig(delay=clink_delay)

    return create_complete_graph_network(
        node_names,
        qlink_typ="depolarise",
        qlink_cfg=qlink_cfg,
        clink_typ="default",
        clink_cfg=clink_cfg,
        qdevice_typ="generic",
        qdevice_cfg=qdevice_cfg,
    )


def create_metro_hub_network(
    node_names: List[str],
    node_distances: Union[float, List[float]],
    qlink_typ: str,
    qlink_cfg: IQLinkConfig,
    schedule_typ: str = "fifo",
    schedule_cfg: IScheduleConfig = None,
    clink_typ: str = "instant",
    clink_cfg: ICLinkConfig = None,
    qdevice_typ: str = "generic",
    qdevice_cfg: IQDeviceConfig = None,
) -> NetworkConfig:
    """Create a star type network with a metro hub in the center.

    The metropolitan hub serves as a central node with links connecting it to each of the end-nodes.
    Each end-node is connected to the hub through classical links
    and has quantum links passing through and facilitated by the hub.

    :param node_names: List of str with the names of the nodes. The amount of names will determine the amount of nodes.
    :param node_distances: List of float or float with distances for each end-node to the central hub.
    :param qlink_typ: str specification of the qlink model to use for quantum links.
    :param qlink_cfg: Configuration of the qlink model.
    :param schedule_typ: str specification of the schedule model to use for the metro hub (default is "fifo").
    :param schedule_cfg: Configuration of the schedule to use. (optional if default `typ` is used)
    :param clink_typ: str specification of the clink model to use for classical communication (default is "instant").
    :param clink_cfg: Configuration of the clink model. (optional if default `typ` is used)
    :param qdevice_typ: str specification of the qdevice model to use for quantum devices (default is "generic").
    :param qdevice_cfg: Configuration of qdevice. (optional if default `typ` is used)
    :return: StackNetworkConfig object with a metro hub network.
    """
    network_config = NetworkConfig(processing_nodes=[], qlinks=[], clinks=[])
    clink_cfg = InstantCLinkConfig() if clink_cfg is None else clink_cfg

    for node_name in node_names:
        qdevice_cfg = GenericQDeviceConfig.perfect_config() if qdevice_cfg is None else qdevice_cfg
        node = ProcessingNodeConfig(
            name=node_name, qdevice_typ=qdevice_typ, qdevice_cfg=deepcopy(qdevice_cfg)
        )
        network_config.processing_nodes.append(node)

    mh_connections = _connect_mh(node_distances, node_names)

    schedule_cfg = FIFOScheduleConfig() if schedule_cfg is None else schedule_cfg

    mh = MetroHubConfig(
        name="metro hub",
        connections=mh_connections,
        qlink_typ=qlink_typ,
        qlink_cfg=deepcopy(qlink_cfg),
        clink_typ=clink_typ,
        clink_cfg=deepcopy(clink_cfg),
        schedule_typ=schedule_typ,
        schedule_cfg=deepcopy(schedule_cfg),
    )
    network_config.hubs = [mh]

    return network_config


def _connect_mh(
    node_distances: Union[float, List[float]], node_names: List[str]
) -> List[MetroHubConnectionConfig]:
    """Utility method that creates a list of MetroHubConnectionConfig objects."""
    mh_connections = []
    if not isinstance(node_distances, list):
        node_distances = [node_distances] * len(node_names)
    assert len(node_names) == len(node_distances)

    for node_name, dist in zip(node_names, node_distances):
        mh_connections.append(MetroHubConnectionConfig(node=node_name, length=dist))

    return mh_connections


def create_two_connected_metro_hubs_network(
    hub1_node_names: List[str],
    hub1_node_distances: List[float],
    hub2_node_names: List[str],
    hub2_node_distances: List[float],
    repeater_chain_distances: List[float],
    qlink_typ: str,
    qlink_cfg: IQLinkConfig,
    schedule_typ: str = "fifo",
    schedule_cfg: IScheduleConfig = None,
    clink_typ: str = "instant",
    clink_cfg: ICLinkConfig = None,
    qdevice_typ: str = "generic",
    qdevice_cfg: IQDeviceConfig = None,
    long_distance_interface_typ: str = None,
    long_distance_interface_cfg: ILongDistanceInterfaceConfig = None,
    qrep_chain_control_typ: str = "swapASAP",
    qrep_chain_control_cfg: IQRepChainControlConfig = None,
) -> NetworkConfig:
    """
    Create a network configuration with two metropolitan hubs and a quantum repeater chain connecting them.

    This function creates a network configuration consisting of two metropolitan hubs connected
    by a quantum repeater chain. Each metropolitan hub connects to its respective end-nodes, and the
    quantum repeater chain connects the two hubs.

    :param hub1_node_names: A list of node names for the first metropolitan hub.
    :param hub1_node_distances: The distance for each end-node to the first metropolitan hub.
    :param hub2_node_names: A list of node names for the second metropolitan hub.
    :param hub2_node_distances: The distances for each end-node to the second metropolitan hub.
    :param repeater_chain_distances: The distances between nodes in the quantum repeater chain.
    :param qlink_typ: The type of qlink model to use for quantum links.
    :param qlink_cfg: The configuration of the qlink model.
    :param schedule_typ: The type of schedule model to use for the metropolitan hubs (default is "fifo").
    :param schedule_cfg: The configuration of the schedule to use. (optional if default `typ` is used)
    :param clink_typ: The type of clink model to use for classical communication (default is "instant").
    :param clink_cfg: The configuration of the clink model. (optional if default `typ` is used)
    :param qdevice_typ: The type of qdevice model to use for quantum devices (default is "generic").
    :param qdevice_cfg: The configuration of the qdevice. (optional if default `typ` is used)
    :param long_distance_interface_typ: The type of long-distance interface model to use. (optional)
    :param long_distance_interface_cfg: The configuration of the long-distance interface model. (optional)
    :param qrep_chain_control_typ: The type of quantum repeater chain control model to use (default is "swapASAP").
    :param qrep_chain_control_cfg: The configuration of the quantum repeater chain control model.
     (optional if default `typ` is used)
    :return: A NetworkConfig object representing of a network with two metropolitan hubs .
    """
    network_config = NetworkConfig(processing_nodes=[], qlinks=[], clinks=[])
    clink_cfg = InstantCLinkConfig() if clink_cfg is None else clink_cfg

    for node_name in hub1_node_names + hub2_node_names:
        qdevice_cfg = GenericQDeviceConfig.perfect_config() if qdevice_cfg is None else qdevice_cfg
        node = ProcessingNodeConfig(
            name=node_name, qdevice_typ=qdevice_typ, qdevice_cfg=deepcopy(qdevice_cfg)
        )
        network_config.processing_nodes.append(node)

    mh1_connections = _connect_mh(hub1_node_distances, hub1_node_names)
    mh2_connections = _connect_mh(hub2_node_distances, hub2_node_names)

    schedule_cfg = FIFOScheduleConfig() if schedule_cfg is None else schedule_cfg

    mh1 = MetroHubConfig(
        name="mh1",
        connections=mh1_connections,
        qlink_typ=qlink_typ,
        qlink_cfg=deepcopy(qlink_cfg),
        clink_typ=clink_typ,
        clink_cfg=deepcopy(clink_cfg),
        schedule_typ=schedule_typ,
        schedule_cfg=deepcopy(schedule_cfg),
    )
    mh2 = MetroHubConfig(
        name="mh2",
        connections=mh2_connections,
        qlink_typ=qlink_typ,
        qlink_cfg=deepcopy(qlink_cfg),
        clink_typ=clink_typ,
        clink_cfg=deepcopy(clink_cfg),
        schedule_typ=schedule_typ,
        schedule_cfg=deepcopy(schedule_cfg),
    )
    network_config.hubs = [mh1, mh2]

    repeater_node_names = [f"r{i}" for i in range(len(repeater_chain_distances) - 1)]
    repeater_nodes = []
    for node_name in repeater_node_names:
        qdevice_cfg = GenericQDeviceConfig.perfect_config() if qdevice_cfg is None else qdevice_cfg
        node = RepeaterNodeConfig(
            name=node_name, qdevice_typ=qdevice_typ, qdevice_cfg=deepcopy(qdevice_cfg)
        )
        repeater_nodes.append(node)

    qrep_chain_control_cfg = (
        qrep_chain_control_cfg if qrep_chain_control_cfg is not None else SwapASAPConfig()
    )

    repeater_chain = RepeaterChainConfig(
        metro_hub1="mh1",
        metro_hub2="mh2",
        qlink_typ=qlink_typ,
        qlink_cfg=deepcopy(qlink_cfg),
        clink_typ=clink_typ,
        clink_cfg=deepcopy(clink_cfg),
        repeater_nodes=repeater_nodes,
        lengths=repeater_chain_distances,
        long_distance_interface_typ=long_distance_interface_typ,
        long_distance_interface_cfg=deepcopy(long_distance_interface_cfg),
        qrep_chain_control_typ=qrep_chain_control_typ,
        qrep_chain_control_cfg=deepcopy(qrep_chain_control_cfg),
    )
    network_config.repeater_chains = [repeater_chain]

    return network_config
