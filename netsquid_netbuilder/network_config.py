from __future__ import annotations

from typing import Any, List, Optional

from netsquid_netbuilder.yaml_loadable import YamlLoadable


class ProcessingNodeConfig(YamlLoadable):
    """Configuration for a processing node.
    A processing node is a node that has a quantum device and is an endpoint in the network.
    """

    name: str
    """Name of the node."""
    qdevice_typ: str
    """Type of the quantum device."""
    qdevice_cfg: Any
    """Configuration of the quantum device, allowed configuration depends on type."""


class RepeaterNodeConfig(YamlLoadable):
    """Configuration for a repeater node.
    A repeater node is a node that has a quantum device and facilitates entanglement generation between other nodes.
    """

    name: str
    """Name of the node."""
    qdevice_typ: str
    """Type of the quantum device."""
    qdevice_cfg: Any
    """Configuration of the quantum device, allowed configuration depends on type."""


class QLinkConfig(YamlLoadable):
    """Configuration for a quantum link.
    A quantum link enables nodes to generate entangled qubits between their quantum devices."""

    node1: str
    """Name of the first node being connected via qlink."""
    node2: str
    """Name of the second node being connected via qlink."""
    typ: str
    """Type of the qlink."""
    cfg: Any
    """Configuration of the qlink, allowed configuration depends on type."""

    @classmethod
    def perfect_config(cls, node1: str, node2: str) -> QLinkConfig:
        """Create a configuration for a qlink without any noise or errors."""
        return QLinkConfig(node1=node1, node2=node2, typ="perfect", cfg=None)


class CLinkConfig(YamlLoadable):
    """Configuration for a classical link.
    A classical link allows classical communication between the two nodes.
    """

    node1: str
    """Name of the first node being connected via clink."""
    node2: str
    """Name of the second node being connected via clink."""
    typ: str
    """Type of the clink."""
    cfg: Any
    """Configuration of the clink, allowed configuration depends on type."""

    @classmethod
    def perfect_config(cls, node1: str, node2: str) -> CLinkConfig:
        """Create a configuration for a clink without any noise or errors."""
        return CLinkConfig(node1=node1, node2=node2, typ="instant", cfg=None)


class MetroHubConnectionConfig(YamlLoadable):
    """Configuration for connecting a repeater node to a metropolitan hub."""

    node: str
    """Name of the node being connected."""
    length: float
    """length [km] of the connection from metropolitan hub to the node."""


class MetroHubConfig(YamlLoadable):
    """Configuration for a metropolitan hub.
    A metropolitan hub connects nodes via connections that go through the hub and acts like a switch.

    .. note::
        The metropolitan hub facilitates the entanglement generation process by having resources that facilitate
        the entanglement generation between connected nodes, for example: bell state detectors.
        The metropolitan hub itself does not have a quantum device and is not capable of generating entanglement
        between itself and another node.
    """

    name: str
    """Name of the hub"""
    connections: List[MetroHubConnectionConfig]
    """List of configurations for connecting a repeater node to the metropolitan hub."""

    qlink_typ: str
    """Type of the quantum link used for connections between the connected nodes."""
    qlink_cfg: Any
    """Configuration for the quantum link."""

    clink_typ: str
    """Type of the classical link used for connections between the metropolitan hub and connected nodes."""
    clink_cfg: Any
    """Configuration for the classical link."""

    schedule_typ: str
    """Type of scheduling mechanism used for scheduling entanglement generation between connected nodes."""
    schedule_cfg: Any
    """Configuration for the scheduling mechanism."""


class RepeaterChainConfig(YamlLoadable):
    """Configuration for a repeater chain.
    A repeater chain connects two metropolitan hubs and allows for entanglement generation between nodes
    in the two metropolitan hubs.

    .. note::
        The quantum link type and configuration will only affect links between repeater nodes.
        The quantum link between repeater node and nodes in a metropolitan hub is determined
        by: :py:class:`MetroHubConfig`.

    .. note::
        The classical links set by this configuration on the other hand
        do affect the metropolitan hub to repeater node classical links.
    """

    metro_hub1: str
    """Name of the metropolitan hub being connected. This metropolitan hub is considered to be "downstream"."""
    metro_hub2: str
    """Name of the metropolitan hub being connected. This metropolitan hub is considered to be "upstream"."""

    qlink_typ: str
    """Type of the quantum link used between repeater nodes."""
    qlink_cfg: Any
    """Configuration for the quantum link."""

    clink_typ: str
    """Type of the classical link used between all nodes in the repeater chain."""
    clink_cfg: Any
    """Configuration for the classical link."""

    repeater_nodes: List[RepeaterNodeConfig]
    """List of configurations for repeater nodes in the repeater chain. Organized from "downstream" to "upstream"."""
    lengths: List[float]
    """List of lengths [km] between consecutive repeater nodes in the chain.
    Organized from "downstream" to "upstream"."""

    long_distance_interface_typ: Optional[str] = None
    """Type of long distance interface used to connect metropolitan hub to the repeater chain."""
    long_distance_interface_cfg: Optional[Any] = None
    """Configuration for the long distance interface."""

    qrep_chain_control_typ: str
    """Type of control mechanism used for the repeater chain for generating long distance entanglement."""
    qrep_chain_control_cfg: Any
    """Configuration for the repeater chain control mechanism."""


class NetworkConfig(YamlLoadable):
    """Network configuration."""

    processing_nodes: List[ProcessingNodeConfig]
    """List of the processing nodes in the network."""
    qlinks: Optional[List[QLinkConfig]] = None
    """List of the links connecting the nodes in the network."""
    clinks: Optional[List[CLinkConfig]] = None
    """List of the clinks connecting the nodes in the network."""
    hubs: Optional[List[MetroHubConfig]] = None
    """List of the metro hubs in the network"""
    repeater_chains: Optional[List[RepeaterChainConfig]] = None
    """List of the repeater chains in the network"""
