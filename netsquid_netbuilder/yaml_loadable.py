from __future__ import annotations

from abc import ABC
from typing import Any

import yaml
from pydantic import BaseModel

# This class is used for all user configuration objects, such that these can be loaded.


class YamlLoadable(BaseModel, ABC):
    """A base class that provides the functionality that the class can be loaded from a YAML file.

    .. note::
        This class uses pydantic to "validate" the arguments of any instance created.
        This validation will try to convert any argument to the correct type or raise an error if it can not.
        This validation will cause an cascade of conversions if one of the underlying types
        of an argument is also a YamlLoadable child class.

        TLDR: Use YamlLoadable to define nested datastructures, when load YAML,
        get back nested datastructure with all type good.
    """

    @classmethod
    def from_file(cls, path: str) -> Any:
        """Load the configuration from a YAML file."""
        with open(path, "r") as f:
            raw_config = yaml.load(f, Loader=yaml.Loader)
            return cls(**raw_config)  # type: ignore
