from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, Dict

from netsquid_driver.EGP import EGPService

from netsquid_netbuilder.network import Network
from netsquid_netbuilder.yaml_loadable import YamlLoadable

if TYPE_CHECKING:
    from netsquid_netbuilder.builder.repeater_chain import Chain


class IQRepChainControlConfig(YamlLoadable, ABC):
    """
    Abstract base class for configuration of quantum repeater chain control.

    This class serves as a base for configurations of quantum repeater chain control.
    Subclasses should provide specific configuration parameters for different types of quantum repeater chain control.
    """


class IQRepChainControlBuilder(ABC):
    """
    Abstract base class for building quantum repeater chain control.

    This class serves as a base for builders of quantum repeater chain control.
    Subclasses should implement the build() method to create and configure control services for the repeater chain.
    """

    @classmethod
    @abstractmethod
    def build(
        cls, chain: Chain, network: Network, control_cfg: IQRepChainControlConfig
    ) -> Dict[(str, str), EGPService]:
        """
        Abstract method to build quantum repeater chain control services.

        This method should be implemented by subclasses to create and configure control services
        for the quantum repeater chain.

        :param chain: The quantum repeater chain.
        :param network: The network in which the repeater chain operates.
        :param control_cfg: The configuration object for quantum repeater chain control.

        :return: A dictionary mapping node names to corresponding EGP services.

        """
