from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import List, Optional

from netsquid.protocols import ServiceProtocol
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest

from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_service import (
    ReqSwapASAP,
    ReqSwapASAPAbort,
    ReqSwapASAPUpdate,
    SwapASAPService,
)
from netsquid_netbuilder.network import Chain

logger = logging.getLogger(__name__)


@dataclass
class ChainControlRequestBase:
    """Base class for entanglement generation requests to Quantum repeater chain controller"""

    node1: str
    node2: str
    request_id: int


@dataclass
class ReqOpenRequestChainControl(ChainControlRequestBase):
    """Request for EGP to ask qrep control to generate entanglement generation"""


@dataclass
class ReqCloseRequestChainControl(ChainControlRequestBase):
    """Request for EGP to retract request to qrep control for engagement generation.

    Is used both to inform control that a request has completed or to cancel the request."""


@dataclass
class ResStartChainControl(ChainControlRequestBase):
    """Signal that the repeater chain will start processing this request"""


@dataclass
class ResStopChainControl(ChainControlRequestBase):
    """Signal that the repeater chain will stop processing this request"""


@dataclass
class _ControlEGRequest(ChainControlRequestBase):
    """Internal data class object for SwapASAPQrepChainController"""

    def __eq__(self, other: _ControlEGRequest):
        if other is None:
            return False
        if self.request_id == other.request_id:
            if self.node1 == other.node1 and self.node2 == other.node2:
                return True
            if self.node1 == other.node2 and self.node2 == other.node1:
                return True
        return False

    @classmethod
    def convert(cls, req: ChainControlRequestBase) -> _ControlEGRequest:
        return _ControlEGRequest(req.node1, req.node2, req.request_id)


class SwapASAPQrepChainController(ServiceProtocol):
    def __init__(self, node, chain: Chain):
        """
        Protocol that controls quantum repeater chain running the Swap ASAP strategy.

        This protocol is responsible for sending instructions to repeater nodes for switch on, off or update.
        Nodes can submit a request for entanglement generation between itself and another node.
        These requests will be executed in a FIFO fashion.
        This protocol sends out signals to indicate a change in status for requests.

        Note: this protocol is currently setup to be given a reference to each EGP protocol.
        Thus, all communication between this protocol and the EGP protocol is instantaneous and unphysical.

        :param node: The node this service is located on.
        :param chain: The quantum repeater chain this service controls.
        """
        super().__init__(node, name=f"{self.__class__.__name__} {chain.name}")
        self._chain = chain
        self._active_request: Optional[_ControlEGRequest] = None
        self._queue: List[_ControlEGRequest] = []
        self.logger = logger.getChild(self.node.name)
        self.register_request(ReqOpenRequestChainControl, self.open_request)
        self.register_request(ReqCloseRequestChainControl, self.close_request)
        self.register_response(ResStartChainControl)
        self.register_response(ResStopChainControl)

    def open_request(self, req: ReqOpenRequestChainControl):
        """Ask to generate entanglement with a remote node across the repeater chain."""
        req = _ControlEGRequest.convert(req)
        if req == self._active_request or req in self._queue:
            return
        if self._active_request is None:
            self._active_request = req
            self._send_repeater_chain_start(req, is_update=False)
        else:
            self._queue.append(req)

    def close_request(self, req: ReqCloseRequestChainControl):
        """Retract request to generate entanglement with a remote node across the repeater chain."""
        req = _ControlEGRequest.convert(req)

        if req == self._active_request:
            self._try_next_request()
        elif req in self._queue:
            self._queue.remove(req)

    def is_active(self, req: ChainControlRequestBase) -> bool:
        """Check if the given request is active."""
        req = _ControlEGRequest.convert(req)
        return req == self._active_request

    @property
    def routing_service(self) -> ClassicalRoutingService:
        return self.node.driver.services[ClassicalRoutingService]

    def stop(self):
        super().stop()
        if self._queue:
            self.logger.warning(f"Queue is not empty: {self._queue}")

    def _send_repeater_chain_shut_down(self, req: _ControlEGRequest):
        """Send a message to the repeaters in the chain to make them stop generating and swapping entanglement.

        The repeaters have been sent a swap-asap request telling them to keep generating and swapping entanglement
        indefinitely. This method sends them a swap-asap-abort request to stop them from going on forever.

        """
        self._send_end_nodes_response(req, "stop")
        self.logger.info("Shutting down repeater chain")
        repeater_node_names = list(self._chain.repeater_nodes_dict.keys())
        abort_req = ReqSwapASAPAbort(request_id=req.request_id)
        message = RemoteServiceRequest(
            abort_req, SwapASAPService, origin=self.node.name, targets=repeater_node_names
        )

        self.routing_service.put(message)

    def _send_end_nodes_response(self, req: _ControlEGRequest, response_typ: str):
        if response_typ == "start":
            response = ResStartChainControl(
                node1=req.node1, node2=req.node2, request_id=req.request_id
            )
        elif response_typ == "stop":
            response = ResStopChainControl(
                node1=req.node1, node2=req.node2, request_id=req.request_id
            )
        else:
            raise KeyError(f"Incorrect response type: {response_typ}")

        self.send_response(response)

    def _try_next_request(self):
        original_req = self._active_request
        self._active_request = None

        if not self._queue:
            self._send_repeater_chain_shut_down(original_req)
        else:
            self._active_request = self._queue.pop(0)
            self._send_repeater_chain_start(self._active_request, is_update=True)

    def _get_hub1_and_hub2_nodes(self, req: _ControlEGRequest):
        hub1_node = None
        hub2_node = None
        if req.node1 in self._chain.hub_1.end_nodes.keys():
            hub1_node = self._chain.hub_1.end_nodes[req.node1]
        if req.node2 in self._chain.hub_1.end_nodes.keys():
            hub1_node = self._chain.hub_1.end_nodes[req.node2]
        if req.node1 in self._chain.hub_2.end_nodes.keys():
            hub2_node = self._chain.hub_2.end_nodes[req.node1]
        if req.node2 in self._chain.hub_2.end_nodes.keys():
            hub2_node = self._chain.hub_2.end_nodes[req.node2]

        assert hub1_node is not None and hub2_node is not None
        return hub1_node, hub2_node

    def _send_repeater_chain_start(self, req: _ControlEGRequest, is_update: bool):
        """Sends a message to all repeater nodes in the chain with a request for them to start their local SWAP-ASAP
        protocols.
        """
        self._send_end_nodes_response(req, "start")
        self.logger.info(f"{'update' if is_update else 'start'} repeater chain with request: {req}")
        self._active_request = req
        hub1_node, hub2_node = self._get_hub1_and_hub2_nodes(req)

        max_idx = len(self._chain.repeater_nodes) - 1

        for idx, repeater_node in enumerate(self._chain.repeater_nodes):
            downstream_node_name = (
                self._chain.repeater_nodes[idx - 1].name if idx > 0 else hub1_node.name
            )
            upstream_node_name = (
                self._chain.repeater_nodes[idx + 1].name if idx < max_idx else hub2_node.name
            )

            if is_update:
                req = ReqSwapASAPUpdate(
                    upstream_node_name=upstream_node_name,
                    downstream_node_name=downstream_node_name,
                    request_id=req.request_id,
                    num=0,
                )
            else:
                req = ReqSwapASAP(
                    upstream_node_name=upstream_node_name,
                    downstream_node_name=downstream_node_name,
                    request_id=req.request_id,
                    num=0,
                )
            message = RemoteServiceRequest(
                req, SwapASAPService, origin=self.node.name, targets=[repeater_node.name]
            )
            self.routing_service.put(message)
