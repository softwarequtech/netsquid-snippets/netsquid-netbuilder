"""Implementation of a link layer protocol for a repeater chain
of automated nodes.
"""
from __future__ import annotations

import logging
from collections import defaultdict
from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, List, Optional

import netsquid as ns
import numpy as np
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest
from netsquid_driver.EGP import EGPService
from netsquid_driver.entanglement_service import (
    EntanglementError,
    EntanglementService,
    ReqEntanglement,
    ReqEntanglementAbort,
    ResEntanglementError,
    ResEntanglementSuccess,
)
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService
from netsquid_driver.measurement_services import MeasureService, ReqMeasure, ResMeasure
from netsquid_driver.memory_manager_service import (
    QuantumMemoryManager,
    ReqFreeMemory,
    ReqMove,
    ResMoveSuccess,
)
from qlink_interface import (
    ErrorCode,
    MeasurementBasis,
    ReqCreateAndKeep,
    ReqCreateBase,
    ReqMeasureDirectly,
    ReqReceive,
    ReqStopReceive,
    ResError,
)
from qlink_interface.interface import ResCreate, ResCreateAndKeep, ResMeasureDirectly

from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_controller import (
    ReqCloseRequestChainControl,
    ReqOpenRequestChainControl,
    ResStartChainControl,
    SwapASAPQrepChainController,
)

if TYPE_CHECKING:
    from netsquid_netbuilder.builder.repeater_chain import Chain
    from netsquid_netbuilder.modules.scheduler import IScheduleProtocol


logger = logging.getLogger(__name__)


@dataclass
class ReqEGPAsk:
    qlink_create_request: ReqCreateBase
    origin_node_id: int
    create_id: int


@dataclass
class ReqEGPConfirm:
    create_id: int


@dataclass
class ReqEGPError:
    result_error: ResError
    origin_node_id: int


@dataclass
class QueueItem:
    qlink_create_request: ReqCreateBase
    create_id: int


class SwapAsapEndNodeLinkLayerProtocol(EGPService):
    """Link layer protocol for end nodes in a repeater chain of automated nodes generating end-to-end entanglement once.

    The process starts when a request is placed on one of the end nodes. This can be a
    :class:`~qlink_interface.ReqMeasureDirectly` request, if the entangled pair should be measured upon generation
    (e.g. for QKD) or a :class:`~qlink_interface.ReqCreateAndKeep` request, if the entangled pair should be kept.

    The node who received the request will send a request to the other end node, asking to generate entanglement.
    Upon receive of this message, the second end node send a confirmation message back.
    The second node will start the next steps, described next paragraph directly,
    the first node will wait for the confirmation before proceeding.

    Now each node will inform the repeater chain controller service, that holds a queue of requests
    for the repeater chain, of this request and wait for a signal from the controller to proceed.
    This signal indicates that the repeater chain is now generating entanglement for the purpose of this request.
    The repeaters will now also inform the local metropolitan hub schedulers and start their own process
    of generating entanglement with the repeater node they are connected to.
    This process will continue until the nodes know that the desired entanglement has been generated
    by listening to a signal from the :class:`~netsquid_driver.entanglement_tracker_service.EntanglementTrackerService`
    that the desired entanglement has been created.

    The process of generating entanglement with the repeater node and waiting for the desired entanglement will
    repeat for as many times as the request specifies.

    When an end node receives all the swap outcomes from the repeater nodes, it sends a
    success response back, which can be :class:`~qlink_interface.ResMeasureDirectly` or
    :class:`~qlink_interface.ResCreateAndKeep`. In the second case, a measurement request
    :class:`~netsquid_driver.measurement_services.ReqMeasure` is placed on the local measurement service and the
    outcome of this measurement is included in the response.


    Parameters
    ----------
        node : :class:`~netsquid.nodes.node.Node`
            The node this protocol is running on
        name : str, optional
            The name of this protocol

    """

    _TYPE_CONFIRM = "confirm"
    _TYPE_ASK = "ask"

    class WaitIfMeasurementOutcomeShouldBeKept(ns.protocols.nodeprotocols.NodeProtocol):
        """Subprotocol to wait and decide whether a measurement outcome should be kept.

        Measurement outcomes on qubits should only be returned if the qubit is successfully entangled with the remote
        end node. However, in case of a measure-directly request, you want to measure your qubit straight away to avoid
        memory decoherence instead of waiting until end-to-end entanglement is heralded. This protocol then hangs
        on to the measurement result, discarding it if end-to-end entanglement generation fails and sending a response
        with the outcome if it succeeds.

        Parameters
        ----------
        superprotocol : :obj:`netsquid_qrepchain.control_layer.swapasap_egp.SwapAsapEndNodeLinkLayerProtocol`
            Protocol of which this protocol should be used as a subprotocol.
        id : int
            Identifier for the subprotocol. Used to name it.

        """

        def __init__(self, superprotocol, id):
            super().__init__(
                node=superprotocol.node, name=f"wait_if_measurement_outcome_should_be_kept_{id}"
            )
            self._superprotocol = superprotocol
            self._link_identifier = None
            self._remote_node_id = None
            self.measurement_outcome = None
            self.measurement_basis = None
            self.create_id = None
            self.bell_state = None

        def start(
            self, link_identifier, remote_node_id, measurement_outcome, measurement_basis, create_id
        ):
            """Start the subprotocol to hold on to a specific measurement result.

            Parameters
            ----------
            link_identifier : :obj:`entanglement_tracker_service.LinkIdentifier`
                Link identifier of qubit that was measured.
            remote_node_id : int
                Identifier of the remote end node with which entanglement is being generated.
            measurement_outcome : bool
                Outcome of the measurement.
            measurement_basis : :obj:`qlink_interface.interface.MeasurementBasis`
                The basis in which the measurement was performed.
            create_id : int
                Identifier of the request on the superprotocol for which the measurement was performed.

            """
            self._link_identifier = link_identifier
            self._remote_node_id = remote_node_id
            self.measurement_outcome = measurement_outcome
            self.measurement_basis = measurement_basis
            self.create_id = create_id
            self.bell_state = None
            super().start()

        def run(self):
            # wait until success or failure of end-to-end entanglement is heralded
            entanglement_tracker = self.node.driver[EntanglementTrackerService]
            yield from entanglement_tracker.await_entanglement(
                node_ids={self.node.ID, self._remote_node_id},
                link_identifiers={self._link_identifier},
                other_nodes_allowed=False,
            )

            # if already enough pairs were generated, don't bother sending the response
            # (sending the response can be confusing in this case, as more responses are obtained than expected)
            if (
                self._superprotocol._number_pairs_generated
                >= self._superprotocol._number_pairs_to_generate
            ):
                return

            # if entanglement was successfully generated, send a response; else, do nothing.
            entanglement_info = entanglement_tracker.get_entanglement_info_of_link(
                self._link_identifier
            )
            if entanglement_info.intact:
                self.bell_state = entanglement_info.bell_index
                self._superprotocol._handle_heralded_success_for_measured_qubit(subprotocol=self)

    def __init__(
        self,
        node,
        node_name_id_mapping: Dict[str, int],
        chain: Chain,
        hub_scheduler: IScheduleProtocol,
        controller: SwapASAPQrepChainController,
        name=None,
    ):
        super().__init__(node=node, name=name)
        self.logger = logger.getChild(node.name)
        # actual params
        self._node_name_id_mapping = node_name_id_mapping
        self._id_node_name_mapping = {
            id_: node_name for node_name, id_ in node_name_id_mapping.items()
        }
        self._local_measurement_angles = None
        self._remote_node_name: Optional[str] = None
        self._remote_node_id: Optional[int] = None
        self._connected_repeater_node = chain.get_connected_repeater(self.node.name)
        self._chain = chain
        self._hub_scheduler = hub_scheduler
        self._controller = controller
        self._create_id = None

        self._active_request: Optional[ReqCreateBase] = None
        self._active_request_create_id: Optional[int] = None

        self._received_control_signals = []

        # params that should be part of a "ActiveRequest" object
        self._should_keep = None
        self._req_directionality_flag = None
        self._link_identifier = None  # link identifier of most recently generated entanglement
        self._number_pairs_to_generate = None
        self._number_pairs_generated = None
        self._handle_request_signal = "handle_request_signal"
        self.add_signal(self._handle_request_signal)
        self._rotation_to_measbasis = {
            (0, np.pi / 2, 0): MeasurementBasis.X,
            (np.pi / 2, 0, 0): MeasurementBasis.Y,
            (0, 0, 0): MeasurementBasis.Z,
        }
        self._bell_state = None  # Bell index of expected end-to-end entanglement
        self._unique_id = 0
        self._sequence_number = 0
        self._measurement_subprotocols = []
        self._request_queue: List[QueueItem] = []
        self._ask_queue: List[ReqEGPAsk] = []

        # Keep track of the rules for receiving EPR pairs
        self._recv_rules = defaultdict(set)

        self.register_request(ReqEGPAsk, self._handle_ask_request)
        self.register_request(ReqEGPConfirm, self._handle_confirm)
        self.register_request(ReqEGPError, self._handle_error)

    @property
    def routing_service(self) -> ClassicalRoutingService:
        return self.node.driver.services[ClassicalRoutingService]

    def run(self):
        """Generates end-to-end entanglement across a repeater chain of automated nodes."""
        while True:
            yield self.await_signal(sender=self, signal_label=self._handle_request_signal)

            # Inform qrep chain controller and hub scheduler of request,
            # await activation of qrep chain before registering to hub to avoid deadlocks
            yield from self._register_and_await_request_chain_controller()
            self._register_request_hub_scheduler()

            # Start request execution
            if self._should_keep:
                yield from self._execute_create_and_keep()
            else:
                yield from self._execute_measure_directly()

            # Inform qrep chain controller and hub scheduler of completion of request
            self._register_result_chain_controller()
            self._register_result_hub_scheduler()

            self.logger.info(f"Finished request: {self._active_request}")
            self._clear_active_request()
            self._attempt_start_next_request()

    def start(self):
        super().start()

    def stop(self):
        super().stop()
        if self._active_request:
            self.logger.warning(f"Unfinished request in process: {self._active_request}")
        if len(self._request_queue):
            self.logger.warning(f"Unfinished requests in queue: {self._request_queue}")
        if len(self._ask_queue):
            self.logger.warning(
                f"Unfinished requests from remote nodes in queue: {self._ask_queue}"
            )

    def _register_request_hub_scheduler(self):
        request = ReqCreateBase(
            remote_node_id=self._connected_repeater_node.ID, number=self._number_pairs_to_generate
        )
        self._hub_scheduler.register_request(self.node.ID, request, self._create_id)

    def _register_result_hub_scheduler(self):
        # We register two results to the scheduler. The first one is from this node, the second one is
        # a "fake" that acts as if it was sent by the repeater to the scheduler.
        # The underlying reason is that scheduler requires both parties to check off before closing
        result = ResCreate(self._create_id, remote_node_id=self.node.ID)
        self._hub_scheduler.register_result(self._connected_repeater_node.ID, result)
        result = ResCreate(self._create_id, remote_node_id=self._connected_repeater_node.ID)
        self._hub_scheduler.register_result(self.node.ID, result)

    def _register_and_await_request_chain_controller(self):
        req = ReqOpenRequestChainControl(
            node1=self.node.name, node2=self._remote_node_name, request_id=self._create_id
        )
        self._controller.open_request(req)
        is_active = self._controller.is_active(req)
        while not is_active:
            yield self.await_signal(
                sender=self._controller, signal_label=ResStartChainControl.__name__
            )
            is_active = self._controller.is_active(req)

    def _register_result_chain_controller(self):
        req = ReqCloseRequestChainControl(
            node1=self.node.name, node2=self._remote_node_name, request_id=self._create_id
        )
        self._controller.close_request(req)

    def _execute_create_and_keep(self):
        """Execute a create-and-keep request.

        End-to-end entanglement is generated and a response is sent detailing the created entangled state and
        the location of the qubit.
        """
        self._number_pairs_generated = 0
        while self._number_pairs_generated < self._number_pairs_to_generate:
            yield from self._generate_end_to_end_entanglement()
            self._number_pairs_generated += 1
            self.send_response(
                ResCreateAndKeep(
                    create_id=self._create_id,
                    bell_state=self._bell_state,
                    logical_qubit_id=self._mem_pos_most_recent_entanglement,
                    sequence_number=self._sequence_number,
                    remote_node_id=self._remote_node_id,
                    directionality_flag=self._req_directionality_flag,
                )
            )

    def _generate_end_to_end_entanglement(self):
        """Generate entanglement between this node and the other end node (specified in the request).

        End-to-end entanglement is generated by generating entanglement with this end node's neighbour,
        and then waiting for the entanglement tracker to decide that either
        1. the local entangled qubit has become entangled with the other end node, or
        2. the entangled state of the local entangled qubit has been destroyed (e.g. because a qubit has been discarded
        by a repeater node).

        In case the second condition is met, a new attempt is made by starting entanglement generation with the
        neighbour again.

        """
        while True:
            yield from self._perform_own_link_layer()
            yield from self._entanglement_tracker.await_entanglement(
                node_ids={self.node.ID, self._remote_node_id},
                link_identifiers={self._link_identifier},
                other_nodes_allowed=False,
            )
            entanglement_info = self._entanglement_tracker.get_entanglement_info_of_link(
                self._link_identifier
            )
            if entanglement_info.intact:
                self._bell_state = entanglement_info.bell_index
                self._entanglement_tracker.untrack_entanglement_info(entanglement_info)
                break
            else:
                self._free_memory()

    def _execute_measure_directly(self):
        """Execute a measure-directly request.

        Generate entanglement locally and measure it as soon as a local qubit becomes available.
        The measurement result is then kept until the entanglement tracker learns whether entangling the measured
        qubit with the remote node has failed or succeeded. If it failed, the result is discarded. If it succeeded,
        a response is sent with the measurement result and the result is counted towards the completion of the
        measure-directly request.
        As soon as the local qubit has been measured, local entanglement generation will restart again,
        up until when local entanglement generation has been completed and it turns out that enough measurement results
        have been successfully reported.
        """
        self._number_pairs_generated = 0
        while self._number_pairs_generated < self._number_pairs_to_generate:
            yield from self._perform_own_link_layer()
            if self._number_pairs_generated >= self._number_pairs_to_generate:
                # if the own link layer finished because it was aborted, we don't need to measure
                break
            request = ReqMeasure(
                mem_pos=self._mem_pos_most_recent_entanglement,
                x_rotation_angle_1=self._local_measurement_angles["x_1"],
                y_rotation_angle=self._local_measurement_angles["y"],
                x_rotation_angle_2=self._local_measurement_angles["x_2"],
            )
            measure_protocol = self.node.driver.services[MeasureService]
            measure_protocol.put(request=request)
            yield self.await_signal(sender=measure_protocol, signal_label=ResMeasure.__name__)
            response = measure_protocol.get_signal_result(label=ResMeasure.__name__, receiver=self)
            if isinstance(response.outcome, list):
                measurement_outcome = response.outcome[0]
            else:
                measurement_outcome = response.outcome
            self._wait_and_decide_if_measurement_outcome_should_be_kept(
                link_identifier=self._link_identifier, measurement_outcome=measurement_outcome
            )
            self._free_memory()

    def _wait_and_decide_if_measurement_outcome_should_be_kept(
        self, link_identifier, measurement_outcome
    ):
        """Start a subprotocol to hold on to a measurement outcome until success or failure is heralded.

        Measurement outcomes on qubits should only be returned if the qubit is successfully entangled with the remote
        end node. However, in case of a measure-directly request, you want to measure your qubit straight away to avoid
        memory decoherence instead of waiting until end-to-end entanglement is heralded.
        This method starts a protocol that hangs on to the measurement result,
        discarding it if end-to-end entanglement generation fails,
        and sending a response with the outcome if it succeeds.

        Parameters
        ----------
        link_identifier : :obj:`entanglement_tracker_service.LinkIdentifier`
            Link identifier of qubit that was measured.
        measurement_outcome : bool
            Outcome of the measurement.

        """
        free_subprotocols = [prot for prot in self._measurement_subprotocols if not prot.is_running]
        if not free_subprotocols:
            subprotocol = self.WaitIfMeasurementOutcomeShouldBeKept(
                superprotocol=self, id=len(self._measurement_subprotocols)
            )
            self._measurement_subprotocols.append(subprotocol)
        else:
            subprotocol = free_subprotocols[0]
        measurement_basis = self._rotation_to_measbasis[
            tuple(self._local_measurement_angles.values())
        ]
        subprotocol.start(
            link_identifier=link_identifier,
            remote_node_id=self._remote_node_id,
            measurement_outcome=measurement_outcome,
            measurement_basis=measurement_basis,
            create_id=self._create_id,
        )

    def _handle_heralded_success_for_measured_qubit(self, subprotocol):
        """Method for when a subprotocol holding on to measurement result indicates it was entangled successfully.

        Sends a measure-directly response and if enough pairs have been generated, it stops remaining running
        subprotocols and aborts local entanglement generation.

        Parameters
        ----------
        subprotocol : :obj:`netsquid_qrepchain.control_layer.swapasap_egp.SwapAsapEndNodeLinkLayerProtocol \
            .WaitIfMeasurementOutcomeShouldBeKept`
            Subprotocol that calls this method.

        Notes
        -----
        Stopping local entanglement generation with an abort request results in the loop of
        `_perform_own_link_layer()` to be broken, allowing the protocol to continue.
        No abort request is sent to the node that this node was generating entanglement with;
        entanglement generation on that node should be aborted through `_shut_down_repeater_chain()`,
        which gets triggered immediately exactly because the protocol is made to continue through the abort request.

        """
        response = ResMeasureDirectly(
            measurement_outcome=subprotocol.measurement_outcome,
            create_id=subprotocol.create_id,
            bell_state=subprotocol.bell_state,
            measurement_basis=subprotocol.measurement_basis,
            sequence_number=self._sequence_number,
            remote_node_id=self._remote_node_id,
            directionality_flag=self._req_directionality_flag,
        )
        self.send_response(response=response)
        self._sequence_number += 1
        self._number_pairs_generated += 1
        if self._number_pairs_generated == self._number_pairs_to_generate:
            self.node.driver[EntanglementService].put(
                ReqEntanglementAbort(remote_node_name=self._get_neighboring_repeater_node_name())
            )

    def _handle_ask_request(self, req: ReqEGPAsk):
        """Handles ask message received from remote end node."""
        if not self._is_valid_request(req.qlink_create_request, req.origin_node_id):
            result_error = ResError(
                create_id=req.create_id,
                error_code=ErrorCode.REJECTED,
            )
            self._send_error_to_other_egp(result_error, req.origin_node_id)
            return

        self._ask_queue.append(req)
        self._attempt_start_next_request()

    def _start_ask_request(self, req: ReqEGPAsk):
        """starts ask message received from remote end node."""
        self.logger.info(f"Start ask request: {req}")
        self._active_request_create_id = req.create_id
        self._active_request = req.qlink_create_request
        self._create_id = req.create_id
        self._req_directionality_flag = 1
        self._remote_node_id = req.origin_node_id
        self._remote_node_name = self._id_node_name_mapping[self._remote_node_id]
        # self._remote_node_name = # from ask put here
        self._send_confirm_message_to_other_egp()
        if isinstance(req.qlink_create_request, ReqCreateAndKeep):
            self._should_keep = True  # otherwise, there would have been measurement angles
        else:
            assert isinstance(req.qlink_create_request, ReqMeasureDirectly)
            self._should_keep = False
            self._local_measurement_angles = {
                "x_1": req.qlink_create_request.x_rotation_angle_remote_1,
                "y": req.qlink_create_request.y_rotation_angle_remote,
                "x_2": req.qlink_create_request.x_rotation_angle_remote_2,
            }
        self._sequence_number = 0
        self._number_pairs_to_generate = req.qlink_create_request.number
        self.send_signal(self._handle_request_signal)

    def _handle_confirm(self, req: ReqEGPConfirm):
        """Handles confirm message received from remote end node."""
        assert req.create_id == self._active_request_create_id
        self._req_directionality_flag = 0
        self._create_id = req.create_id
        self.send_signal(self._handle_request_signal)

    def _handle_error(self, req: ReqEGPError):
        """Handles error message received from remote end node."""
        assert req.result_error.create_id == self._active_request_create_id
        assert req.origin_node_id == self._remote_node_id
        self.send_response(req.result_error)
        self._clear_active_request()
        self._attempt_start_next_request()

    def _send_confirm_message_to_other_egp(self):
        """Sends a message to the other end node on the chain confirming
        that it wants to start generating entanglement."""
        req = ReqEGPConfirm(self._create_id)
        message = RemoteServiceRequest(
            req, EGPService, origin=self.node.name, targets=[self._remote_node_name]
        )
        self.routing_service.put(message)

    def _send_ask_to_other_egp(self, req: ReqCreateBase):
        """Sends a message to the other end node on the chain asking if it wants to start generating entanglement."""
        req = ReqEGPAsk(req, self.node.ID, self._create_id)
        message = RemoteServiceRequest(
            req, EGPService, origin=self.node.name, targets=[self._remote_node_name]
        )
        self.routing_service.put(message)

    def _send_error_to_other_egp(self, res: ResError, remote_node_id: int):
        req = ReqEGPError(result_error=res, origin_node_id=self.node.ID)
        remote_node_name = self._id_node_name_mapping[remote_node_id]
        message = RemoteServiceRequest(
            req, EGPService, origin=self.node.name, targets=[remote_node_name]
        )
        self.routing_service.put(message)

    def create_and_keep(self, req: ReqCreateAndKeep):
        """Starts the process of generating an end-to-end entangled pair to be kept by sending message to other end node
        asking to start entanglement generation.

        Parameters
        ----------
        req : :class:`~qlink_interface.ReqCreateAndKeep`
            Request that needs to be handled by this method.

        """
        req_create_id = self._get_create_id()
        self._request_queue.append(QueueItem(req, req_create_id))
        self._attempt_start_next_request()
        return req_create_id

    def measure_directly(self, req: ReqMeasureDirectly):
        """Starts the process of generating an end-to-end entangled pair to be immediately measured by sending message
        to other end node asking to start entanglement generation.

        Parameters
        ----------
        req : :class:`~qlink_interface.ReqMeasureDirectly`
            Request that needs to be handled by this method.

        """
        req_create_id = self._get_create_id()
        self._request_queue.append(QueueItem(req, req_create_id))
        self._attempt_start_next_request()
        return req_create_id

    def remote_state_preparation(self, req):
        raise NotImplementedError

    def receive(self, req: ReqReceive):
        """Allow entanglement generation with a remote EGP upon request by that EGP.

        Parameters
        ----------
        req : :object:`qlink_interface.ReqReceive`
            Request that needs to be handled by this method.

        """
        self._recv_rules[req.remote_node_id].add(req.purpose_id)

    def stop_receive(self, req: ReqStopReceive):
        """Stop allowing entanglement generation with a remote EGP upon request by that EGP.

        Parameters
        ----------
        req : :object:`qlink_interface.ReqStopReceive`
            Request that needs to be handled by this method.

        """
        self._recv_rules[req.remote_node_id].remove(req.purpose_id)

    def _is_valid_request(self, req: ReqCreateBase, origin_node_id: int):
        return req.purpose_id in self._recv_rules[origin_node_id]

    def _start_request(self, queue_item: QueueItem):
        """Begin processing a request that was put on the queue"""
        req = queue_item.qlink_create_request
        self.logger.info(f"Start request: {req}")
        self._create_id = queue_item.create_id
        self._remote_node_id = req.remote_node_id
        self._remote_node_name = self._id_node_name_mapping[self._remote_node_id]
        self._should_keep = True
        self._active_request_create_id = (
            self._create_id
        )  # TODO investigate if we need a separate _active_request_create_id
        self._number_pairs_to_generate = req.number
        self._send_ask_to_other_egp(req)
        self._active_request = req
        self._sequence_number = 0

        if isinstance(req, ReqMeasureDirectly):
            self._should_keep = False
            self._local_measurement_angles = {
                "x_1": req.x_rotation_angle_local_1,
                "y": req.y_rotation_angle_local,
                "x_2": req.x_rotation_angle_local_2,
            }
            if tuple(self._local_measurement_angles.values()) not in self._rotation_to_measbasis:
                raise ValueError(
                    "Link layer protocol only supports measurements in X, Y and Z bases."
                )
        elif isinstance(req, ReqCreateAndKeep):
            self._should_keep = True
        else:
            raise NotImplementedError(f"Requests of type: {type(req)} not supported")

        return self._create_id

    def _perform_own_link_layer(self):
        """Places :class:`~netsquid_driver.entanglement_service.ReqEntanglement` requests to the local
        entanglement generation protocol and awaits a response. If we get a failure response,
        :class:`~netsquid_driver.entanglement_service.ResEntanglementError`, we try again until a success response,
        :class:`~netsquid_driver.entanglement_service.ResEntanglementSuccess`, occurs.
        """
        mem_pos = self._get_free_communication_qubit_position()

        request = ReqEntanglement(
            remote_node_name=self._get_neighboring_repeater_node_name(), mem_pos=mem_pos
        )
        local_entanglement_protocol = self.node.driver[EntanglementService]
        local_entanglement_protocol.put(request=request)

        while True:
            evt_entanglement_succ = self.await_signal(
                sender=local_entanglement_protocol, signal_label=ResEntanglementSuccess.__name__
            )
            evt_entanglement_fail = self.await_signal(
                sender=local_entanglement_protocol, signal_label=ResEntanglementError.__name__
            )
            evt_expr = evt_entanglement_succ | evt_entanglement_fail
            yield evt_expr

            if evt_expr.first_term.value:
                self._link_identifier = self._entanglement_tracker.get_link(mem_pos)
                try:
                    if (
                        self.node.driver[QuantumMemoryManager].num_communication_qubits == 1
                        and self._should_keep
                        and self.node.qmemory.num_positions > 2
                    ):
                        request = ReqMove(from_memory_position_id=mem_pos)
                        self.node.driver[QuantumMemoryManager].put(request=request)
                        yield self.await_signal(
                            sender=self.node.driver[QuantumMemoryManager],
                            signal_label=ResMoveSuccess.__name__,
                        )
                        response = self.node.driver[QuantumMemoryManager].get_signal_result(
                            label=ResMoveSuccess.__name__, receiver=self
                        )
                        self._link_identifier = self._entanglement_tracker.get_link(
                            response.memory_position_id
                        )
                except AttributeError:
                    pass
                break
            else:
                self._free_memory()
                response = local_entanglement_protocol.get_signal_result(
                    ResEntanglementError.__name__
                )
                # if entanglement was aborted, we stop trying to generate entanglement
                # if it failed for another reason, we just keep trying until success
                if response.error_code is EntanglementError.ABORTED:
                    break

    def _get_neighboring_repeater_node_name(self):
        if len(self._chain.repeater_nodes) == 0:
            return self._remote_node_name
        elif self.node.name in self._chain.hub_1.end_nodes.keys():
            return self._chain.repeater_nodes[0].name
        else:
            return self._chain.repeater_nodes[-1].name

    @property
    def is_connected(self):
        """The service is only connected if the driver at the node has one communication port and the right services.

        Requires exactly one port with either name "A" or "B" for classical communication.
        At the node's :class:`Driver`, the following services must be registered:
        - :class:`netsquid_driver.classical_routing_service.ClassicalRoutingService`,
        - :class:`netsquid_driver.measurement_services.MeasureService`,
        - :class:`netsquid_driver.entanglement_service.EntanglementService`.

        """
        if ClassicalRoutingService not in self.node.driver.services:
            return False
        if MeasureService not in self.node.driver.services:
            return False
        if EntanglementService not in self.node.driver.services:
            return False
        if EntanglementTrackerService not in self.node.driver.services:
            return False
        return super().is_connected

    def _get_free_communication_qubit_position(self):
        """Get ID of a memory position corresponding to a free communication qubit.

        The free communication qubit can be used to generate entanglement.
        Currently, this method takes any qubit on the qmemory.
        Ideally, a call would be made to a memory manager to obtain a free communication qubit.
        """
        unused_positions = self.node.qmemory.unused_positions
        if len(unused_positions) > 0:
            return unused_positions[0]
        else:
            raise RuntimeError(
                f"Node: {self.node.name} has no free qubit available for EPR pair generation"
            )

    @property
    def _mem_pos_most_recent_entanglement(self):
        """Get memory position where the most-recently entangled qubit is currently stored."""
        return self._entanglement_tracker.get_mem_pos(self._link_identifier)

    @property
    def _entanglement_tracker(self):
        """Entanglement tracker running on this node."""
        return self.node.driver[EntanglementTrackerService]

    def _free_memory(self):
        """Free all memory positions at this node.

        Currently, this only means setting the `in_use` flag to False.
        """
        req = ReqFreeMemory()
        self.node.driver[QuantumMemoryManager].put(request=req)

    def _clear_active_request(self):
        self._remote_node_id = None
        self._active_request = None
        self._remote_node_name = None
        self._active_request_create_id = None
        self._create_id = None
        self._number_pairs_generated = 0
        self._local_measurement_angles = None

    def _attempt_start_next_request(self):
        """Try to start executing a new item on the queue if not busy"""
        if self._active_request is not None:
            return
        if len(self._ask_queue) > 0:
            self._start_ask_request(self._ask_queue.pop(0))
            return
        if len(self._request_queue) > 0:
            self._start_request(self._request_queue.pop(0))
            return


class SwapAsapLinkLayerThatCollectsQubitsWhenMeasuring(SwapAsapEndNodeLinkLayerProtocol):
    """This version of the swap-asap link-layer protocol sends measure-directly responses with qubits, not outcomes.

    While this protocol abuses interfaces a bit by returning qubits in the place of measurement outcomes,
    using it can be convenient; by collecting qubits on measure-directly requests one can reconstruct the effective
    end-to-end state at the times of Alice's and Bob's measurements. From this, the measurement statistics can be
    obtained, which contains more information than just the result of a single measurement.
    This makes for more-efficient data collection.

    """

    def _execute_measure_directly(self):
        self._number_pairs_generated = 0
        while self._number_pairs_generated < self._number_pairs_to_generate:
            yield from self._perform_own_link_layer()
            qubit = self.node.qmemory.mem_positions[
                self._mem_pos_most_recent_entanglement
            ].get_qubit(remove=True, skip_noise=False)
            self._wait_and_decide_if_measurement_outcome_should_be_kept(
                link_identifier=self._link_identifier, measurement_outcome=qubit
            )
            self._free_memory()
