from __future__ import annotations

import itertools
from typing import TYPE_CHECKING, Dict, Optional

from netsquid_driver.EGP import EGPService
from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService
from netsquid_driver.entanglement_service import EntanglementService
from netsquid_driver.initiative_based_agreement_service import (
    InitiativeAgreementService,
    InitiativeBasedAgreementServiceMode,
)
from netsquid_entanglementtracker.cutoff_service import CutoffService
from netsquid_entanglementtracker.cutoff_timer import CutoffTimer
from netsquid_magic.entanglement_magic import EntanglementMagic
from netsquid_magic.qlink import MagicQLink

from netsquid_netbuilder.modules.qrep_chain_control.interface import (
    IQRepChainControlBuilder,
    IQRepChainControlConfig,
)
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap import SwapASAP
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_controller import (
    SwapASAPQrepChainController,
)
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_egp import (
    SwapAsapEndNodeLinkLayerProtocol,
)
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_service import (
    SwapASAPService,
)

if TYPE_CHECKING:
    from netsquid_netbuilder.builder.repeater_chain import Chain
    from netsquid_netbuilder.network import Network, NodeWithDriver


class SwapASAPConfig(IQRepChainControlConfig):
    cutoff_time: Optional[float] = None
    """cutoff time [ns] after which entanglements will be discarded."""
    parallel_link_generation: bool = True
    """Toggle if nodes can generate entanglement with upstream and downstream at the same time, or one at the time."""
    controller_node: Optional[str] = None
    """Name of the node that hosts the control service that sends and receives instructions for the repeater chain."""


class SwapASAPBuilder(IQRepChainControlBuilder):
    @classmethod
    def build(
        cls, chain: Chain, network: Network, control_cfg: SwapASAPConfig
    ) -> Dict[(str, str), EGPService]:

        # Create control node service
        control_node = cls._get_control_node(chain, network, control_cfg)
        control_service = SwapASAPQrepChainController(control_node, chain)
        control_node.driver.add_service(SwapASAPQrepChainController, control_service)

        # The following code has two stages, first we create temp_egp_dict and then egp_dict.
        # The real EGP objects are per node as temp_egp_dict builds them,
        # but due to how the direct LinkLayer protocol works (a separate EGP per peer),
        # and because direct link and cross-chain EGP have not been combined we need to expose an EGP per peer.
        # So the egp_dict that is created is a dict where we give the same EGP per peer (Only in this cross-chain case).
        temp_egp_dict: Dict[str, EGPService] = {}

        for hub in [chain.hub_1, chain.hub_2]:
            for end_node in hub.end_nodes.values():
                repeater_egp = SwapAsapEndNodeLinkLayerProtocol(
                    end_node,
                    network.node_name_id_mapping,
                    list(network.chains.values())[0],
                    hub.scheduler,
                    control_service,
                )
                end_node.driver.add_service(EGPService, repeater_egp)
                temp_egp_dict[end_node.name] = repeater_egp

        egp_dict: Dict[(str, str), EGPService] = {}
        for hub1_end_node, hub2_end_node in itertools.product(
            chain.hub_1.end_nodes.values(), chain.hub_2.end_nodes.values()
        ):

            egp_dict[(hub1_end_node.name, hub2_end_node.name)] = temp_egp_dict[hub1_end_node.name]
            egp_dict[(hub2_end_node.name, hub1_end_node.name)] = temp_egp_dict[hub2_end_node.name]

        cls._setup_services(chain, network, control_cfg)

        return egp_dict

    @classmethod
    def _get_control_node(
        cls, chain: Chain, network: Network, control_cfg: SwapASAPConfig
    ) -> NodeWithDriver:
        # Default is a repeater node on the edge of the system
        if control_cfg.controller_node is None:
            return chain.repeater_nodes[0]
        if control_cfg.controller_node not in network.nodes.keys():
            raise ValueError(
                f"Could not find node with name: {control_cfg.controller_node} in the network"
            )
        return network.nodes[control_cfg.controller_node]

    @classmethod
    def _setup_services(cls, chain: Chain, network: Network, control_cfg: SwapASAPConfig):
        all_chain_nodes = (
            list(chain.hub_1.end_nodes.values())
            + list(chain.hub_2.end_nodes.values())
            + chain.repeater_nodes
        )

        for node in all_chain_nodes:
            driver = node.driver

            local_link_dict = network.filter_for_node(node.name, network.qlinks)

            local_link_dict = {
                node_name: qlink
                for node_name, qlink in local_link_dict.items()
                if isinstance(qlink, MagicQLink)
            }

            num_parallel_links = 2 if control_cfg.parallel_link_generation else 1

            driver.services[EntanglementService] = EntanglementMagic(
                node,
                local_link_dict,
                node_name_id_mapping=network.node_name_id_mapping,
                num_parallel_links=num_parallel_links,
            )

            if control_cfg.cutoff_time:
                driver.add_service(
                    CutoffService, CutoffTimer(node=node, cutoff_time=control_cfg.cutoff_time)
                )

        for repeater in chain.repeater_nodes:
            driver = repeater.driver
            driver.add_service(SwapASAPService, SwapASAP(repeater))

        cls._setup_agreement_services(chain, network)

    @classmethod
    def _setup_agreement_services(cls, chain: Chain, network: Network):

        # Hub1 nodes, start mode as Initiative
        for end_node in chain.hub_1.end_nodes.values():
            cls._setup_agreement_service(
                end_node, mode=InitiativeBasedAgreementServiceMode.InitiativeTaking, network=network
            )

        # repeater nodes, alternate modes
        for i, repeater in enumerate(chain.repeater_nodes):
            if i % 2 == 0:
                mode = InitiativeBasedAgreementServiceMode.Responding
            else:
                mode = InitiativeBasedAgreementServiceMode.InitiativeTaking

            cls._setup_agreement_service(repeater, mode, network)

        # Hub2 nodes, choose mode based on length of repeater chain
        if len(chain.repeater_nodes) % 2 == 1:
            mode = InitiativeBasedAgreementServiceMode.InitiativeTaking
        else:
            mode = InitiativeBasedAgreementServiceMode.Responding

        for end_node in chain.hub_2.end_nodes.values():
            cls._setup_agreement_service(end_node, mode, network)

    @staticmethod
    def _setup_agreement_service(
        node: NodeWithDriver, mode: InitiativeBasedAgreementServiceMode, network: Network
    ):
        driver = node.driver
        driver.add_service(
            EntanglementAgreementService,
            InitiativeAgreementService(
                node=node,
                delay_per_node=network.filter_for_node(
                    node.name, network.network_topology_info.delays_table
                ),
                mode=mode,
            ),
        )
