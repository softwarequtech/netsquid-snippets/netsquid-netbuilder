from __future__ import annotations

from abc import ABC, abstractmethod

from netsquid.components import QuantumProcessor

from netsquid_netbuilder.yaml_loadable import YamlLoadable


class IQDeviceConfig(YamlLoadable, ABC):
    """
    Abstract base class for configuration of quantum devices.

    This class serves as a base for configurations of quantum devices.
    Subclasses should provide specific configuration parameters for different types of quantum devices.

    """


class IQDeviceBuilder(ABC):
    """
    Abstract base class for building quantum devices.

    This class serves as a base for builders of quantum devices.
    Subclasses should implement the build() method to create instances of QuantumProcessor
    and build_services() method to provide necessary services for the quantum device.

    """

    @classmethod
    @abstractmethod
    def build(cls, name: str, qdevice_cfg: IQDeviceConfig) -> QuantumProcessor:
        """
        Abstract method to build a quantum processor.

        This method should be implemented by subclasses to create and configure
        instances of QuantumProcessor based on the provided configuration.

        :param name: The name of the quantum processor.
        :param qdevice_cfg: The configuration object for the quantum device.
        :return: An instance of the built quantum processor.

        """

    @classmethod
    @abstractmethod
    def build_services(cls, node):
        """
        Abstract method to build services for the quantum device.

        This method should be implemented by subclasses to provide necessary services
        for the quantum device.

        :param node: The quantum device node.

        """
