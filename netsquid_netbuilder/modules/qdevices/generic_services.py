from netsquid.components import (
    INSTR_CNOT,
    INSTR_H,
    INSTR_MEASURE,
    INSTR_ROT_X,
    INSTR_ROT_Y,
    QuantumProgram,
)
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.operation_services import (
    IBellStateMeasurementProgram,
    ProcessingNodeMeasureService,
    ProcessingNodeSwapService,
)


class GenericMoveProgram(QuantumProgram):
    def program(self):
        move_from, move_to = self.get_qubit_indices(2)

        # Perform a SWAP gate via three CNOT gates
        self.apply(INSTR_CNOT, [move_from, move_to])
        self.apply(INSTR_CNOT, [move_to, move_from])
        self.apply(INSTR_CNOT, [move_from, move_to])

        yield self.run()


class GenericMeasureProgram(QuantumProgram):
    def program(self, x_rotation_angle_1, y_rotation_angle, x_rotation_angle_2):
        qubit_to_meas = self.get_qubit_indices(1)
        self.apply(instruction=INSTR_ROT_X, qubit_indices=qubit_to_meas, angle=x_rotation_angle_1)
        self.apply(instruction=INSTR_ROT_Y, qubit_indices=qubit_to_meas, angle=y_rotation_angle)
        self.apply(instruction=INSTR_ROT_X, qubit_indices=qubit_to_meas, angle=x_rotation_angle_2)
        self.apply(instruction=INSTR_MEASURE, qubit_indices=qubit_to_meas, output_key="outcome")
        yield self.run()


class GenericBellStateMeasurementProgram(IBellStateMeasurementProgram):
    """
    QuantumProgram that represents a quantum circuit which measures
    in the Bell basis by applying a CNOT and a Hadamard, followed
    by measuring both qubits in the standard basis.
    """

    _OUTCOME_TO_BELL_INDEX = {(0, 0): 0, (0, 1): 1, (1, 0): 2, (1, 1): 3}

    def program(self):
        control, target = self.get_qubit_indices(2)
        self.apply(INSTR_CNOT, [control, target])
        self.apply(INSTR_H, control)
        for (qubit, name) in [
            (control, self._NAME_OUTCOME_CONTROL),
            (target, self._NAME_OUTCOME_TARGET),
        ]:
            self.apply(INSTR_MEASURE, qubit, name, inplace=False)
        yield self.run()


class GenericBellStateMeasurementProgramWithBellIndex(GenericBellStateMeasurementProgram):
    _translate = [BellIndex.PHI_PLUS, BellIndex.PSI_PLUS, BellIndex.PHI_MINUS, BellIndex.PSI_MINUS]

    @property
    def outcome_as_netsquid_bell_index(self):
        old_bell_index = self.get_outcome_as_bell_index
        return self._translate[old_bell_index]


class GenericMeasureService(ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on an generic node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the measurement is registered at the entanglement tracker.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'GenericNodeMeasureService'.

    """

    def __init__(self, node, name="GenericMeasureService"):
        super().__init__(node=node, name=name, meas_prog=GenericMeasureProgram)


class GenericSwapService(ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on an generic node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the swap is registered at the entanglement tracker.

    If a discard is registered at the local `EntanglementTrackerService` while the swap program is being executed,
    the swap is considered to have failed, and all participating qubits are discarded (if they weren't already).

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'GenericSwapService'.

    """

    def __init__(self, node, name="GenericSwapService"):
        super().__init__(
            node=node, name=name, swap_prog=GenericBellStateMeasurementProgramWithBellIndex
        )
