from netsquid.components import INSTR_INIT, INSTR_MEASURE, INSTR_ROT_X, INSTR_ROT_Y, QuantumProgram
from netsquid_driver.operation_services import (
    ProcessingNodeMeasureService,
    ProcessingNodeSwapService,
)

from netsquid_netbuilder.modules.qdevices.nv_move_cirquits import move_using_CXDirections
from netsquid_netbuilder.modules.qdevices.nv_rotated_bell_measurement import (
    apply_rotated_bell_state_measurement,
    convert_rotated_bell_state_circuit_outcomes_to_bell_index,
)

ELECTRON_POSITION = 0


class NVMeasureProgram(QuantumProgram):
    """Implements arbitrary-basis single-qubit measurement circuit on an NV quantum processor.

    The measurement is done in any desired basis by first performing a rotation according to the
    specified Euler angles.
    """

    def program(self, x_rotation_angle_1, y_rotation_angle, x_rotation_angle_2):
        [qubit_to_meas] = self.get_qubit_indices(1)
        if qubit_to_meas != ELECTRON_POSITION:
            raise ValueError("Only the electron can be measured")

        self.apply(instruction=INSTR_ROT_X, qubit_indices=qubit_to_meas, angle=x_rotation_angle_1)
        self.apply(instruction=INSTR_ROT_Y, qubit_indices=qubit_to_meas, angle=y_rotation_angle)
        self.apply(instruction=INSTR_ROT_X, qubit_indices=qubit_to_meas, angle=x_rotation_angle_2)
        self.apply(instruction=INSTR_MEASURE, qubit_indices=qubit_to_meas, output_key="outcome")

        yield self.run()


class NVMeasureService(ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on an NV node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the measurement is registered at the entanglement tracker.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'NVMeasureService'.

    """

    def __init__(self, node, name="NVMeasureService"):
        super().__init__(node=node, name=name, meas_prog=NVMeasureProgram)


class NVSwapService(ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on an NV node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the swap is registered at the entanglement tracker.

    If a discard is registered at the local `EntanglementTrackerService` while the swap program is being executed,
    the swap is considered to have failed, and all participating qubits are discarded (if they weren't already).

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'NVSwapService'.

    """

    class NVSwapProgram(QuantumProgram):
        def program(self):
            electron_pos, carbon_pos = self.get_qubit_indices(2)

            apply_rotated_bell_state_measurement(
                self, electron_position=electron_pos, carbon_position=carbon_pos, inplace=False
            )
            yield self.run()

            self.output["bell_index"] = convert_rotated_bell_state_circuit_outcomes_to_bell_index(
                self
            )
            yield self.run()

        @property
        def outcome_as_netsquid_bell_index(self):  # required by the way service is set up
            return self.output["bell_index"]

    def __init__(self, node):
        super().__init__(node=node, name="NVSwapService", swap_prog=self.NVSwapProgram)

    def _perform_swap(self):
        if self._mem_pos_1 != ELECTRON_POSITION:
            raise ValueError(
                "For NV, the first qubit of the Bell-state measurement should be the electron spin"
            )
        return super()._perform_swap()


class NVMoveProgram(QuantumProgram):
    def program(self):
        electron, carbon = self.get_qubit_indices(2)
        self.apply(INSTR_INIT, [carbon])
        move_using_CXDirections(qprogram=self, control_position=electron, target_position=carbon)
        yield self.run()
