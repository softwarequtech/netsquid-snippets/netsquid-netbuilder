from netsquid.components.cchannel import ClassicalChannel
from netsquid.nodes import Node
from netsquid.nodes.connections import DirectConnection

from netsquid_netbuilder.modules.clinks.interface import ICLinkBuilder, ICLinkConfig


class InstantCLinkConfig(ICLinkConfig):
    """Configuration for instant classical links.

    This model provides instant classical messaging between nodes.
    """

    pass


class InstantCLinkBuilder(ICLinkBuilder):
    @classmethod
    def build(cls, node1: Node, node2: Node, clink_cfg: InstantCLinkConfig) -> DirectConnection:

        channel1to2 = ClassicalChannel(name=f"Default channel {node1.name} to {node2.name}")
        channel2to1 = ClassicalChannel(name=f"Default channel {node2.name} to {node1.name}")

        conn = DirectConnection(
            name=f"Connection {node1.name} - {node2.name}",
            channel_AtoB=channel1to2,
            channel_BtoA=channel2to1,
        )
        return conn
