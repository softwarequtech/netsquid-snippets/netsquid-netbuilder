from __future__ import annotations

import copy
from typing import Optional

from netsquid.components.cchannel import ClassicalChannel
from netsquid.components.models.delaymodels import FixedDelayModel
from netsquid.nodes import Node
from netsquid.nodes.connections import DirectConnection

from netsquid_netbuilder.modules.clinks.interface import ICLinkBuilder, ICLinkConfig


class DefaultCLinkConfig(ICLinkConfig):
    """
    Configuration for default classical links.

    This model delays classical messages with the given delay, or calculates it based on distance
    and the speed of light.
    """

    delay: Optional[float] = None
    """The delay [ns] for the classical message to propagate through the clink."""
    length: Optional[float] = None
    """The length [km] of the clink. If provided, the delay will be calculated based on this and the speed of light."""
    speed_of_light: float = 200000
    """The speed of light [km/s] in the fiber. Default is 200000 km/s."""


class DefaultCLinkBuilder(ICLinkBuilder):
    @classmethod
    def build(cls, node1: Node, node2: Node, clink_cfg: DefaultCLinkConfig) -> DirectConnection:
        clink_cfg = cls._pre_process_config(clink_cfg)

        channel1to2 = ClassicalChannel(
            name=f"Default channel {node1.name} to {node2.name}",
            models={"delay_model": FixedDelayModel(delay=clink_cfg.delay)},
        )
        channel2to1 = ClassicalChannel(
            name=f"Default channel {node2.name} to {node1.name}",
            models={"delay_model": FixedDelayModel(delay=clink_cfg.delay)},
        )

        conn = DirectConnection(
            name=f"Connection {node1.name} - {node2.name}",
            channel_AtoB=channel1to2,
            channel_BtoA=channel2to1,
        )
        return conn

    @classmethod
    def _pre_process_config(cls, clink_cfg: DefaultCLinkConfig) -> DefaultCLinkConfig:
        if isinstance(clink_cfg, dict):
            clink_cfg = DefaultCLinkConfig(**clink_cfg)
        else:
            clink_cfg = copy.deepcopy(clink_cfg)
        if clink_cfg.delay is None and (
            clink_cfg.length is None or clink_cfg.speed_of_light is None
        ):
            raise ValueError(
                f"{cls.__name__} model config requires a delay"
                f" or distance with speed of light specification"
            )
        if clink_cfg.delay is not None and clink_cfg.length is not None:
            raise ValueError(
                f"{cls.__name__} model config can only use delay or distance, but both where specified"
            )
        if clink_cfg.length is not None:
            clink_cfg.delay = clink_cfg.length / clink_cfg.speed_of_light * 1e9
        return clink_cfg
