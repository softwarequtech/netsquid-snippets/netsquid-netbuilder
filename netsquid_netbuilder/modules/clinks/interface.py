from abc import ABC, abstractmethod

from netsquid.nodes import Node
from netsquid.nodes.connections import DirectConnection

from netsquid_netbuilder.yaml_loadable import YamlLoadable


class ICLinkConfig(YamlLoadable, ABC):
    """
    Abstract base class for classical link configuration.

    This class is used for defining configuration options for classical links.
    Subclasses should provide specific configuration options.
    """

    pass


class ICLinkBuilder(ABC):
    """
    Abstract base class for classical link builders.

    This class defines the interface for building classical links.
    """

    @classmethod
    @abstractmethod
    def build(cls, node1: Node, node2: Node, clink_cfg: ICLinkConfig) -> DirectConnection:
        """
        Build a classical link.

        This method is called to build a classical link between two nodes.

        :param node1: The first Node involved in the link.
        :param node2: The second Node involved in the link.
        :param clink_cfg: The configuration for the classical link.
        :return: An instance of the built classical link.
        """
