from __future__ import annotations

import copy
from typing import Optional

from netsquid_magic.link_layer import (
    MagicLinkLayerProtocolWithSignaling,
    SingleClickTranslationUnit,
)
from netsquid_magic.magic_distributor import SingleClickMagicDistributor
from netsquid_magic.model_parameters import SingleClickModelParameters

from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.modules.qlinks.util import (
    set_length_side_parameters_from_config,
    set_side_parameter_default,
    set_side_parameters_from_config,
    set_side_parameters_from_qmemory,
)
from netsquid_netbuilder.network import QDeviceNode


class HeraldedSingleClickQLinkConfig(IQLinkConfig):
    """
    Configuration for single click quantum links.

    The heralded link uses a model with both nodes connected by fiber to a midpoint station with a
    Bell-state measurement detector.
    The nodes repeatedly send out entangled photons and, on a successful measurement at the midpoint,
    the midpoint station will send out a signal to both nodes, heralding successful entanglement.

    This model uses the single click model as developed and described by:
    https://arxiv.org/abs/2207.10579 (Appendix E: single-click model)

    .. warning::

        The parameters `bright_state_parameter_A` and `bright_state_parameter_B` behave differently
        than other parameters. These parameters determine the fraction of the matter qubit state
        that is in the :math:`|1⟩` state. Excitation and subsequent radiative decay of the matter's :math:`|1⟩`
        state entangles the state of the matter qubit with the presence :math:`|1⟩` or absence :math:`|0⟩` of a photon.

        In the absense of other noise and loss, increasing this parameter improves the odds of "successful"
        entanglement, but decreases the fidelity of that entanglement.

        For direct qlinks the parameter will be calculated based on `minimum_fidelity` of the entanglement request.
        The simulation will use this calculated value only if this parameter was not specified
        in the qlink model or via `external_params` in a qdevice configuration.
        For repeater chains this parameter calculation is not implemented, thus this parameter must be specified
        qlink model or via `external_params` in a qdevice configuration in order for the simulation to work.

    .. _single-click-length:

    Length
    ---------
    This model simulates a heralded link with a midpoint station.
    The lengths from the nodes to the midpoint station are set via the parameters `length_A` and `length_B`,
    but if desired, the parameter `length` can be used to set the total length and
    this will make the "A" and "B" sides equally long.

    .. _single-click-per_side_parameter:

    Per side parameter
    ---------------------

    The parameters p_loss_init, p_loss_length and speed_of_light can be specified
    per side of the heralded link. These parameters have a "global" parameter and per side parameters with _A and _B
    extensions. The model will use the per side parameters,
    but for easier usage, the input can be set to the "global" parameter that will
    be distributed to each of the sides if their per side value is missing.

    .. _single-click-qdevice_parameter:

    Qdevice parameter
    ---------------------

    The parameters emission_duration, bright_state_parameter and collection_efficiency originate from the qdevice used,
    but affect the entanglement generation that is simulated in the qlink.
    These parameters have a "global" parameter and per side parameters with _A and _B
    extensions. The model will only use the per side parameters,
    but for easier usage, the input can be set to the "global" parameter that will
    be distributed to each of the sides if their per side value is missing.

    The value of these parameters will be retrieved in the following order:

    1) Directly from this configuration.
       If either the "global" parameter or both per side parameters have been specified.
    2) From the qdevice properties.
       This will assign the parameter to either the "A" or "B" side that the qdevice is connected to.
       To add a property to a qdevice, add it to the `external_params` in a qdevice configuration.
    3) From the defaults.
    """

    length: Optional[float] = None
    """Total length [km] of fiber. For more information, see: :ref:`single-click-length`."""
    p_loss_init: Optional[float] = None
    """Probability that photons are lost when entering connection. This is a :ref:`single-click-per_side_parameter`."""
    p_loss_length: Optional[float] = None
    """Attenuation coefficient [dB/km] of the fiber. This is a :ref:`single-click-per_side_parameter`."""
    speed_of_light: Optional[float] = None
    """Speed of light [km/s] in fiber of the heralded connection. This is a :ref:`single-click-per_side_parameter`."""
    dark_count_probability: float = 0
    """Dark-count probability per detection"""
    detector_efficiency: float = 1
    """Probability that the presence of a photon leads to a detection event"""
    visibility: float = 1
    """Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability)"""
    num_resolving: bool = False
    """Determines whether photon-number-resolving detectors are used for the Bell-state measurement"""
    emission_duration: Optional[float] = None
    """Time [ns] it takes the memory to emit a photon that is entangled with a memory qubit.
    This is a :ref:`single-click-qdevice_parameter`."""
    collection_efficiency: Optional[float] = None
    """Chance of collecting the entangled photon emitted by a memory qubit.
     This parameter wil only rescale `p_loss_init` in this model.
     This is a :ref:`single-click-qdevice_parameter`."""
    bright_state_parameter: Optional[float] = None
    """Determines the fraction of the matter qubit state that is in the :math:`|1⟩` state.
    Excitation and subsequent radiative decay of the matter's :math:`|1⟩` state entangles the state of the
    matter qubit with the presence :math:`|1⟩` or absence :math:`|0⟩` of a photon.

    This is a :ref:`single-click-qdevice_parameter`, but can be left unspecified for direct qlinks.
    For direct qlinks the parameter is calculated based on `minimum_fidelity` of the entanglement request
    if this was parameter was not specified.
    For repeater chains this parameter must be specified.
    """

    length_A: Optional[float] = None
    """Length [km] of "A" side of the fiber. For more information, see: :ref:`single-click-length`."""
    length_B: Optional[float] = None
    """Length [km] of "B" side of the fiber. For more information, see: :ref:`single-click-length`."""

    p_loss_init_A: Optional[float] = None
    p_loss_length_A: Optional[float] = None
    speed_of_light_A: Optional[float] = None
    emission_duration_A: Optional[float] = None
    collection_efficiency_A: Optional[float] = None
    bright_state_parameter_A: Optional[float] = None

    p_loss_init_B: Optional[float] = None
    p_loss_length_B: Optional[float] = None
    speed_of_light_B: Optional[float] = None
    emission_duration_B: Optional[float] = None
    collection_efficiency_B: Optional[float] = None
    bright_state_parameter_B: Optional[float] = None

    @staticmethod
    def _get_defaults():
        """Get default values for the configuration."""
        return {
            "p_loss_init": 0,
            "p_loss_length": 0.2,
            "speed_of_light": 200000,
            "emission_duration": 0,
            "collection_efficiency": 1,
        }


class HeraldedSingleClickQLinkBuilder(IQLinkBuilder):
    @classmethod
    def build(
        cls, node1: QDeviceNode, node2: QDeviceNode, qlink_cfg: HeraldedSingleClickQLinkConfig
    ) -> MagicLinkLayerProtocolWithSignaling:
        qlink_cfg = cls._pre_process_config(qlink_cfg, node1, node2)

        model_params = cls._convert_config_object_to_model_params(qlink_cfg)

        link_dist = SingleClickMagicDistributor([node1, node2], model_params=model_params)
        link_prot = MagicLinkLayerProtocolWithSignaling(
            nodes=[node1, node2],
            magic_distributor=link_dist,
            translation_unit=SingleClickTranslationUnit(),
        )
        return link_prot

    @classmethod
    def _pre_process_config(
        cls, qlink_cfg: HeraldedSingleClickQLinkConfig, node1: QDeviceNode, node2: QDeviceNode
    ) -> HeraldedSingleClickQLinkConfig:
        if isinstance(qlink_cfg, dict):
            qlink_cfg = HeraldedSingleClickQLinkConfig(**qlink_cfg)
        else:
            qlink_cfg = copy.deepcopy(qlink_cfg)

        set_length_side_parameters_from_config(qlink_cfg)

        # Try to set side values from config
        side_params = [
            "p_loss_init",
            "p_loss_length",
            "speed_of_light",
            "emission_duration",
            "collection_efficiency",
            "bright_state_parameter",
        ]
        for side_param in side_params:
            set_side_parameters_from_config(qlink_cfg, side_param)

        # Try to set values from qmemory properties
        qmem_a = node1.qmemory
        qmem_b = node2.qmemory
        qmem_params = ["emission_duration", "collection_efficiency", "bright_state_parameter"]
        for qmem_param in qmem_params:
            set_side_parameters_from_qmemory(qlink_cfg, qmem_param, qmem_a, qmem_b)

        # Fallback to defaults
        defaults_dict = HeraldedSingleClickQLinkConfig._get_defaults()
        for side_param in defaults_dict.keys():
            set_side_parameter_default(qlink_cfg, side_param, defaults_dict[side_param])

        return qlink_cfg

    @classmethod
    def _convert_config_object_to_model_params(
        cls, cfg: HeraldedSingleClickQLinkConfig
    ) -> SingleClickModelParameters:
        model = SingleClickModelParameters()
        model.cycle_time = None
        params_to_copy_over = [
            "length_A",
            "length_B",
            "p_loss_init_A",
            "p_loss_init_B",
            "p_loss_length_A",
            "p_loss_length_B",
            "speed_of_light_A",
            "speed_of_light_B",
            "emission_duration_A",
            "emission_duration_B",
            "dark_count_probability",
            "detector_efficiency",
            "visibility",
            "num_resolving",
            "collection_efficiency_A",
            "collection_efficiency_B",
            "bright_state_parameter_A",
            "bright_state_parameter_B",
        ]
        for param_name in params_to_copy_over:
            assert param_name in model.__dict__.keys()
            assert param_name in cfg.__dict__.keys()
            val = cfg.__getattribute__(param_name)
            model.__setattr__(param_name, val)

        return model
