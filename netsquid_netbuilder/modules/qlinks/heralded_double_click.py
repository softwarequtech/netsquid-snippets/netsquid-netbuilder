from __future__ import annotations

import copy
from typing import Optional

from netsquid_magic.link_layer import (
    MagicLinkLayerProtocolWithSignaling,
    SingleClickTranslationUnit,
)
from netsquid_magic.magic_distributor import DoubleClickMagicDistributor
from netsquid_magic.model_parameters import DoubleClickModelParameters

from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.modules.qlinks.util import (
    set_length_side_parameters_from_config,
    set_side_parameter_default,
    set_side_parameters_from_config,
    set_side_parameters_from_qmemory,
)
from netsquid_netbuilder.network import QDeviceNode


class HeraldedDoubleClickQLinkConfig(IQLinkConfig):
    """
    Heralded double-click quantum link model.

    The heralded link uses a model with both nodes connected by fiber to a midpoint station with a
    Bell-state measurement detector.
    The nodes repeatedly send out entangled photons and, on a successful measurement at the midpoint,
    the midpoint station will send out a signal to both nodes, heralding successful entanglement.

    This model uses the double click model as developed and described by:
    https://arxiv.org/abs/2207.10579 (Appendix C: Double-click model)

    .. _double-click-length:

    Length
    ---------
    This model simulates a heralded link with a midpoint station.
    The lengths from the nodes to the midpoint station are set via the parameters `length_A` and `length_B`,
    but if desired, the parameter `length` can be used to set the total length and
    this will make the "A" and "B" sides equally long.

    .. _double-click-per_side_parameter:

    Per side parameter
    ---------------------

    The parameters p_loss_init, p_loss_length and speed_of_light can be specified
    per side of the heralded link. These parameters have a "global" parameter and per side parameters with _A and _B
    extensions. The model will use the per side parameters,
    but for easier usage, the input can be set to the "global" parameter that will
    be distributed to each of the sides if their per side value is missing.

    .. _double-click-qdevice_parameter:

    Qdevice parameter
    ---------------------

    The parameters emission_fidelity, emission_duration and collection_efficiency originate from the qdevice used,
    but affect the entanglement generation that is simulated in the qlink.
    These parameters have a "global" parameter and per side parameters with _A and _B
    extensions. The model will only use the per side parameters,
    but for easier usage, the input can be set to the "global" parameter that will
    be distributed to each of the sides if their per side value is missing.

    The value of these parameters will be retrieved in the following order:

    1) Directly from this configuration.
       If either the "global" parameter or both per side parameters have been specified.
    2) From the qdevice properties.
       This will assign the parameter to either the "A" or "B" side that the qdevice is connected to.
       To add a property to a qdevice, add it to the `external_params` in a qdevice configuration.
    3) From the defaults.

    """

    length: Optional[float] = None
    """Total length [km] of fiber. For more information, see: :ref:`double-click-length`."""
    p_loss_init: Optional[float] = None
    """Probability that photons are lost when entering connection. This is a :ref:`double-click-per_side_parameter`."""
    p_loss_length: Optional[float] = None
    """Attenuation coefficient [dB/km] of the fiber. This is a :ref:`double-click-per_side_parameter`."""
    speed_of_light: Optional[float] = None
    """Speed of light [km/s] in fiber of the heralded connection. This is a :ref:`double-click-per_side_parameter`."""
    emission_fidelity: Optional[float] = None
    """
    Fidelity of state shared between photon and memory qubit compared to
    :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission.
    This is a :ref:`double-click-qdevice_parameter`.
    """
    dark_count_probability: float = 0
    """Dark-count probability per detection"""
    detector_efficiency: float = 1
    """Probability that the presence of a photon leads to a detection event"""
    visibility: float = 1
    """Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability)"""
    num_resolving: bool = False
    """Determines whether photon-number-resolving detectors are used for the Bell-state measurement"""
    coin_prob_ph_ph: float = 1
    """Coincidence probability for two photons. When using a coincidence time window in the double-click protocol,
    two clicks are only accepted if they occurred within one coincidence time window away from each other.
    This parameter is the probability that if both clicks are photon detections,
    they are within one coincidence window. In general, this depends not only on the size of the coincidence
    time window, but also on the state of emitted photons and the total detection time window. Defaults to 1."""
    coin_prob_ph_dc: float = 1
    """Coincidence probability for a photon and a dark count.
    When using a coincidence time window in the double-click protocol,
    two clicks are only accepted if they occurred within one coincidence time window away from each other.
    This parameter is the probability that if one click is a photon detection and the other a dark count,
    they are within one coincidence window. In general, this depends not only on the size of the coincidence
    time window, but also on the state of emitted photons and the total detection time window. Defaults to 1."""
    coin_prob_dc_dc: float = 1
    """Coincidence probability for two dark counts. When using a coincidence time window in the double-click protocol,
    two clicks are only accepted if they occurred within one coincidence time window away from each other.
    This parameter is the probability that if both clicks are dark counts,
    they are within one coincidence window. In general, this depends on the size of the coincidence time window
    and the total detection time window. Defaults to 1."""
    num_multiplexing_modes: int = 1
    """Number of modes used for multiplexing, i.e. how many entanglement generation attempts are made per round."""
    emission_duration: Optional[float] = None
    """Time in nanoseconds it takes the memory to emit a photon that is entangled with a memory qubit.
    This is a :ref:`double-click-qdevice_parameter`."""
    collection_efficiency: Optional[float] = None
    """Chance of collecting the entangled photon emitted by a memory qubit.
    This parameter wil only rescale `p_loss_init` in this model.
    This is a :ref:`double-click-qdevice_parameter`."""

    length_A: Optional[float] = None
    """Length [km] of "A" side of the fiber. For more information, see: :ref:`double-click-length`."""
    length_B: Optional[float] = None
    """Length [km] of "B" side of the fiber. For more information, see: :ref:`double-click-length`."""

    p_loss_init_A: Optional[float] = None
    p_loss_length_A: Optional[float] = None
    speed_of_light_A: Optional[float] = None
    emission_fidelity_A: Optional[float] = None
    emission_duration_A: Optional[float] = None
    collection_efficiency_A: Optional[float] = None

    p_loss_init_B: Optional[float] = None
    p_loss_length_B: Optional[float] = None
    speed_of_light_B: Optional[float] = None
    emission_fidelity_B: Optional[float] = None
    emission_duration_B: Optional[float] = None
    collection_efficiency_B: Optional[float] = None

    @staticmethod
    def _get_defaults():
        """Get default values for the configuration."""
        return {
            "p_loss_init": 0,
            "p_loss_length": 0.2,
            "speed_of_light": 200000,
            "emission_fidelity": 1,
            "emission_duration": 0,
            "collection_efficiency": 1,
        }


class HeraldedDoubleClickQLinkBuilder(IQLinkBuilder):
    @classmethod
    def build(
        cls, node1: QDeviceNode, node2: QDeviceNode, qlink_cfg: HeraldedDoubleClickQLinkConfig
    ) -> MagicLinkLayerProtocolWithSignaling:
        qlink_cfg = cls._process_config(qlink_cfg, node1, node2)

        model_params = cls._convert_config_object_to_model_params(qlink_cfg)

        link_dist = DoubleClickMagicDistributor([node1, node2], model_params=model_params)
        link_prot = MagicLinkLayerProtocolWithSignaling(
            nodes=[node1, node2],
            magic_distributor=link_dist,
            translation_unit=SingleClickTranslationUnit(),
        )
        return link_prot

    @classmethod
    def _process_config(
        cls, qlink_cfg: HeraldedDoubleClickQLinkConfig, node1: QDeviceNode, node2: QDeviceNode
    ) -> HeraldedDoubleClickQLinkConfig:

        if isinstance(qlink_cfg, dict):
            qlink_cfg = HeraldedDoubleClickQLinkConfig(**qlink_cfg)
        else:
            qlink_cfg = copy.deepcopy(qlink_cfg)

        # set lengths
        set_length_side_parameters_from_config(qlink_cfg)

        # Try to set side values from config
        side_params = [
            "p_loss_init",
            "p_loss_length",
            "speed_of_light",
            "emission_fidelity",
            "emission_duration",
            "collection_efficiency",
        ]
        for side_param in side_params:
            set_side_parameters_from_config(qlink_cfg, side_param)

        # Try to set side values from qmemory properties
        qmem_a = node1.qmemory
        qmem_b = node2.qmemory
        qmem_params = ["emission_fidelity", "emission_duration", "collection_efficiency"]
        for qmem_param in qmem_params:
            set_side_parameters_from_qmemory(qlink_cfg, qmem_param, qmem_a, qmem_b)

        # Fallback to defaults for side params
        defaults_dict = HeraldedDoubleClickQLinkConfig._get_defaults()
        for side_param in side_params:
            set_side_parameter_default(qlink_cfg, side_param, defaults_dict[side_param])

        return qlink_cfg

    @classmethod
    def _convert_config_object_to_model_params(
        cls, cfg: HeraldedDoubleClickQLinkConfig
    ) -> DoubleClickModelParameters:
        model = DoubleClickModelParameters()
        model.cycle_time = None
        params_to_copy_over = [
            "length_A",
            "length_B",
            "p_loss_init_A",
            "p_loss_init_B",
            "p_loss_length_A",
            "p_loss_length_B",
            "speed_of_light_A",
            "speed_of_light_B",
            "emission_duration_A",
            "emission_duration_B",
            "emission_fidelity_A",
            "emission_fidelity_B",
            "num_multiplexing_modes",
            "dark_count_probability",
            "detector_efficiency",
            "visibility",
            "num_resolving",
            "coin_prob_ph_ph",
            "coin_prob_ph_dc",
            "coin_prob_dc_dc",
            "collection_efficiency_A",
            "collection_efficiency_B",
        ]
        for param_name in params_to_copy_over:
            assert param_name in model.__dict__.keys(), f"No parameter {param_name}"
            assert param_name in cfg.__dict__.keys()
            val = cfg.__getattribute__(param_name)
            model.__setattr__(param_name, val)

        return model
