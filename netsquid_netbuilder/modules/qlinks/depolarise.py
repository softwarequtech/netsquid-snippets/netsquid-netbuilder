from __future__ import annotations

import copy
from typing import Optional

from netsquid_magic.link_layer import (
    MagicLinkLayerProtocolWithSignaling,
    SingleClickTranslationUnit,
)
from netsquid_magic.magic_distributor import DepolariseWithFailureMagicDistributor
from netsquid_magic.model_parameters import DepolariseModelParameters

from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.network import QDeviceNode
from netsquid_netbuilder.util.fidelity import fidelity_to_prob_max_mixed


class DepolariseQLinkConfig(IQLinkConfig):
    """Depolarising model config.

    Depolarising quantum links are used to generate entangled pairs with a specified fidelity
    and probability of success. The noise in the generated entangled pairs is depolarising noise.

    The time of a cycle is either explicitly given or calculated by using the distance and speed of light.
    """

    fidelity: float
    """Fidelity of successfully generated EPR pairs."""
    prob_success: float
    """Probability of successfully generating an EPR pair per cycle."""
    t_cycle: Optional[float] = None
    """Duration of a cycle. [ns]"""
    length: Optional[float] = None
    """length of the link. Will be used to calculate t_cycle with t_cycle=length/speed_of_light. [km]"""
    speed_of_light: float = 200000
    """Speed of light in the optical fiber connecting the two nodes. [km/s]"""
    random_bell_state: bool = False
    """Determines whether the Bell state is always phi+ or randomly chosen from phi+, phi-, psi+, or psi-."""


class DepolariseQLinkBuilder(IQLinkBuilder):
    @classmethod
    def build(
        cls, node1: QDeviceNode, node2: QDeviceNode, qlink_cfg: DepolariseQLinkConfig
    ) -> MagicLinkLayerProtocolWithSignaling:
        qlink_cfg = cls._pre_process_config(qlink_cfg)

        model_params = cls._convert_config_object_to_model_params(qlink_cfg)
        link_dist = DepolariseWithFailureMagicDistributor(
            nodes=[node1, node2], model_params=model_params
        )

        link_prot = MagicLinkLayerProtocolWithSignaling(
            nodes=[node1, node2],
            magic_distributor=link_dist,
            translation_unit=SingleClickTranslationUnit(),
        )
        return link_prot

    @classmethod
    def _pre_process_config(cls, qlink_cfg: DepolariseQLinkConfig) -> DepolariseQLinkConfig:
        if isinstance(qlink_cfg, dict):
            qlink_cfg = DepolariseQLinkConfig(**qlink_cfg)
        else:
            qlink_cfg = copy.deepcopy(qlink_cfg)

        if qlink_cfg.t_cycle is None and (
            qlink_cfg.length is None or qlink_cfg.speed_of_light is None
        ):
            raise ValueError(
                f"{cls.__name__} model config requires a t_cycle"
                f" or distance with speed of light specification"
            )
        if qlink_cfg.t_cycle is not None and qlink_cfg.length is not None:
            raise ValueError(
                f"{cls.__name__} model config can only use t_cycle or distance, but both where specified"
            )
        if qlink_cfg.length is not None:
            qlink_cfg.t_cycle = qlink_cfg.length / qlink_cfg.speed_of_light * 1e9
        return qlink_cfg

    @classmethod
    def _convert_config_object_to_model_params(
        cls, cfg: DepolariseQLinkConfig
    ) -> DepolariseModelParameters:
        model = DepolariseModelParameters()
        model.cycle_time = cfg.t_cycle
        model.prob_max_mixed = fidelity_to_prob_max_mixed(cfg.fidelity)
        model.prob_success = cfg.prob_success
        model.random_bell_state = cfg.random_bell_state
        return model
