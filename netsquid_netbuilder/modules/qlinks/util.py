import logging
from typing import Any

from netsquid.components.qmemory import QuantumMemory

from netsquid_netbuilder.modules.qlinks.interface import IQLinkConfig

logger = logging.getLogger(__name__)


def set_side_parameters_from_config(config: IQLinkConfig, param_key: str):
    param_key_a = param_key + "_A"
    param_key_b = param_key + "_B"
    value = getattr(config, param_key)

    if value is None:
        return

    if getattr(config, param_key_a) is None:
        setattr(config, param_key_a, value)
    if getattr(config, param_key_b) is None:
        setattr(config, param_key_b, value)


def set_side_parameters_from_qmemory(
    config: IQLinkConfig, param_key: str, qmem_a: QuantumMemory, qmem_b: QuantumMemory
):
    param_key_a = param_key + "_A"
    param_key_b = param_key + "_B"

    qmem_a_val = qmem_a.properties.get(param_key)
    qmem_b_val = qmem_b.properties.get(param_key)

    error_msg = (
        f"The parameter {param_key} for link type {type(config)}"
        f" should only be defined once. Either in the link model or in the node configuration."
    )

    if qmem_a_val:
        if getattr(config, param_key_a) is not None:
            raise ValueError(error_msg)
        setattr(config, param_key_a, qmem_a_val)
    if qmem_b_val:
        if getattr(config, param_key_b) is not None:
            raise ValueError(error_msg)
        setattr(config, param_key_b, qmem_b_val)


def set_side_parameter_default(config: IQLinkConfig, param_key: str, default_value: Any):
    param_key_a = param_key + "_A"
    param_key_b = param_key + "_B"

    log_msg = f"Using default value: {default_value} for parameter {param_key} in link model: {type(config)}"

    if getattr(config, param_key_a) is None:
        logger.info(log_msg)
        setattr(config, param_key_a, default_value)
    if getattr(config, param_key_b) is None:
        logger.info(log_msg)
        setattr(config, param_key_b, default_value)


def set_length_side_parameters_from_config(config: IQLinkConfig):
    param_key_a = "length_A"
    param_key_b = "length_B"
    default = getattr(config, "length")

    if getattr(config, param_key_a) is None:
        setattr(config, param_key_a, default / 2)
    if getattr(config, param_key_b) is None:
        setattr(config, param_key_b, default / 2)
