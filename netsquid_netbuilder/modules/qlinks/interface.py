from abc import ABC, abstractmethod

from netsquid_magic.link_layer import MagicLinkLayerProtocolWithSignaling

from netsquid_netbuilder.network import QDeviceNode
from netsquid_netbuilder.yaml_loadable import YamlLoadable


class IQLinkConfig(YamlLoadable, ABC):
    """
    Abstract base class for qlink configuration.

    This class is used for defining qlink configurations. It is expected to be subclassed
    to provide specific configuration options for different types of qlinks.
    """

    pass


class IQLinkBuilder(ABC):
    """
    Abstract base class for qlink builders.

    This class defines the interface for building qlinks.
    """

    @classmethod
    @abstractmethod
    def build(
        cls, node1: QDeviceNode, node2: QDeviceNode, qlink_cfg: IQLinkConfig
    ) -> MagicLinkLayerProtocolWithSignaling:
        """
        Build a link layer protocol.

        This method is called to build a link layer protocol based on the provided parameters.

        :param node1: The first QDeviceNode involved in the link.
        :param node2: The second QDeviceNode involved in the link.
        :param qlink_cfg: The configuration for the link.
        :return: An instance of the built link layer protocol.
        """
