from __future__ import annotations

import copy
from typing import Optional

from netsquid_magic.link_layer import (
    MagicLinkLayerProtocolWithSignaling,
    SingleClickTranslationUnit,
)
from netsquid_magic.magic_distributor import PerfectStateMagicDistributor
from netsquid_magic.model_parameters import PerfectModelParameters

from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.network import QDeviceNode


class PerfectQLinkConfig(IQLinkConfig):
    """Configuration for perfect quantum links.

    This model generates perfect EPR pairs after a delay, calculated either by explicitly
    setting the delay or by using the distance and speed of light.
    """

    state_delay: Optional[float] = None
    """Time [ns] to complete entanglement generation. """
    length: Optional[float] = None
    """Length [km] of the link. Will be used to calculate state_delay with state_delay=length/speed_of_light. """
    speed_of_light: float = 200000
    """Speed of light [km/s] in the optical fiber connecting the two nodes. """

    def set_default(self):
        """Set default values for the configuration."""
        self.state_delay = 1000.0


class PerfectQLinkBuilder(IQLinkBuilder):
    @classmethod
    def build(
        cls, node1: QDeviceNode, node2: QDeviceNode, qlink_cfg: PerfectQLinkConfig
    ) -> MagicLinkLayerProtocolWithSignaling:
        qlink_cfg = cls._pre_process_config(qlink_cfg)

        model_params = cls._convert_config_object_to_model_params(qlink_cfg)

        link_dist = PerfectStateMagicDistributor(nodes=[node1, node2], model_params=model_params)
        link_prot = MagicLinkLayerProtocolWithSignaling(
            nodes=[node1, node2],
            magic_distributor=link_dist,
            translation_unit=SingleClickTranslationUnit(),
        )
        return link_prot

    @classmethod
    def _pre_process_config(cls, qlink_cfg: PerfectQLinkConfig) -> PerfectQLinkConfig:
        if isinstance(qlink_cfg, dict):
            qlink_cfg = PerfectQLinkConfig(**qlink_cfg)
        else:
            qlink_cfg = copy.deepcopy(qlink_cfg)

        if qlink_cfg.state_delay is None and qlink_cfg.length is None:
            qlink_cfg.set_default()
        if qlink_cfg.state_delay is not None and qlink_cfg.length is not None:
            raise ValueError(
                f"{cls.__name__} model config can only use t_cycle or distance, but both where specified"
            )
        if qlink_cfg.length is not None:
            qlink_cfg.state_delay = qlink_cfg.length / qlink_cfg.speed_of_light * 1e9
        return qlink_cfg

    @classmethod
    def _convert_config_object_to_model_params(
        cls, cfg: PerfectQLinkConfig
    ) -> PerfectModelParameters:
        model = PerfectModelParameters()
        model.cycle_time = 0
        model.state_delay = cfg.state_delay
        return model
