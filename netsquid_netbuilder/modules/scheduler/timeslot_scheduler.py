from __future__ import annotations

import logging
from abc import ABCMeta, abstractmethod
from collections import namedtuple
from dataclasses import dataclass
from typing import Dict, List, Tuple, Union

from netsquid_magic.qlink import IQLink
from pydynaa import Event, EventHandler, EventType
from qlink_interface import ReqCreateBase, ResError
from qlink_interface.interface import ResCreate

from netsquid_netbuilder.modules.scheduler.interface import IScheduleProtocol

logger = logging.getLogger(__name__)

LinkOpenEvent = EventType("evtLinkOpen", "A qlink is opened")
LinkCloseEvent = EventType("evtLinkClose", "A qlink is closed")


@dataclass
class RequestValue:
    """Dataclass for keeping track of how many qubits each node needs to receive in order to complete the request.

    .. note::
        We track the amount of qubits left per node instead of just qubits for the entire link,
        because if the link is closed when only one of the nodes completes the request,
        the request on the other node could still be incomplete.
        (The other node may complete at the current simulation time, but its event has not been processed yet)
        Closing the link can stop the entanglement generation simulation in such a way that it will not complete
        on the other node, thus necessitating tracking of both nodes, so the link is only closed if both are complete.
    """

    qubits_node1_left: int
    qubits_node2_left: int

    @property
    def done(self) -> bool:
        if self.qubits_node1_left == 0 and self.qubits_node2_left == 0:
            return True
        return False

    def verify(self):
        if self.qubits_node1_left < 0 or self.qubits_node2_left < 0:
            raise RuntimeError("Negative value reached for num qubits left")


RequestKey = namedtuple("RequestKey", "node1, node2, create_id")
"""Key for a request. Use :meth:`get_request_key` to create this key such that it is unique for a node combination."""


def get_request_key(node1_id: int, node2_id: int, create_id: int) -> RequestKey:
    """Construct a unique RequestKey tuple.
    The key is constructed such that the combination of two nodes gives the same key,
    but it does not matter what node is "node1" or "node2"."""
    assert node1_id != node2_id
    n1 = min(node1_id, node2_id)
    n2 = max(node1_id, node2_id)
    return RequestKey(n1, n2, create_id)


@dataclass
class Timeslot:
    """Timeslot to represent a time allocation for entanglement generation between node1 and node2 for
    the duration between start and end."""

    node1_name: str
    """Node name of the first node."""
    node2_name: str
    """Node name of the second node."""
    start_time: float
    """Scheduled time [ns] to start the timeslot and open the link."""
    end_time: Union[float, None]
    """Scheduled time [ns] to end the timeslot, if None the timeslot is open ended.
    (and another mechanism is expected to close the timeslot)"""

    def contains_node(self, node_name: str) -> bool:
        return self.node2_name == node_name or self.node1_name == node_name

    def contains_link(self, node_name_1: str, node_name_2: str) -> bool:
        if self.node2_name == node_name_1 and self.node1_name == node_name_2:
            return True
        if self.node1_name == node_name_1 and self.node2_name == node_name_2:
            return True
        return False


class TimeslotSchedulerProtocol(IScheduleProtocol, metaclass=ABCMeta):
    def __init__(
        self, name: str, qlinks: Dict[Tuple[str, str], IQLink], node_id_mapping: Dict[str, int]
    ):
        """
        Base class for a scheduler meant to be inherited from.
        This class provides the method :meth`register_timeslot` and related infrastructure such that
        any registered timeslots will open the associated link at the start of
        the timeslot and close it at the end of the timeslot.

        :param name: Name of the protocol.
        :param qlinks: A dictionary of qlinks that are managed by this scheduler.
        Keys are tuples of node names representing the two connected nodes.
        :param node_id_mapping: Mapping between node names and IDs in the network. Keys are node names.

        """
        super().__init__()
        self.name = name
        self.qlinks = qlinks
        self._node_id_mapping = node_id_mapping
        self._node_name_mapping = {id_: name for name, id_ in self._node_id_mapping.items()}

        self._evhandler = EventHandler(self._handle_event)
        self._ev_to_timeslot: Dict[Event, Timeslot] = {}
        self.logger = logger.getChild(self.name)

    def start(self):
        super().start()
        self._wait(self._evhandler, entity=self, event_type=LinkOpenEvent)
        self._wait(self._evhandler, entity=self, event_type=LinkCloseEvent)

    def stop(self):
        super().stop()
        self._dismiss(self._evhandler, entity=self, event_type=LinkOpenEvent)
        self._dismiss(self._evhandler, entity=self, event_type=LinkCloseEvent)

    def register_timeslot(self, timeslot: Timeslot):
        """Register a timeslot.
        This will schedule events to open the associated link at the start time and close it at the end time."""
        open_event = self._schedule_at(timeslot.start_time, LinkOpenEvent)
        self._ev_to_timeslot[open_event] = timeslot

        if timeslot.end_time:
            close_event = self._schedule_at(timeslot.end_time, LinkCloseEvent)
            self._ev_to_timeslot[close_event] = timeslot

    @abstractmethod
    def register_request(self, node_id: int, req: ReqCreateBase, create_id: int):
        pass

    @abstractmethod
    def register_result(self, node_id: int, res: ResCreate):
        pass

    @abstractmethod
    def register_error(self, node_id: int, error: ResError):
        pass

    def find_registered_timeslots(self, node_id: int, remote_id: int) -> List[Timeslot]:
        """Find all registered timeslots between two nodes."""
        node_name = self._node_name_mapping[node_id]
        remote_node_name = self._node_name_mapping[remote_id]
        timeslots = [
            timeslot
            for timeslot in self.registered_timeslots
            if timeslot.contains_link(node_name, remote_node_name)
        ]
        return timeslots

    @property
    def registered_timeslots(self):
        return self._ev_to_timeslot.values()

    def _handle_event(self, event):
        if event.type == LinkOpenEvent:
            self._handle_open_link_event(event)
        if event.type == LinkCloseEvent:
            self._handle_close_link_event(event)

    def _handle_open_link_event(self, event):
        timeslot = self._ev_to_timeslot[event]
        self._open_link(timeslot.node1_name, timeslot.node2_name)
        self._ev_to_timeslot.pop(event)

    def _handle_close_link_event(self, event):
        timeslot = self._ev_to_timeslot[event]
        self._close_link(timeslot.node1_name, timeslot.node2_name)
        self._ev_to_timeslot.pop(event)

    def _open_link(self, node1_name: str, node2_name: str):
        self.logger.info(f"Opening link between nodes {node1_name} and {node2_name}")
        self.qlinks[(node1_name, node2_name)].open()

    def _close_link(self, node1_name: str, node2_name: str):
        self.logger.info(f"Closing link between nodes {node1_name} and {node2_name}")
        self.qlinks[(node1_name, node2_name)].close()
