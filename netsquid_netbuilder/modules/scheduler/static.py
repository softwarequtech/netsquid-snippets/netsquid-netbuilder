from __future__ import annotations

import logging
import math
from typing import TYPE_CHECKING, Dict, List, Tuple

import netsquid as ns
from netsquid_magic.qlink import IQLink
from pydynaa import EventHandler
from qlink_interface import ReqCreateBase, ResError
from qlink_interface.interface import ResCreate

from netsquid_netbuilder.modules.scheduler.interface import IScheduleBuilder, IScheduleConfig
from netsquid_netbuilder.modules.scheduler.timeslot_scheduler import (
    RequestKey,
    RequestValue,
    Timeslot,
    TimeslotSchedulerProtocol,
    get_request_key,
)

if TYPE_CHECKING:
    from netsquid_netbuilder.network import Network

logger = logging.getLogger(__name__)


class StaticScheduleConfig(IScheduleConfig):
    """Configuration for a static schedule.

    This schedule gives each node pair a fixed timeslot of equal size in a repeating schedule.
    """

    time_window: float = 1_000_000  # 1 ms
    """Size of each timeslot for entanglement generation. [ns]"""
    switch_time: float = 0
    """Dead time when switching links where no entanglement generation is possible. [ns]"""
    max_multiplexing: int = 1
    """Number of links that can be open at the same time"""


class StaticScheduleProtocol(TimeslotSchedulerProtocol):
    CycleEndEvent = ns.pydynaa.EventType(
        "evtCycleEnd", "A full cycle of the static schedule has been completed"
    )

    MAX_REPEATS = 1000

    def __init__(
        self,
        name: str,
        params: StaticScheduleConfig,
        schema,
        qlinks: Dict[Tuple[str, str], IQLink],
        node_id_mapping: Dict[str, int],
    ):
        super().__init__(name, qlinks, node_id_mapping)
        self.params = params
        self._schema = schema

        self.full_cycle_time = len(schema) * (self.params.time_window + self.params.switch_time)
        self._populate_cycle(cycle_start_time=0)
        self._active_requests: Dict[RequestKey, RequestValue] = {}
        self._termination_counter = self.MAX_REPEATS
        self._evhandler = EventHandler(self._handle_event)
        self.logger = logger.getChild(self.name)

    def register_request(self, node_id: int, req: ReqCreateBase, create_id: int):
        key = get_request_key(node_id, req.remote_node_id, create_id)
        self._active_requests[key] = RequestValue(req.number, req.number)
        self._termination_counter = self.MAX_REPEATS

        if not self.find_registered_timeslots(node_id, req.remote_node_id):
            self._populate_cycle(cycle_start_time=self.next_cycle_start)

    def register_result(self, node_id: int, res: ResCreate):
        self._termination_counter = self.MAX_REPEATS
        key = get_request_key(node_id, res.remote_node_id, res.create_id)
        if key not in self._active_requests.keys():
            return

        req = self._active_requests[key]

        if key.node1 == node_id:
            req.qubits_node1_left -= 1
        else:
            req.qubits_node2_left -= 1

        if req.done:
            self._active_requests.pop(key)

        req.verify()

    def register_error(self, node_id: int, error: ResError):
        pass

    @property
    def next_cycle_start(self):
        current_time = ns.sim_time()
        cycle_iter = math.floor(current_time / self.full_cycle_time)
        if cycle_iter * self.full_cycle_time < current_time:
            return (cycle_iter + 1) * self.full_cycle_time
        else:
            return cycle_iter * self.full_cycle_time

    def _populate_cycle(self, cycle_start_time):
        time = cycle_start_time
        for sub_cycle in self._schema:
            for link in sub_cycle:
                timeslot = Timeslot(
                    node1_name=link[0],
                    node2_name=link[1],
                    start_time=time,
                    end_time=time + self.params.time_window,
                )
                self.register_timeslot(timeslot)
            time += self.params.time_window + self.params.switch_time
        self._schedule_at(time, self.CycleEndEvent)

    def start(self):
        super().start()
        self._wait(self._evhandler, entity=self, event_type=self.CycleEndEvent)

    def stop(self):
        super().stop()
        self._dismiss(self._evhandler, entity=self, event_type=self.CycleEndEvent)
        if self._termination_counter <= 0:
            self.logger.warning(
                f"{self.__class__.__name__} stopped due to internal termination counter."
            )
        if len(self._active_requests):
            self.logger.warning(
                f"Unfinished requests remaining in static scheduler. Requests: {self._active_requests}"
            )

    def _handle_event(self, event):
        super()._handle_event(event)
        if event.type == self.CycleEndEvent:
            self._handle_cycle_end()

    @property
    def _all_queues_empty(self) -> bool:
        for link in self.qlinks.values():
            if link.num_requests_in_queue > 0:
                return False
        return True

    def _handle_cycle_end(self):
        # Populate a new cycle if: counter not exceeded, there are still active requests and no
        if self._termination_counter > 0 and (
            len(self._active_requests) or not self._all_queues_empty
        ):
            self._termination_counter -= 1
            self._populate_cycle(self.next_cycle_start)


class StaticScheduleBuilder(IScheduleBuilder):
    @classmethod
    def build(
        cls,
        name: str,
        network: Network,
        qlinks: Dict[Tuple[str, str], IQLink],
        schedule_config: StaticScheduleConfig,
    ) -> StaticScheduleProtocol:

        if isinstance(schedule_config, dict):
            schedule_config = StaticScheduleConfig(**schedule_config)

        qlink_combinations = list(qlinks.keys())

        schema = cls.generate_schema(qlink_combinations, schedule_config.max_multiplexing)

        scheduler = StaticScheduleProtocol(
            name, schedule_config, schema, qlinks, network.node_name_id_mapping
        )
        return scheduler

    @staticmethod
    def generate_schema(conn: List[Tuple[str, str]], num_conn_max_active: int):
        num_conn = len(conn)
        schema = []
        used_connections = set()  # Keep track of connections that have been used
        while len(used_connections) < num_conn:
            subcycle = []
            used_nodes = (
                set()
            )  # Keep track of nodes that have been used within the current sub-cycle
            for i in range(min(num_conn_max_active, num_conn - len(used_connections))):
                for j in range(num_conn):
                    connection = conn[j]
                    node_a, node_b = connection
                    if (
                        (connection not in used_connections)
                        and (node_a not in used_nodes)
                        and (node_b not in used_nodes)
                    ):
                        subcycle.append(connection)
                        used_connections.add(connection)
                        used_nodes.add(node_a)
                        used_nodes.add(node_b)
                        break
            schema.append(subcycle)
        return schema
