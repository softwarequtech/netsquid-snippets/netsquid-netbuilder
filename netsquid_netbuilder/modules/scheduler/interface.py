from __future__ import annotations

import logging
from abc import ABC, ABCMeta, abstractmethod
from typing import TYPE_CHECKING, Dict, Tuple

from netsquid.protocols import Protocol
from netsquid_magic.qlink import IQLink
from qlink_interface import ReqCreateBase, ResError
from qlink_interface.interface import ResCreate

from netsquid_netbuilder.yaml_loadable import YamlLoadable

if TYPE_CHECKING:
    from netsquid_netbuilder.network import Network

logger = logging.getLogger(__name__)


class IScheduleConfig(YamlLoadable, ABC):
    """
    Abstract base class for schedule configuration.

    This class is used for defining schedule configurations. It is expected to be subclassed
    to provide specific configuration options for different schedule protocols.
    """

    pass


class IScheduleProtocol(Protocol, metaclass=ABCMeta):
    """
    Abstract base class for schedule protocols.

    This class defines the interface for schedule protocols that manage the scheduling
    of entanglement generation in a metropolitan hub.
    """

    @abstractmethod
    def register_request(self, node_id: int, req: ReqCreateBase, create_id: int):
        """
        Register a request with the schedule protocol.

        This method is called to register a request for scheduling entanglement generation.

        :param node_id: The ID of the node making the request.
        :param req: The request object containing information about the task to be scheduled.
        :param create_id: The ID of the request creation.
        """

    @abstractmethod
    def register_result(self, node_id: int, res: ResCreate):
        """
        Register a scheduling result with the protocol.

        This method is called to register the result of entanglement generation with the scheduler.

        :param node_id: The ID of the node receiving the scheduling result.
        :param res: The result object containing information about the scheduled task.
        """

    @abstractmethod
    def register_error(self, node_id: int, error: ResError):
        """
        Register an error with the protocol.

        This method is called to register an error encountered during the process of entanglement generation.

        :param node_id: The ID of the node encountering the error.
        :param error: The error object containing information about the encountered error.
        """


class IScheduleBuilder(metaclass=ABCMeta):
    """
    Abstract base class for schedule builders.

    This class defines the interface for building schedule protocols.
    """

    @classmethod
    @abstractmethod
    def build(
        cls,
        name: str,
        network: Network,
        qlinks: Dict[Tuple[str, str], IQLink],
        schedule_config: IScheduleConfig,
    ) -> IScheduleProtocol:
        """
        Build a schedule protocol.

        This method is called to build a schedule protocol based on the provided parameters.

        :param name: The name of the schedule protocol.
        :param network: The network for which the schedule protocol is being built.
        :param qlinks: A dictionary containing the links in the metropolitan hub.
        :param schedule_config: The configuration for the schedule protocol.
        :return: An instance of the built schedule protocol.
        """
