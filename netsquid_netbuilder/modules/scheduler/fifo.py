from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, List, Tuple

import netsquid as ns
from netsquid_magic.qlink import IQLink
from qlink_interface import ReqCreateBase, ResError
from qlink_interface.interface import ResCreate

from netsquid_netbuilder.modules.scheduler.interface import IScheduleBuilder, IScheduleConfig
from netsquid_netbuilder.modules.scheduler.timeslot_scheduler import (
    RequestKey,
    RequestValue,
    Timeslot,
    TimeslotSchedulerProtocol,
    get_request_key,
)

if TYPE_CHECKING:
    from netsquid_netbuilder.network import Network

logger = logging.getLogger(__name__)


@dataclass
class QueItem:
    node1_id: int
    node2_id: int
    create_id: int
    num_qubits: int


class FIFOScheduleConfig(IScheduleConfig):
    """Configuration for a first in first out schedule (FIFO).

    This schedule will give each request unlimited time to finish and prioritizes
    requests according to when they were submitted.
    """

    switch_time: float = 0.0
    """Dead time when switching links where no entanglement generation is possible. [ns]"""
    max_multiplexing: int = 1
    """Number of links that can be open at the same time"""


class FIFOScheduleProtocol(TimeslotSchedulerProtocol):
    # TODO FIFO schedule will still close the qlink even when next request is for same link
    # TODO FIFO schedule has nothing to prevent two links to the same node being open at the same time

    def __init__(
        self,
        name: str,
        params: FIFOScheduleConfig,
        qlinks: Dict[Tuple[str, str], IQLink],
        node_id_mapping: Dict[str, int],
    ):
        super().__init__(name, qlinks, node_id_mapping)
        self.params = params
        self._que: List[QueItem] = []
        # We need a reference item for each node per request,
        # to ensure behaviour that we don't close the connection before all nodes received their qubits
        self._active_requests: Dict[RequestKey, RequestValue] = {}
        self.logger = logger.getChild(self.name)

    def register_request(self, node_id: int, req: ReqCreateBase, create_id: int):
        if len(self._active_requests) < self.params.max_multiplexing:
            self._activate_request(node_id, req.remote_node_id, create_id, req.number)
        else:
            self._que.append(QueItem(node_id, req.remote_node_id, create_id, req.number))

    def register_result(self, node_id: int, res: ResCreate):
        key = get_request_key(node_id, res.remote_node_id, res.create_id)
        if key not in self._active_requests.keys():
            return

        req = self._active_requests[key]

        if key.node1 == node_id:
            req.qubits_node1_left -= 1
        else:
            req.qubits_node2_left -= 1

        if req.done:
            node1_name = self._node_name_mapping[key.node1]
            node2_name = self._node_name_mapping[key.node2]
            self._close_link(node1_name, node2_name)
            self._active_requests.pop(key)

            if len(self._que) > 0:
                que_item = self._que.pop(0)
                self._activate_request(
                    que_item.node1_id, que_item.node2_id, que_item.create_id, que_item.num_qubits
                )

        req.verify()

    def register_error(self, node_id: int, error: ResError):
        pass

    def _activate_request(self, node1_id: int, node2_id: int, create_id: int, number_qubits: int):
        key = get_request_key(node1_id, node2_id, create_id)
        self._active_requests[key] = RequestValue(number_qubits, number_qubits)
        node1_name = self._node_name_mapping[node1_id]
        node2_name = self._node_name_mapping[node2_id]
        timeslot = Timeslot(
            node1_name,
            node2_name,
            start_time=ns.sim_time() + self.params.switch_time,
            end_time=None,
        )
        self.register_timeslot(timeslot)

    def stop(self):
        super().stop()
        if len(self._que):
            self.logger.warning(
                f"Unfinished requests remaining in fifo scheduler queue. Requests: {self._que}"
            )
        if len(self._active_requests):
            self.logger.warning(
                f"Unfinished active requests remaining in fifo scheduler active request bookkeeping."
                f" Active requests: {self._active_requests}"
            )


class FIFOScheduleBuilder(IScheduleBuilder):
    @classmethod
    def build(
        cls,
        name: str,
        network: Network,
        qlinks: Dict[Tuple[str, str], IQLink],
        schedule_config: FIFOScheduleConfig,
    ) -> FIFOScheduleProtocol:

        if isinstance(schedule_config, dict):
            schedule_config = FIFOScheduleConfig(**schedule_config)

        scheduler = FIFOScheduleProtocol(
            name, schedule_config, qlinks, network.node_name_id_mapping
        )
        return scheduler
