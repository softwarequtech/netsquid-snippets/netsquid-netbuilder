from __future__ import annotations

import numpy as np
from netsquid.qubits import DenseDMRepr
from netsquid.qubits.qrepr import QRepr
from netsquid_magic.long_distance_interface import ILongDistanceInterface

from netsquid_netbuilder.modules.long_distance_interface.interface import (
    ILongDistanceInterfaceBuilder,
    ILongDistanceInterfaceConfig,
)


class DepolariseLongDistanceInterfaceConfig(ILongDistanceInterfaceConfig):
    """Model for a long distance interface that applies a depolarising noise to generated EPR pairs
    and an additional probability of failing a single entanglement generation attempt due to the interface.."""

    prob_max_mixed: float = 0.0
    """Probability of producing a maximally mixed state due to the long distance interface."""
    p_loss: float = 0.0
    """Probability of failing the EPR pair generation due to a loss at the long distance interface."""


class DepolariseLongDistanceInterface(ILongDistanceInterface):
    def __init__(self, config: DepolariseLongDistanceInterfaceConfig):
        self.prob_max_mixed = config.prob_max_mixed
        self.p_loss = config.p_loss

    @property
    def probability_success(self) -> float:
        return 1 - self.p_loss

    def operate(self, state: QRepr) -> QRepr:
        if self.prob_max_mixed == 0:
            return state

        maximally_mixed = 1 / 4.0 * np.identity(n=4, dtype=complex)

        original_state = state.reduced_dm()
        return DenseDMRepr(
            num_qubits=state.num_qubits,
            dm=self.prob_max_mixed * maximally_mixed + (1 - self.prob_max_mixed) * original_state,
        )


class DepolariseLongDistanceInterfaceBuilder(ILongDistanceInterfaceBuilder):
    @classmethod
    def build(
        cls, long_distance_interface_cfg: ILongDistanceInterfaceConfig
    ) -> ILongDistanceInterface:
        if not isinstance(long_distance_interface_cfg, DepolariseLongDistanceInterfaceConfig):
            raise TypeError(
                f"Expected type: {DepolariseLongDistanceInterfaceConfig}"
                f"got: {type(long_distance_interface_cfg)}"
            )
        return DepolariseLongDistanceInterface(long_distance_interface_cfg)
