from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

from netsquid_netbuilder.yaml_loadable import YamlLoadable

if TYPE_CHECKING:
    from netsquid_magic.long_distance_interface import ILongDistanceInterface


class ILongDistanceInterfaceConfig(YamlLoadable, ABC):
    """
    Abstract base class for configuration of long-distance interfaces.

    This class serves as a base for configurations of long-distance interfaces.
    Subclasses should provide specific configuration parameters for different types of long-distance interfaces.

    """


class ILongDistanceInterfaceBuilder(ABC):
    """
    Abstract base class for building long-distance interfaces.

    This class serves as a base for builders of long-distance interfaces.
    Subclasses should implement the build() method to create and configure
    instances of long-distance interfaces.

    """

    @classmethod
    @abstractmethod
    def build(
        cls, long_distance_interface_cfg: ILongDistanceInterfaceConfig
    ) -> ILongDistanceInterface:
        """
        Abstract method to build a long-distance interface.

        This method should be implemented by subclasses to create and configure
        instances of long-distance interfaces based on the provided configuration.

        :param long_distance_interface_cfg: The configuration object for the long-distance interface.

        :return: An instance of the built long-distance interface.

        """
