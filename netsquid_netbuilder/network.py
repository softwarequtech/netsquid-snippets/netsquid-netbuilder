from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum, auto
from typing import TYPE_CHECKING, Dict, List, Optional, Tuple

from netsquid.components import QuantumProcessor
from netsquid.nodes import Node
from netsquid_driver.driver import Driver

from netsquid_netbuilder.modules.scheduler.interface import IScheduleProtocol

if TYPE_CHECKING:
    from netsquid.components import Port
    from netsquid_magic.egp import EGPService
    from netsquid_magic.qlink import IQLink

    from netsquid_netbuilder.builder.network_builder import ProtocolController


@dataclass
class Network:
    """Data object containing objects that make up the network."""

    end_nodes: Dict[str, ProcessingNode] = field(default_factory=dict)
    """Dictionary of end nodes in the network. Keys are node names."""

    qlinks: Dict[Tuple[str, str], IQLink] = field(default_factory=dict)
    """
    Dictionary of links connecting nodes in the network.
    Keys are tuples of node names representing the two connected nodes.
    """

    hubs: Dict[str, MetroHub] = field(default_factory=dict)
    """Dictionary of metropolitan hubs in the network. Keys are metropolitan hub names."""

    chains: Dict[Tuple[str, str], Chain] = field(default_factory=dict)
    """
    Dictionary of repeater chains in the network.
    Keys are tuples of metro hub names that are being connected.
    """

    egp: Dict[Tuple[str, str], EGPService] = field(default_factory=dict)
    """
    Dictionary of entanglement generation protocols in the network.
    First key in the tuple is the node name with the egp, the second key is the remote node name.
    """

    ports: Dict[Tuple[str, str], Port] = field(default_factory=dict)
    """
    Dictionary of ports in the network.
    First key in the tuple is the node name with the port, the second key is the remote node name.
    """

    node_name_id_mapping: Dict[str, int] = field(default_factory=dict)
    """Mapping between node names and IDs in the network. Keys are node names."""

    _protocol_controller: Optional[ProtocolController] = None
    """Optional protocol controller for managing protocols in the network."""

    network_topology_info: Optional[NetworkTopologyInfo] = None
    """Optional network topology information."""

    class Role(Enum):
        """Enumeration representing the role of a node in the network."""

        END_NODE = auto()
        REPEATER = auto()
        HUB = auto()

    def find_role(self, node_name: str) -> Network.Role:
        """
        Find the role of a node in the network.

        :param node_name: Name of the node.
        :return: Role of the node in the network.
        """
        if node_name in self.end_nodes:
            return self.Role.END_NODE
        # Merge all repeaters node names into a single list using sum(.., [])
        elif node_name in sum(
            [list(chain.repeater_nodes_dict.keys()) for chain in self.chains.values()],
            [],
        ):
            return self.Role.REPEATER
        elif node_name in [hub.hub_node.name for hub in self.hubs.values()]:
            return self.Role.HUB
        else:
            raise ValueError(f"Could not find node: {node_name} in network")

    @property
    def nodes(self) -> Dict[str, NodeWithDriver]:
        """
        Get all nodes in the network.

        :return: Dictionary of all nodes in the network, the key is the node name."""
        nodes = {}
        nodes.update(self.end_nodes)
        nodes.update({hub_name: hub.hub_node for hub_name, hub in self.hubs.items()})
        for chain in self.chains.values():
            nodes.update(chain.repeater_nodes_dict)

        return nodes

    @staticmethod
    def filter_for_node(node_name: str, dictionary: Dict[(str, str), any]) -> Dict[str, any]:
        keys = dictionary.keys()
        keys = filter(lambda key_tuple: key_tuple[0] == node_name, keys)
        return {key[1]: dictionary[key] for key in keys}

    def start(self):
        """Start the network."""
        self._protocol_controller.start_all()

    def stop(self):
        """Stop the network."""
        self._protocol_controller.stop_all()


@dataclass
class MetroHub:
    """Data object containing objects that make up a metropolitan hub and its connected nodes."""

    hub_node: MetroHubNode = None
    """The node representing the metropolitan hub."""
    end_nodes: Dict[str, ProcessingNode] = field(default_factory=dict)
    """Dictionary of end nodes connected to the hub. Keys are node names."""
    scheduler: IScheduleProtocol = None
    """The scheduler protocol used in the hub."""
    end_node_lengths: Dict[str, float] = field(default_factory=dict)
    """Dictionary of lengths [km] between the hub and end nodes. Keys are node names."""
    chains: List[Chain] = field(default_factory=list)
    """List of repeater chains connected to the hub."""

    @property
    def name(self) -> str:
        """Name of the metropolitan hub"""
        return self.hub_node.name


@dataclass
class Chain:
    """Data object containing objects that make up chain of repeater nodes connecting two metropolitan hubs."""

    hub_1: MetroHub
    """The first metropolitan hub in the chain. This hub is considered to be "downstream"."""
    hub_2: MetroHub
    """The second metropolitan hub in the chain. This hub is considered to be "upstream"."""
    repeater_nodes: List[RepeaterNode] = field(default_factory=list)
    """List of repeater nodes within the chain. Organized from "downstream" to "upstream"."""
    link_lengths: List[float] = field(default_factory=list)
    """List of link lengths [km] between repeater nodes. Organized from "downstream" to "upstream"."""

    @property
    def name(self) -> str:
        """Name of the repeater chain"""
        return f"Chain ({self.hub_1.name}-{self.hub_2.name})"

    @property
    def repeater_nodes_dict(self) -> Dict[str, RepeaterNode]:
        """Dictionary of repeater nodes within the chain. The key is the node name."""
        return {node.name: node for node in self.repeater_nodes}

    def get_connected_repeater(self, node_name: str) -> RepeaterNode:
        """Get the repeater node that has a direct connection (via the metropolitan hub) to the given node name."""
        if node_name == self.hub_1.name or node_name in self.hub_1.end_nodes.keys():
            return self.repeater_nodes[0]
        elif node_name == self.hub_2.name or node_name in self.hub_2.end_nodes.keys():
            return self.repeater_nodes[-1]
        else:
            raise KeyError(f"Invalid request with hub_name: {node_name} for chain: {self.name}")


@dataclass
class NetworkTopologyInfo:
    """Data object containing information about the topology and layout of a network."""

    graph_delay: Dict[str, Dict[str, float]] = field(default_factory=dict)
    """A graph representation of the network with expected classical delay values for each edge."""

    routes: Dict[(str, str), List[str]] = field(default_factory=dict)
    """A dictionary storing shortest routes between pairs of nodes, represented as lists of intermediary nodes."""

    routing_tables: Dict[str, Dict[str, str]] = field(default_factory=dict)
    """
    The routing tables containing mappings of next hop information per destination at each node for the shortest route.

    In the following example of an item in the routing table:

    .. code-block:: python

        {"A": {"C": "B", ...}, ...}

    ``"A"`` is the current location, ``"C"`` the final destination, and the table provides the information
    that ``"B"`` is the next hop in order to reach ``"C"`` with the shortest route when at ``"A"``.
    """

    delays_table: Dict[(str, str), float] = field(default_factory=dict)
    """A table containing delays between pairs of nodes in the network."""

    def get_delay(self, node1: str, node2: str) -> float:
        """
        Get the delay between two nodes in the network.

        :param node1: The source node.
        :param node2: The destination node.
        :return: The classical delay between the two nodes.
        """
        return self.graph_delay[node1][node2]


class NodeWithDriver(Node):
    """
    A node with a driver component.

    :param name: The name of the node.
    :param node_id: The ID of the node.
    """

    def __init__(
        self,
        name: str,
        node_id: Optional[int] = None,
    ):
        super().__init__(name, ID=node_id)
        driver = Driver(f"Driver_{name}")
        self.add_subcomponent(driver, "driver")

    @property
    def driver(self) -> Driver:
        """The driver component of the node."""
        return self.subcomponents["driver"]


class QDeviceNode(NodeWithDriver):
    """
    A node that has a quantum device.

    :param name: The name of the node.
    :param qdevice: The quantum processor located on the node.
    :param qdevice_type: The type of the quantum device.
    :param node_id: optional The ID of the node.
    """

    def __init__(
        self,
        name: str,
        qdevice: QuantumProcessor,
        qdevice_type: str,
        node_id: Optional[int] = None,
    ):
        super().__init__(name, node_id=node_id)
        self.qmemory = qdevice
        self.qmemory_typ = qdevice_type

    @property
    def qdevice(self) -> QuantumProcessor:
        """The quantum processor located on the node."""
        return self.qmemory


class ProcessingNode(QDeviceNode):
    """A processing node.
    A processing node is a node that has a quantum device and is an endpoint in the network.
    """


class RepeaterNode(QDeviceNode):
    """A repeater node.
    A repeater node is a node that has a quantum device and facilitates entanglement generation between other nodes.
    """


class MetroHubNode(NodeWithDriver):
    """A node representing a metropolitan hub.
    A metropolitan hub connects nodes via connections that go through the hub and acts like a switch.
    """
