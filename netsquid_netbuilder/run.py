from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING, Dict, Optional

import netsquid as ns
from netsquid.protocols import Protocol, Signals
from netsquid_driver.classical_socket_service import ClassicalSocket, ClassicalSocketService
from netsquid_driver.connectionless_socket_service import ConnectionlessSocketService

from netsquid_netbuilder.builder.network_builder import NetworkBuilder
from netsquid_netbuilder.modules.clinks.default import DefaultCLinkBuilder, DefaultCLinkConfig
from netsquid_netbuilder.modules.clinks.instant import InstantCLinkBuilder, InstantCLinkConfig
from netsquid_netbuilder.modules.long_distance_interface.depolarise import (
    DepolariseLongDistanceInterfaceBuilder,
    DepolariseLongDistanceInterfaceConfig,
)
from netsquid_netbuilder.modules.qdevices.generic import GenericQDeviceBuilder
from netsquid_netbuilder.modules.qdevices.nv import NVQDeviceBuilder
from netsquid_netbuilder.modules.qlinks.depolarise import (
    DepolariseQLinkBuilder,
    DepolariseQLinkConfig,
)
from netsquid_netbuilder.modules.qlinks.heralded_double_click import (
    HeraldedDoubleClickQLinkBuilder,
    HeraldedDoubleClickQLinkConfig,
)
from netsquid_netbuilder.modules.qlinks.heralded_single_click import (
    HeraldedSingleClickQLinkBuilder,
    HeraldedSingleClickQLinkConfig,
)
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkBuilder, PerfectQLinkConfig
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_builder import (
    SwapASAPBuilder,
    SwapASAPConfig,
)
from netsquid_netbuilder.modules.scheduler.fifo import FIFOScheduleBuilder
from netsquid_netbuilder.modules.scheduler.static import StaticScheduleBuilder

if TYPE_CHECKING:
    from netsquid.components import Port
    from netsquid_magic.egp import EGPService
    from netsquid_magic.qlink import IQLink

    from netsquid_netbuilder.network import Network, ProcessingNode


def get_default_builder() -> NetworkBuilder:
    """
    Get the default network builder.

    This function returns a NetworkBuilder instance configured with all models
    in the netsquid_netbuilder package already registered with the following model names:

    QDevice Models: "generic", "nv"

    Link Models: "perfect", "depolarise", "heralded-single-click", "heralded-double-click"

    Long Distance Interface Models: "depolarise"

    CLink Models: "instant", "default"

    Schedulers: "static", "fifo"

    Quantum Repeater Chain Control: "swapASAP"

    :return: A NetworkBuilder instance.
    """
    builder = NetworkBuilder()
    # Default qdevice models registration
    builder.register_qdevice("generic", GenericQDeviceBuilder)
    builder.register_qdevice("nv", NVQDeviceBuilder)

    # default qlink models registration
    builder.register_qlink("perfect", PerfectQLinkBuilder, PerfectQLinkConfig)
    builder.register_qlink("depolarise", DepolariseQLinkBuilder, DepolariseQLinkConfig)
    builder.register_qlink(
        "heralded-single-click",
        HeraldedSingleClickQLinkBuilder,
        HeraldedSingleClickQLinkConfig,
    )
    builder.register_qlink(
        "heralded-double-click",
        HeraldedDoubleClickQLinkBuilder,
        HeraldedDoubleClickQLinkConfig,
    )

    # default long distance interface model
    builder.register_long_distance_interface(
        "depolarise",
        DepolariseLongDistanceInterfaceBuilder,
        DepolariseLongDistanceInterfaceConfig,
    )

    # default clink models registration
    builder.register_clink("instant", InstantCLinkBuilder, InstantCLinkConfig)
    builder.register_clink("default", DefaultCLinkBuilder, DefaultCLinkConfig)

    # default schedulers
    builder.register_scheduler("static", StaticScheduleBuilder)
    builder.register_scheduler("fifo", FIFOScheduleBuilder)

    # default quantum repeater chain control
    builder.register_qrep_chain_control("swapASAP", SwapASAPBuilder, SwapASAPConfig)

    return builder


def run(
    network: Network, programs: Dict[str, Program], end_time: float = None, magnitude: float = 1
) -> ns.util.SimStats:
    """
    Run programs on the given network.

    This function starts all programs and the network, runs the simulation,
    and stops all programs and network after the simulation finishes.

    If no `end_time` is set, then the simulation will run until there are no more events in the timeline.

    :param network: The network on which to run the programs.
    :param programs: A dictionary mapping node names to program instances.
    :param end_time: Time up to which to run the simulation, not including this instant itself.
     In other words, events scheduled at end_time will not be issued. Default unit is nanoseconds (see `magnitude`).
    :param magnitude: Time magnitude to use. Default is nanoseconds.
    :return: Simulation statistics.

    .. note::
        It can be the case that there will always be a new event in the timeline.
        In this case, not passing an `end_time` to the run may cause a never ending simulation.

    """
    # start all programs
    network.start()
    for node_name, prog in programs.items():
        context = ProgramContext.create_from_network(node_name, network)
        prog.set_context(context)
        prog.start()

    sim_stats = ns.sim_run(end_time=end_time, magnitude=magnitude)

    # stop all programs
    network.stop()
    for node_name, prog in programs.items():
        prog.stop()

    return sim_stats


@dataclass
class ProgramContext:
    """The context, of objects accessible to the node, for executing programs on a processing node."""

    node: ProcessingNode
    """The processing node associated with the context."""
    qlinks: Dict[str, IQLink]
    """Dictionary of links accessible from the node. Keys are remote node names."""
    egp: Dict[str, EGPService]
    """Dictionary of entanglement generation protocols (EGPs) accessible from the node. Keys are remote node names."""
    node_id_mapping: Dict[str, int]
    """Mapping between node names and IDs in the network. Keys are remote node names."""
    sockets: Dict[str, ClassicalSocket]
    """Dictionary of classical sockets accessible from the node. Keys are remote node names."""
    ports: Dict[str, Port]
    """Dictionary of ports accessible from the node. Keys are remote node names."""

    @classmethod
    def create_from_network(cls, node_name: str, network: Network) -> ProgramContext:
        """
        Create the protocol context for a node in the network.

        :param node_name: Name of the node for which to get the protocol context.
        :param network: The network where the node is located in.
        :return: Protocol context for the specified node.
        """
        node = network.end_nodes[node_name]
        qlinks = network.filter_for_node(node_name, network.qlinks)
        egp = network.filter_for_node(node_name, network.egp)
        ports = network.filter_for_node(node_name, network.ports)

        socket_service = ConnectionlessSocketService(node=node)
        node.driver.add_service(ClassicalSocketService, socket_service)
        sockets = {}

        for remote_node_name in network.nodes.keys():
            if remote_node_name is not node_name:
                socket = socket_service.create_socket()
                socket.bind(port_name="0", remote_node_name=remote_node_name)
                socket.connect(remote_port_name="0", remote_node_name=remote_node_name)
                sockets[remote_node_name] = socket
                network._protocol_controller.register(socket)

        return ProgramContext(node, qlinks, egp, network.node_name_id_mapping, sockets, ports)


class Program(Protocol):
    """
    Base class for defining quantum programs.

    :ivar context: The context in which the program is executed.
    """

    def __init__(self):
        self.context: Optional[ProgramContext] = None
        self.add_signal(Signals.FINISHED)

    def set_context(self, context: ProgramContext):
        """
        Set the execution context for the program.

        :param context: The execution context.
        """
        self.context = context

    def start(self) -> None:
        """
        Start the execution of the program.
        """
        super().start()

    def stop(self) -> None:
        """
        Stop the execution of the program.
        """
        super().stop()
