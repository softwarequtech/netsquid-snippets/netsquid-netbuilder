from __future__ import annotations

import heapq
import logging
from copy import copy
from typing import Dict, List, Type

from netsquid.components import Port
from netsquid_driver.classical_routing_service import ClassicalRoutingService
from netsquid_driver.driver import Driver
from netsquid_driver.EGP import EGPService
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService
from netsquid_entanglementtracker.bell_state_tracker import BellStateTracker
from netsquid_magic.egp import EgpProtocol
from netsquid_magic.link_layer import MagicLinkLayerProtocolWithSignaling

from netsquid_netbuilder.builder.builder_utils import create_and_connect_ports
from netsquid_netbuilder.builder.metro_hub import HubBuilder
from netsquid_netbuilder.builder.repeater_chain import ChainBuilder
from netsquid_netbuilder.modules.clinks.interface import ICLinkBuilder, ICLinkConfig
from netsquid_netbuilder.modules.long_distance_interface.interface import (
    ILongDistanceInterfaceBuilder,
    ILongDistanceInterfaceConfig,
)
from netsquid_netbuilder.modules.qdevices.interface import IQDeviceBuilder, IQDeviceConfig
from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.modules.qrep_chain_control.interface import (
    IQRepChainControlBuilder,
    IQRepChainControlConfig,
)
from netsquid_netbuilder.modules.scheduler.interface import IScheduleBuilder, IScheduleConfig
from netsquid_netbuilder.network import Network, NetworkTopologyInfo, NodeWithDriver, QDeviceNode
from netsquid_netbuilder.network_config import NetworkConfig

logger = logging.getLogger(__name__)


class NetworkBuilder:
    """
    Class for building networks.

    This class provides methods for registering and building networks.

    :ivar protocol_controller: Protocol controller instance.
    """

    def __init__(self):
        self.protocol_controller = ProtocolController()
        self.node_builder = NodeBuilder()
        self.clink_builder = CLinkBuilder()
        self.qlink_builder = QLinkBuilder(self.protocol_controller)
        self.network_topology_calculator = NetworkTopologyCalculator()
        self.network_service_builder = NetworkServicesBuilder()
        self.egp_builder = EGPBuilder(self.protocol_controller)
        self.hub_builder = HubBuilder(self.protocol_controller)
        self.chain_builder = ChainBuilder(self.protocol_controller)

    def register(self, model_name: str, builder: type, config: type):
        """
        Register a model with the builder.

        :param model_name: Name of the model to register.
        :param builder: Type of the builder.
        :param config: Type of the configuration.
        """
        _error_msg = f"Could not register model: {model_name}. The config class is incorrect"

        if issubclass(builder, IQDeviceBuilder):
            assert issubclass(config, IQDeviceConfig), _error_msg
            self.register_qdevice(model_name, builder)

        elif issubclass(builder, IQLinkBuilder):
            assert issubclass(config, IQLinkConfig), _error_msg
            self.register_qlink(model_name, builder, config)

        elif issubclass(builder, ICLinkBuilder):
            assert issubclass(config, ICLinkConfig), _error_msg
            self.register_clink(model_name, builder, config)

        elif issubclass(builder, IScheduleBuilder):
            assert issubclass(config, IScheduleConfig), _error_msg
            self.register_scheduler(model_name, builder)

        elif issubclass(builder, IQRepChainControlBuilder):
            assert issubclass(config, IQRepChainControlConfig), _error_msg
            self.register_qrep_chain_control(model_name, builder, config)

        elif issubclass(builder, ILongDistanceInterfaceBuilder):
            assert issubclass(config, ILongDistanceInterfaceConfig), _error_msg
            self.register_long_distance_interface(model_name, builder, config)

        else:
            raise KeyError(
                f"Could not register model: {model_name}. The builder class is incorrect."
            )

    def register_qdevice(self, model_name: str, builder: Type[IQDeviceBuilder]):
        """
        Register a qdevice model with the builder.

        :param model_name: Name of the qdevice model.
        :param builder: Type of the qdevice builder.
        """
        self.node_builder.register(model_name, builder)
        self.chain_builder.register_qdevice(model_name, builder)

    def register_qlink(
        self, model_name: str, builder: Type[IQLinkBuilder], config: Type[IQLinkConfig]
    ):
        """
        Register a qlink model with the builder.

        :param model_name: Name of the qlink model.
        :param builder: Type of the qlink builder.
        :param config: Type of the qlink configuration.
        """
        self.qlink_builder.register(model_name, builder, config)
        self.hub_builder.register_qlink(model_name, builder, config)
        self.chain_builder.register_qlink(model_name, builder, config)

    def register_clink(
        self, model_name: str, builder: Type[ICLinkBuilder], config: Type[ICLinkConfig]
    ):
        """
        Register a clink model with the builder.

        :param model_name: Name of the clink model.
        :param builder: Type of the clink builder.
        :param config: Type of the clink configuration.
        """
        self.clink_builder.register(model_name, builder, config)
        self.hub_builder.register_clink(model_name, builder, config)
        self.chain_builder.register_clink(model_name, builder, config)

    def register_scheduler(self, model_name: str, builder: Type[IScheduleBuilder]):
        """
        Register a scheduler with the builder.

        :param model_name: Name of the scheduler.
        :param builder: Type of the scheduler builder.
        """
        self.hub_builder.register_scheduler(model_name, builder)

    def register_long_distance_interface(
        self,
        model_name: str,
        builder: Type[ILongDistanceInterfaceBuilder],
        config: Type[ILongDistanceInterfaceConfig],
    ):
        """
        Register a long distance interface model with the builder.

        :param model_name: Name of the long distance interface model.
        :param builder: Type of the long distance interface builder.
        :param config: Type of the long distance interface configuration.
        """
        self.chain_builder.register_long_distance_interface(model_name, builder, config)

    def register_qrep_chain_control(
        self,
        model_name: str,
        builder: Type[IQRepChainControlBuilder],
        config: Type[IQRepChainControlConfig],
    ):
        """
        Register a quantum repeater chain control model with the builder.

        :param model_name: Name of the quantum repeater chain control model.
        :param builder: Type of the quantum repeater chain control builder.
        :param config: Type of the quantum repeater chain control configuration.
        """
        self.chain_builder.register_qrep_chain_control(model_name, builder, config)

    def build(self, config: NetworkConfig) -> Network:
        """
        Build a network based on the provided configuration.

        :param config: Network configuration.
        :return: Network object.
        """
        self.hub_builder.set_configs(config.hubs)
        self.chain_builder.set_configs(config.repeater_chains)

        # Create the data structures for the network
        network = Network()
        network.hubs = self.hub_builder.create_metro_hub_objects()
        network.chains = self.chain_builder.create_chain_objects(network.hubs)

        # Create node instances
        network.end_nodes = self.node_builder.build(config)
        self.hub_builder.register_end_nodes_to_hub(network)
        self.hub_builder.build_hub_nodes(network)
        self.chain_builder.build_repeater_nodes(network)

        # Set up a mapping between node name (str) and id (int)
        network.node_name_id_mapping = self._create_node_name_id_mapping(network)

        # Set up ports and connections for classical communication
        network.ports = self.clink_builder.build(config, network)
        network.ports.update(self.hub_builder.build_clinks(network))
        network.ports.update(self.chain_builder.build_clinks(network))

        # Create instances of all direct quantum links
        network.qlinks = self.qlink_builder.build(config, network.end_nodes)
        network.qlinks.update(self.hub_builder.build_qlinks(network))
        network.qlinks.update(self.chain_builder.build_qlinks(network))

        # Install photonic interface models in relevant links
        self.chain_builder.build_long_distance_interfaces(network)

        # Calculate topology and shortest routes
        network.network_topology_info = (
            self.network_topology_calculator.calculate_network_topology_info(network)
        )

        # setup classical messaging
        self.network_service_builder.build_routing_services(network)
        self.network_service_builder.build_entanglement_tracker_services(network)

        # Create the scheduler
        self.hub_builder.build_schedule(network)

        # Set up EGP protocols
        network.egp = self.egp_builder.build(network)
        network.egp.update(self.chain_builder.build_egp(network))

        # "Move" the protocol controller to the network object
        for node in network.nodes.values():
            self.protocol_controller.register(node.driver)
        network._protocol_controller = self.protocol_controller

        logger.info(f"Node name to ID mapping:\n{network.node_name_id_mapping}")
        return network

    @staticmethod
    def _create_node_name_id_mapping(network: Network) -> Dict[str, int]:
        mapping = {node_name: node.ID for node_name, node in network.end_nodes.items()}
        for chain in network.chains.values():
            mapping.update(
                {node_name: node.ID for node_name, node in chain.repeater_nodes_dict.items()}
            )
        return mapping


class NodeBuilder:
    def __init__(self):
        self.qdevice_builders: Dict[str, Type[IQDeviceBuilder]] = {}

    def register(self, model_name: str, builder: Type[IQDeviceBuilder]):
        self.qdevice_builders[model_name] = builder

    def build(self, config: NetworkConfig) -> Dict[str, QDeviceNode]:
        nodes = {}
        for node_config in config.processing_nodes:
            node_name = node_config.name
            node_qdevice_typ = node_config.qdevice_typ

            if node_qdevice_typ not in self.qdevice_builders.keys():
                raise RuntimeError(f"No model of type: {node_qdevice_typ} registered")

            builder = self.qdevice_builders[node_qdevice_typ]
            qdevice = builder.build(f"qdevice_{node_name}", qdevice_cfg=node_config.qdevice_cfg)

            nodes[node_name] = QDeviceNode(
                node_name,
                qdevice=qdevice,
                qdevice_type=node_qdevice_typ,
            )
            self.qdevice_builders[node_qdevice_typ].build_services(nodes[node_name])
        return nodes


class CLinkBuilder:
    def __init__(self):
        self.clink_builders: Dict[str, Type[ICLinkBuilder]] = {}
        self.clink_configs: Dict[str, Type[ICLinkConfig]] = {}

    def register(self, model_name: str, builder: Type[ICLinkBuilder], config: Type[ICLinkConfig]):
        self.clink_builders[model_name] = builder
        self.clink_configs[model_name] = config

    def build(self, config: NetworkConfig, network: Network) -> Dict[(str, str), Port]:
        """
        Builds the classical links from the clinks specified in config and returns
        the newly connected ports for these clinks.

        :return: Dictionary of newly connected ports. Keys are node name tuples.
        First key in the tuple is the node name with the port, the second key is the remote node name.
        """
        nodes = network.end_nodes
        ports = {}
        if config.clinks is None:
            return {}
        for clink in config.clinks:
            s1 = nodes[clink.node1]
            s2 = nodes[clink.node2]
            clink_builder = self.clink_builders[clink.typ]
            connection = clink_builder.build(s1, s2, clink_cfg=clink.cfg)

            ports.update(create_and_connect_ports(s1, s2, connection, port_name_prefix="external"))

        return ports


class QLinkBuilder:
    def __init__(self, protocol_controller: ProtocolController):
        self.protocol_controller = protocol_controller
        self.qlink_builders: Dict[str, Type[IQLinkBuilder]] = {}
        self.qlink_configs: Dict[str, Type[IQLinkConfig]] = {}

    def register(self, model_name: str, builder: Type[IQLinkBuilder], config: Type[IQLinkConfig]):
        self.qlink_builders[model_name] = builder
        self.qlink_configs[model_name] = config

    def build(
        self, config: NetworkConfig, nodes: Dict[str, QDeviceNode]
    ) -> Dict[(str, str), MagicLinkLayerProtocolWithSignaling]:
        """
        Builds the quantum links from the qlinks specified in config and returns
        the newly created MagicLinkLayer objects.

        :return: Dictionary of newly created MagicLinkLayer objects.
        Keys are tuples of node names representing the two connected nodes.
        """
        qlink_dict = {}
        if config.qlinks is None:
            return {}
        for qlink in config.qlinks:
            node1 = nodes[qlink.node1]
            node2 = nodes[qlink.node2]
            if qlink.typ not in self.qlink_builders.keys():
                raise RuntimeError(f"No model of type: {qlink.typ} registered")

            builder = self.qlink_builders[qlink.typ]
            link_prot = builder.build(node1, node2, qlink.cfg)
            self.protocol_controller.register(link_prot)
            qlink_dict[(node1.name, node2.name)] = link_prot
            qlink_dict[(node2.name, node1.name)] = link_prot

        return qlink_dict


class EGPBuilder:
    def __init__(self, protocol_controller: ProtocolController):
        self.protocol_controller = protocol_controller

    def build(self, network: Network) -> Dict[(str, str), EGPService]:
        """
        Creates the Entanglement generation protocols (EGP) for each end node to another end node
        that have a direct qlink connection between each other.

        .. note::
            This does not create an EGP for an end node and another end node that are connected indirectly with each
            other.

        :return: Dictionary of newly created EGP protocols. Keys are node name tuples.
        First key in the tuple is the node name with the EGP, the second key is the remote node name.
        """
        egp_dict = {}

        for id_tuple, link_layer in network.qlinks.items():
            node_name, peer_node_name = id_tuple
            if (
                network.find_role(node_name) is network.Role.END_NODE
                and network.find_role(peer_node_name) is network.Role.END_NODE
            ):
                node = network.end_nodes[node_name]
                egp = EgpProtocol(node, link_layer)
                egp_dict[(node_name, peer_node_name)] = egp
                self.protocol_controller.register(egp)
        return egp_dict


class NetworkTopologyCalculator:
    @classmethod
    def calculate_network_topology_info(cls, network: Network) -> NetworkTopologyInfo:
        """
        Creates the network topology information object by:
        creating a graph of the classical network with time delay as distance and then
        calculating the shortest routes between each node pair by using Dijkstra's algorithm.
        """
        network_info = NetworkTopologyInfo()
        cls._create_graphs(network_info, network)
        cls._calculate_shortest_routes(network_info, network)
        return network_info

    @classmethod
    def _create_graphs(cls, network_info: NetworkTopologyInfo, network: Network):
        node_names = network.nodes.keys()
        network_info.graph_delay = {node_name: {} for node_name in node_names}
        for (node_name, peer_name), port in network.ports.items():
            network_info.graph_delay[node_name][peer_name] = cls._get_delay(port)

    @staticmethod
    def _get_delay(port: Port) -> float:
        connection = port.connected_port.component
        channel_a_to_b = connection.subcomponents["channel_AtoB"]
        return channel_a_to_b.delay_mean

    @classmethod
    def _calculate_shortest_routes(cls, network_info: NetworkTopologyInfo, network: Network):
        for node_name in network.nodes.keys():
            distances, routes = cls._calculate_local_shortest_routes(
                network_info.graph_delay, node_name
            )
            network_info.routing_tables[node_name] = {}
            for remote_node in network.nodes.keys():
                if remote_node == node_name:
                    continue
                network_info.delays_table[(node_name, remote_node)] = distances[remote_node]
                network_info.routes[(node_name, remote_node)] = routes[remote_node]
                network_info.routing_tables[node_name][remote_node] = routes[remote_node][0]

    @staticmethod
    def _calculate_local_shortest_routes(
        graph: Dict[str, Dict[str, float]], node_name: str
    ) -> (Dict[str, float], Dict[str, List[str]]):
        distances = {node: float("inf") for node in graph.keys()}
        routes = {node: [] for node in graph.keys()}
        distances[node_name] = 0
        routes[node_name] = [node_name]
        priority_queue = [(0.0, node_name)]

        while priority_queue:
            current_dist, current_node = heapq.heappop(priority_queue)

            if current_dist > distances[current_node]:
                continue

            for node, link_dist in graph[current_node].items():
                dist = current_dist + link_dist
                if dist < distances[node] or (
                    dist == distances[node] and len(routes[current_node]) + 1 < len(routes[node])
                ):
                    distances[node] = dist
                    routes[node] = copy(routes[current_node])
                    routes[node].append(node)
                    heapq.heappush(priority_queue, (dist, node))

        for _, rout in routes.items():
            if node_name not in rout:
                raise RuntimeError(
                    f"Could not calculate a shortest route between nodes: {rout[0]} and {node_name}"
                )
            rout.remove(node_name)

        return distances, routes


class NetworkServicesBuilder:
    @staticmethod
    def build_routing_services(network: Network):
        for node in network.nodes.values():
            assert isinstance(node, NodeWithDriver)
            local_routing_table = network.network_topology_info.routing_tables[node.name]
            local_port_routing_table = {
                target_name: network.ports[(node.name, forward_node_name)]
                for target_name, forward_node_name in local_routing_table.items()
            }
            routing_service = ClassicalRoutingService(
                node=node, local_routing_table=local_port_routing_table
            )

            node.driver.add_service(ClassicalRoutingService, routing_service)

    @staticmethod
    def build_entanglement_tracker_services(network: Network):
        for node in network.nodes.values():
            assert isinstance(node, NodeWithDriver)
            node.driver.add_service(EntanglementTrackerService, BellStateTracker(node))


class ProtocolController:
    """
    Class for managing protocols and drivers.

    This class maintains a registry of protocols and drivers and provides methods to start and stop them.
    """

    def __init__(self):
        """Initialize the ProtocolController."""
        self._registry = []
        self._drivers = []

    def register(self, obj: object):
        """
        Register a protocol or driver with the controller.

        :param obj: Protocol or driver object to register.
        """
        if isinstance(obj, Driver):
            self._drivers.append(obj)
            return
        assert callable(getattr(obj, "start", None))
        assert callable(getattr(obj, "stop", None))
        self._registry.append(obj)

    def start_all(self):
        """Start all registered protocols and drivers."""
        for obj in self._registry:
            obj.start()
        for driver in self._drivers:
            driver.start_all_services()

    def stop_all(self):
        """Stop all registered protocols and drivers."""
        for obj in self._registry:
            obj.stop()
        for driver in self._drivers:
            driver.stop_all_services()
