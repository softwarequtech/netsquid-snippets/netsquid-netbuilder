from __future__ import annotations

import itertools
import logging
from typing import TYPE_CHECKING, Dict, List, Optional, Tuple, Type

from netsquid.components import Port
from netsquid.nodes import Node
from netsquid_magic.link_layer import MagicLinkLayerProtocolWithSignaling
from netsquid_magic.qlink import IQLink, MagicQLink

from netsquid_netbuilder.builder.builder_utils import (
    create_and_connect_ports,
    link_config_has_length,
    link_config_set_length,
)
from netsquid_netbuilder.modules.clinks.interface import ICLinkBuilder, ICLinkConfig
from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.modules.scheduler.interface import IScheduleBuilder
from netsquid_netbuilder.network import MetroHub, MetroHubNode, Network
from netsquid_netbuilder.network_config import MetroHubConfig

if TYPE_CHECKING:
    from netsquid_netbuilder.builder.network_builder import ProtocolController

logger = logging.getLogger(__name__)


class HubBuilder:
    def __init__(self, protocol_controller):
        self.protocol_controller: ProtocolController = protocol_controller
        self.qlink_builders: Dict[str, Type[IQLinkBuilder]] = {}
        self.qlink_configs: Dict[str, Type[IQLinkConfig]] = {}
        self.hub_configs: Optional[List[MetroHubConfig]] = None
        self.clink_builders: Dict[str, Type[ICLinkBuilder]] = {}
        self.clink_configs: Dict[str, Type[ICLinkConfig]] = {}
        self.scheduler_builders: Dict[str, Type[IScheduleBuilder]] = {}

    def register_clink(
        self, model_name: str, builder: Type[ICLinkBuilder], config: Type[ICLinkConfig]
    ):
        self.clink_builders[model_name] = builder
        self.clink_configs[model_name] = config

    def set_configs(self, metro_hub_configs: List[MetroHubConfig]):
        self.hub_configs = metro_hub_configs

    def register_qlink(
        self, model_name: str, builder: Type[IQLinkBuilder], config: Type[IQLinkConfig]
    ):
        self.qlink_builders[model_name] = builder
        self.qlink_configs[model_name] = config

    def register_scheduler(self, model_name: str, model: Type[IScheduleBuilder]):
        self.scheduler_builders[model_name] = model

    def register_end_nodes_to_hub(self, network: Network):
        if self.hub_configs is None:
            return
        for hub_config in self.hub_configs:
            hub = network.hubs[hub_config.name]
            for connection in hub_config.connections:
                hub.end_nodes[connection.node] = network.end_nodes[connection.node]

    def create_metro_hub_objects(self) -> Dict[str, MetroHub]:
        hubs = {}
        if self.hub_configs is not None:
            for hub_config in self.hub_configs:
                hub = hubs[hub_config.name] = MetroHub()

                # Populate hub_end_node_length field of metro hub object
                for connection in hub_config.connections:
                    hub.end_node_lengths[connection.node] = connection.length
        return hubs

    def build_hub_nodes(self, network: Network):
        if self.hub_configs is None:
            return
        for hub_config in self.hub_configs:
            hub = network.hubs[hub_config.name]
            hub.hub_node = MetroHubNode(name=hub_config.name)

    def build_clinks(self, network: Network) -> Dict[(str, str), Port]:
        ports: Dict[(str, str), Port] = {}
        if self.hub_configs is None:
            return ports
        for hub_config in self.hub_configs:
            hub = network.hubs[hub_config.name]
            hub_node = hub.hub_node
            clink_builder = self.clink_builders[hub_config.clink_typ]
            clink_cfg_typ = self.clink_configs[hub_config.clink_typ]
            clink_config = hub_config.clink_cfg

            if isinstance(clink_config, dict):
                clink_config = clink_cfg_typ(**clink_config)
            if not isinstance(clink_config, clink_cfg_typ):  # noqa
                raise TypeError(
                    f"Incorrect configuration provided. Got {type(clink_config)},"
                    f" expected {clink_cfg_typ.__name__}"
                )

            if not hasattr(clink_config, "length"):
                logger.warning(
                    f"CLink type: {clink_cfg_typ} has no length attribute length,"
                    f"metro hub lengths wil not be used for this Clink."
                )

            def _connect_nodes(node_1: Node, node_2: Node, length: float):
                if hasattr(clink_config, "length"):
                    clink_config.length = length

                connection = clink_builder.build(node_1, node_2, clink_config)

                ports.update(
                    create_and_connect_ports(
                        node_1, node_2, connection, port_name_prefix="external"
                    )
                )

            # Build hub - end node connections
            for connection_config in hub_config.connections:
                end_node = network.end_nodes[connection_config.node]
                _connect_nodes(end_node, hub_node, connection_config.length)

            for chain in hub.chains:
                repeater_node = chain.get_connected_repeater(hub_node.name)

                if repeater_node == chain.repeater_nodes[0]:
                    hub_repeater_link_length = chain.link_lengths[0]
                elif repeater_node == chain.repeater_nodes[-1]:
                    hub_repeater_link_length = chain.link_lengths[-1]
                else:
                    raise RuntimeError(
                        f"Repeater node: {repeater_node.name} not and edge of chain: {chain.name}"
                    )
                _connect_nodes(hub_node, repeater_node, hub_repeater_link_length)

        return ports

    def build_qlinks(self, network: Network) -> Dict[(str, str), IQLink]:
        qlink_dict = {}
        if self.hub_configs is None:
            return qlink_dict

        for hub_config in self.hub_configs:
            qlink_builder = self.qlink_builders[hub_config.qlink_typ]
            qlink_cfg_typ = self.qlink_configs[hub_config.qlink_typ]
            qlink_config = hub_config.qlink_cfg

            if isinstance(qlink_config, dict):
                qlink_config = qlink_cfg_typ(**hub_config.qlink_cfg)
            if not isinstance(qlink_config, qlink_cfg_typ):  # noqa
                raise TypeError(
                    f"Incorrect configuration provided. Got {type(qlink_config)},"
                    f" expected {qlink_cfg_typ.__name__}"
                )

            if not link_config_has_length(qlink_config):
                logger.warning(
                    f"Link type: {qlink_cfg_typ} has no length attribute length,"
                    f"metro hub lengths wil not be used for this link."
                )

            new_qlinks = self._build_end_node_end_node_links(
                hub_config, network, qlink_config, qlink_builder
            )
            qlink_dict.update(new_qlinks)

            new_qlinks = self._build_repeater_end_node_qlinks(
                hub_config, network, qlink_config, qlink_builder
            )
            qlink_dict.update(new_qlinks)

        return qlink_dict

    def _build_end_node_end_node_links(
        self,
        hub_config: MetroHubConfig,
        network: Network,
        qlink_config: IQLinkConfig,
        qlink_builder: Type[IQLinkBuilder],
    ) -> Dict[Tuple[str, str], MagicLinkLayerProtocolWithSignaling]:
        """Build the qlinks between end-nodes in a hub."""
        qlink_dict = {}
        for conn_1_cfg, conn_2_cfg in itertools.combinations(hub_config.connections, 2):
            node1 = network.end_nodes[conn_1_cfg.node]
            node2 = network.end_nodes[conn_2_cfg.node]
            link_config_set_length(qlink_config, conn_1_cfg.length, conn_2_cfg.length)

            link_prot = qlink_builder.build(node1, node2, qlink_config)
            link_prot.close()

            self.protocol_controller.register(link_prot)
            qlink_dict[(node1.name, node2.name)] = link_prot
            qlink_dict[(node2.name, node1.name)] = link_prot
        return qlink_dict

    @staticmethod
    def _build_repeater_end_node_qlinks(
        hub_config: MetroHubConfig,
        network: Network,
        qlink_config: IQLinkConfig,
        qlink_builder: Type[IQLinkBuilder],
    ) -> Dict[Tuple[str, str], MagicQLink]:
        """Build the links between end nodes in a hub and the repeater node that connects to the hub."""
        qlink_dict = {}
        hub = network.hubs[hub_config.name]
        for chain in hub.chains:
            for conn_cfg in hub_config.connections:
                end_node = network.end_nodes[conn_cfg.node]
                repeater_node = chain.get_connected_repeater(end_node.name)

                if repeater_node == chain.repeater_nodes[0]:
                    hub_repeater_link_length = chain.link_lengths[0]
                elif repeater_node == chain.repeater_nodes[-1]:
                    hub_repeater_link_length = chain.link_lengths[-1]
                else:
                    raise RuntimeError(
                        f"Repeater node: {repeater_node.name} not and edge of chain: {chain.name}"
                    )

                link_config_set_length(
                    qlink_config,
                    conn_cfg.length,
                    hub_repeater_link_length,
                )

                # This section of code makes a "hack" due to direct link and cross-chain entanglement generation
                # not being properly integrated. The qlink_builder will produce
                # a MagicLinkLayerProtocolWithSignaling object, but the cross-chain code requires usage of the
                # EntanglementService that requires MagicQLink objects. So we take out the MagicDistributor
                # from the linklayer and use it to create the MagicQLink
                link_layer = qlink_builder.build(end_node, repeater_node, qlink_config)
                magic_distributor = link_layer.magic_distributor
                magic_distributor.clear_all_callbacks()
                qlink = MagicQLink(magic_distributor)

                qlink_dict[(end_node.name, repeater_node.name)] = qlink
                qlink_dict[(repeater_node.name, end_node.name)] = qlink
                qlink.close()

        return qlink_dict

    def build_schedule(self, network: Network):
        if self.hub_configs is None:
            return
        for hub_config in self.hub_configs:
            schedule_builder = self.scheduler_builders[hub_config.schedule_typ]
            hub = network.hubs[hub_config.name]

            # Find all combinations between end nodes in the hub
            node_name_combinations = []
            node_name_combinations += list(itertools.permutations(hub.end_nodes.keys(), 2))

            # Find all combinations between end nodes and the first repeater node of connected chains
            repeater_node_names = [
                chain.get_connected_repeater(hub.name).name for chain in hub.chains
            ]
            node_name_combinations += list(
                itertools.product(hub.end_nodes.keys(), repeater_node_names)
            )
            node_name_combinations += list(
                itertools.product(repeater_node_names, hub.end_nodes.keys())
            )

            # Fetch the qlink objects from the name combinations
            qlinks = {
                (node_1, node_2): network.qlinks[(node_1, node_2)]
                for node_1, node_2 in node_name_combinations
            }

            schedule = schedule_builder.build(
                hub_config.name,
                network,
                qlinks,
                schedule_config=hub_config.schedule_cfg,
            )
            self.protocol_controller.register(schedule)
            hub.scheduler = schedule

            for node_name_combination in node_name_combinations:
                qlink = network.qlinks[node_name_combination]
                qlink.scheduler = schedule
