from __future__ import annotations

import logging
from typing import TYPE_CHECKING, Dict, List, Optional, Tuple, Type

from netsquid.components import Port
from netsquid.nodes import Node
from netsquid_driver.EGP import EGPService
from netsquid_magic.qlink import IQLink, MagicQLink

from netsquid_netbuilder.builder.builder_utils import (
    create_and_connect_ports,
    link_config_has_length,
    link_config_set_length,
)
from netsquid_netbuilder.modules.clinks.interface import ICLinkBuilder, ICLinkConfig
from netsquid_netbuilder.modules.long_distance_interface.interface import (
    ILongDistanceInterfaceBuilder,
    ILongDistanceInterfaceConfig,
)
from netsquid_netbuilder.modules.qdevices.interface import IQDeviceBuilder
from netsquid_netbuilder.modules.qlinks.interface import IQLinkBuilder, IQLinkConfig
from netsquid_netbuilder.modules.qrep_chain_control.interface import (
    IQRepChainControlBuilder,
    IQRepChainControlConfig,
)
from netsquid_netbuilder.network import Chain, MetroHub, Network, RepeaterNode
from netsquid_netbuilder.network_config import RepeaterChainConfig

if TYPE_CHECKING:
    from netsquid_netbuilder.builder.network_builder import ProtocolController

logger = logging.getLogger(__name__)


class ChainBuilder:
    def __init__(self, protocol_controller):
        self.protocol_controller: ProtocolController = protocol_controller
        self.qdevice_builders: Dict[str, Type[IQDeviceBuilder]] = {}
        self.qlink_builders: Dict[str, Type[IQLinkBuilder]] = {}
        self.qlink_configs: Dict[str, Type[IQLinkConfig]] = {}
        self.chain_configs: Optional[List[RepeaterChainConfig]] = None
        self.clink_builders: Dict[str, Type[ICLinkBuilder]] = {}
        self.clink_configs: Dict[str, Type[ICLinkConfig]] = {}
        self.long_distance_interface_builders: Dict[str, Type[ILongDistanceInterfaceBuilder]] = {}
        self.long_distance_interface_configs: Dict[str, Type[ILongDistanceInterfaceConfig]] = {}
        self.qrep_chain_control_builders: Dict[str, Type[IQRepChainControlBuilder]] = {}
        self.qrep_chain_control_configs: Dict[str, Type[IQRepChainControlConfig]] = {}

    def register_qdevice(self, model_name: str, builder: Type[IQDeviceBuilder]):
        self.qdevice_builders[model_name] = builder

    def register_qlink(
        self, model_name: str, builder: Type[IQLinkBuilder], config: Type[IQLinkConfig]
    ):
        self.qlink_builders[model_name] = builder
        self.qlink_configs[model_name] = config

    def register_clink(
        self, model_name: str, builder: Type[ICLinkBuilder], config: Type[ICLinkConfig]
    ):
        self.clink_builders[model_name] = builder
        self.clink_configs[model_name] = config

    def register_long_distance_interface(
        self,
        model_name: str,
        builder: Type[ILongDistanceInterfaceBuilder],
        config: Type[ILongDistanceInterfaceConfig],
    ):
        self.long_distance_interface_builders[model_name] = builder
        self.long_distance_interface_configs[model_name] = config

    def register_qrep_chain_control(
        self,
        model_name: str,
        builder: Type[IQRepChainControlBuilder],
        config: Type[IQRepChainControlConfig],
    ):
        self.qrep_chain_control_builders[model_name] = builder
        self.qrep_chain_control_configs[model_name] = config

    def set_configs(self, chain_configs: List[RepeaterChainConfig]):
        if chain_configs is None:
            self.chain_configs = []
            return
        if len(chain_configs) > 1:
            raise NotImplementedError("Currently we only support a single repeater chain")
        self.chain_configs = chain_configs

    def register(self, model_name: str, builder: Type[IQLinkBuilder], config: Type[IQLinkConfig]):
        self.qlink_builders[model_name] = builder
        self.qlink_configs[model_name] = config

    def create_chain_objects(self, hubs: Dict[str, MetroHub]) -> Dict[(str, str), Chain]:
        chains = {}
        if self.chain_configs is None:
            return chains
        for chain_config in self.chain_configs:
            chain = Chain(hub_1=hubs[chain_config.metro_hub1], hub_2=hubs[chain_config.metro_hub2])
            chains[(chain_config.metro_hub1, chain_config.metro_hub2)] = chain
            chains[(chain_config.metro_hub2, chain_config.metro_hub1)] = chain

            # Register chain object on the hubs
            hubs[chain_config.metro_hub1].chains.append(chain)
            hubs[chain_config.metro_hub2].chains.append(chain)

            chain.link_lengths = chain_config.lengths

        return chains

    def build_repeater_nodes(self, network: Network):
        if self.chain_configs is None:
            return

        for chain_config in self.chain_configs:
            chain = network.chains[(chain_config.metro_hub1, chain_config.metro_hub2)]
            for repeater_index, repeater_node in enumerate(chain_config.repeater_nodes):
                if repeater_node.qdevice_typ not in self.qdevice_builders.keys():
                    raise RuntimeError(f"No model of type: {repeater_node.qdevice_typ} registered")

                builder = self.qdevice_builders[repeater_node.qdevice_typ]

                qdevice = builder.build(
                    f"qdevice_{repeater_node.name}", qdevice_cfg=repeater_node.qdevice_cfg
                )

                node = RepeaterNode(
                    repeater_node.name,
                    qdevice=qdevice,
                    qdevice_type=repeater_node.qdevice_typ,
                )
                builder.build_services(node)

                # Add node to chain object
                chain.repeater_nodes.append(node)

    def build_clinks(
        self,
        network: Network,
    ) -> Dict[(str, str), Port]:
        ports: Dict[(str, str), Port] = {}
        if self.chain_configs is None:
            return ports
        for chain_config in self.chain_configs:
            chain = network.chains[(chain_config.metro_hub1, chain_config.metro_hub2)]
            clink_builder = self.clink_builders[chain_config.clink_typ]
            clink_cfg_typ = self.clink_configs[chain_config.clink_typ]
            clink_config = chain_config.clink_cfg

            if isinstance(clink_config, dict):
                clink_config = clink_cfg_typ(**chain_config.clink_cfg)
            if not isinstance(clink_config, clink_cfg_typ):  # noqa
                raise TypeError(
                    f"Incorrect configuration provided. Got {type(clink_config)},"
                    f" expected {clink_cfg_typ.__name__}"
                )

            if not hasattr(clink_config, "length"):
                logger.warning(
                    f"CLink type: {clink_cfg_typ} has no length attribute length,"
                    f"metro hub lengths wil not be used for this Clink."
                )

            if len(chain_config.lengths) != len(chain_config.repeater_nodes) + 1:
                raise ValueError(
                    f"The amount of repeater chain distances should be equal to"
                    f" the number of repeater nodes + 1 for chain {chain_config.metro_hub1}-{chain_config.metro_hub2}"
                )

            def _connect_nodes(node_1: Node, node_2: Node, length: float):
                if hasattr(clink_config, "length"):
                    clink_config.length = length

                connection = clink_builder.build(node_1, node_2, clink_config)

                ports.update(
                    create_and_connect_ports(
                        node_1, node_2, connection, port_name_prefix="external"
                    )
                )

            for chain_index in range(len(chain_config.repeater_nodes) - 1):
                n1 = chain.repeater_nodes[chain_index]
                n2 = chain.repeater_nodes[chain_index + 1]
                _connect_nodes(n1, n2, chain.link_lengths[chain_index + 1])

        return ports

    def build_qlinks(self, network: Network) -> Dict[(str, str), IQLink]:
        qlink_dict = {}
        if self.chain_configs is None:
            return qlink_dict
        for chain_config in self.chain_configs:
            qlink_builder = self.qlink_builders[chain_config.qlink_typ]
            qlink_cfg_typ = self.qlink_configs[chain_config.qlink_typ]
            qlink_config = chain_config.qlink_cfg
            chain = network.chains[(chain_config.metro_hub1, chain_config.metro_hub2)]

            if isinstance(qlink_config, dict):
                qlink_config = qlink_cfg_typ(**chain_config.qlink_cfg)
            if not isinstance(qlink_config, qlink_cfg_typ):  # noqa
                raise TypeError(
                    f"Incorrect configuration provided. Got {type(qlink_config)},"
                    f" expected {qlink_cfg_typ.__name__}"
                )

            if not link_config_has_length(qlink_config):
                logger.warning(
                    f"Link type: {qlink_cfg_typ} has no length attribute length,"
                    f"metro hub lengths wil not be used for this link."
                )

            # QLink repeater nodes
            for idx in range(len(chain.repeater_nodes) - 1):
                length = chain.link_lengths[idx + 1]
                link_config_set_length(qlink_config, length / 2, length / 2)

                # This section of code makes a "hack" due to direct link and cross-chain entanglement generation
                # not being properly integrated. The qlink_builder will produce
                # a MagicLinkLayerProtocolWithSignaling object, but the cross-chain code requires usage of the
                # EntanglementService that requires MagicQLink objects. So we take out the MagicDistributor
                # from the linklayer and use it to create the MagicQLink
                node1 = chain.repeater_nodes[idx]
                node2 = chain.repeater_nodes[idx + 1]
                link_layer = qlink_builder.build(node1, node2, qlink_config)
                magic_distributor = link_layer.magic_distributor
                magic_distributor.clear_all_callbacks()
                qlink = MagicQLink(magic_distributor)

                qlink_dict[(node1.name, node2.name)] = qlink
                qlink_dict[(node2.name, node1.name)] = qlink

        return qlink_dict

    def build_egp(self, network: Network) -> Dict[(str, str), EGPService]:
        num_repeater_chains = int(len(network.chains) / 2)
        egp_dict = {}

        if num_repeater_chains == 0:
            return egp_dict
        elif num_repeater_chains == 1:
            pass
        else:
            raise NotImplementedError("Simulation is currently limited to a single repeater chain")

        for chain_config in self.chain_configs:
            qrep_control_builder = self.qrep_chain_control_builders[
                chain_config.qrep_chain_control_typ
            ]
            qrep_control_cfg_typ = self.qrep_chain_control_configs[
                chain_config.qrep_chain_control_typ
            ]
            qrep_control_config = chain_config.qrep_chain_control_cfg
            chain = network.chains[(chain_config.metro_hub1, chain_config.metro_hub2)]

            if isinstance(qrep_control_config, dict):
                qrep_control_config = qrep_control_cfg_typ(**qrep_control_config)
            if not isinstance(qrep_control_config, qrep_control_cfg_typ):  # noqa
                raise TypeError(
                    f"Incorrect configuration provided. Got {type(qrep_control_config)},"
                    f" expected {qrep_control_cfg_typ.__name__}"
                )
            egp_dict.update(qrep_control_builder.build(chain, network, qrep_control_config))

        return egp_dict

    def build_long_distance_interfaces(self, network: Network):
        if self.chain_configs is None:
            return
        for chain_config in self.chain_configs:
            if chain_config.long_distance_interface_typ is None:
                return

            ld_interface_builder = self.long_distance_interface_builders[
                chain_config.long_distance_interface_typ
            ]
            ld_interface_cfg_typ = self.long_distance_interface_configs[
                chain_config.long_distance_interface_typ
            ]
            ld_interface_config = chain_config.long_distance_interface_cfg
            chain = network.chains[(chain_config.metro_hub1, chain_config.metro_hub2)]

            if isinstance(ld_interface_config, dict):
                ld_interface_config = ld_interface_cfg_typ(**ld_interface_config)
            if not isinstance(ld_interface_config, ld_interface_cfg_typ):  # noqa
                raise TypeError(
                    f"Incorrect configuration provided. Got {type(ld_interface_config)},"
                    f" expected {ld_interface_cfg_typ.__name__}"
                )

            qlink_keys = self._get_metro_to_repeater_qlink_keys(chain)

            for qlink_key in qlink_keys:
                ld_interface = ld_interface_builder.build(ld_interface_config)
                qlink = network.qlinks[qlink_key]
                qlink.magic_distributor.long_distance_interface = ld_interface

    @staticmethod
    def _get_metro_to_repeater_qlink_keys(chain: Chain) -> List[Tuple[str, str]]:
        """Get the list of keys (the node name pairs) for qlinks that connect between a repeater and metro hub."""
        link_keys = []
        for node in chain.hub_1.end_nodes.values():
            link_keys.append((node.name, chain.repeater_nodes[0].name))
        for node in chain.hub_2.end_nodes.values():
            link_keys.append((node.name, chain.repeater_nodes[-1].name))

        return link_keys
