netsquid_netbuilder.modules.scheduler
=======================================

netsquid_netbuilder.modules.scheduler.interface
-------------------------------------------------

 .. automodule:: netsquid_netbuilder.modules.scheduler.interface
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.scheduler.fifo
--------------------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.scheduler.fifo.FIFOScheduleConfig
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.scheduler.static
--------------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.scheduler.static.StaticScheduleConfig
    :members:
    :undoc-members:
    :member-order: bysource

