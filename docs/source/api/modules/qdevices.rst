netsquid_netbuilder.modules.qdevices
====================================

netsquid_netbuilder.modules.qdevices.interface
-------------------------------------------------

 .. automodule:: netsquid_netbuilder.modules.qdevices.interface
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.qdevices.generic
--------------------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.qdevices.generic.GenericQDeviceConfig
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.qdevices.nv
--------------------------------------------------
 .. autoclass:: netsquid_netbuilder.modules.qdevices.nv.NVQDeviceConfig
    :members:
    :undoc-members:
    :member-order: bysource

