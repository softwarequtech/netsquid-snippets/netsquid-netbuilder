netsquid_netbuilder.modules.clinks
====================================

netsquid_netbuilder.modules.clinks.interface
-------------------------------------------------

 .. automodule:: netsquid_netbuilder.modules.clinks.interface
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.clinks.instant
-----------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.clinks.instant.InstantCLinkConfig
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.clinks.default
------------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.clinks.default.DefaultCLinkConfig
    :members:
    :undoc-members:
    :member-order: bysource
