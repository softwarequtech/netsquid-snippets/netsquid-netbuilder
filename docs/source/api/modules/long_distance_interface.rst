netsquid_netbuilder.modules.long_distance_interface
======================================================

netsquid_netbuilder.modules.long_distance_interface.interface
----------------------------------------------------------------------

 .. automodule:: netsquid_netbuilder.modules.long_distance_interface.interface
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.long_distance_interface.depolarise
----------------------------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.long_distance_interface.depolarise.DepolariseLongDistanceInterfaceConfig
    :members:
    :undoc-members:
    :member-order: bysource

