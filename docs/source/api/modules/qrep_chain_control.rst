netsquid_netbuilder.modules.qrep_chain_control
==================================================

netsquid_netbuilder.modules.qrep_chain_control.interface
--------------------------------------------------------------

 .. automodule:: netsquid_netbuilder.modules.qrep_chain_control.interface
    :members:
    :undoc-members:
    :member-order: bysource


netsquid_netbuilder.modules.qrep_chain_control.swap_asap
---------------------------------------------------------------------

 .. autoclass:: netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_builder.SwapASAPConfig
    :members:
    :undoc-members:
    :member-order: bysource

