.. _label_tutorial_basics:

Basics
===============
This tutorial section will teach the basics of using the netsquid-netbuilder,
in particular we will introduce the basics of the: ``config.yaml``, ``programs.py`` and ``run_simulation.py`` files.

The examples of this and the following sections contain the code snippets that are used in this tutorial.
As such the examples may provide support in understanding the context of each of the snippets.
Additionally all examples are fully functional.
In order to run an example, one must first make the current example directory the active directory:

.. code-block:: bash

   cd examples/tutorial/1_basics

Afterwards one may run the simulation using:

.. code-block:: bash

   python3 run_simulation.py


Programs
--------

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_basics/programs.py AliceProgram
   :pyobject: AliceProgram
   :lines:  1-4

To write programs for nodes in a quantum network, create a class for your program that inherits from :py:class:`netsquid_netbuilder.run.Program`.
In this example we have created two classes: ``AliceProgram`` and ``BobProgram`` that will run on the Alice and Bob nodes respectively.

The ``run`` method is where one can write the code that the program will execute during the simulation.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_basics/programs.py AliceProgram
   :pyobject: AliceProgram
   :lines:  4-12

Programs are given a :py:class:`netsquid_netbuilder.run.ProgramContext` object under the property ``context``.
The ``context`` property provides access to various objects and protocols available to the current node, most importantly it has the following properties:

* The ``node`` property contains a processing node object. This in turn provides access to the ``qdevice`` and ``driver`` properties, these can be used for local quantum operations or accessing the various service protocols on the node.
* The ``egp`` property contains a dictionary with EGP protocols for generating entanglement with a remote node.
* The ``sockets`` property contains a dictionary with classical sockets that have been connected with each other. These sockets can be used for sending and receiving classical messages.
* ``node_id_mapping`` is a dictionary where that maps the node name to the corresponding node ID. This is primarily used to look up the node id from a node name for the requests that use node id instead of node name.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py AliceProgram
   :lines:  22-23

To send classical messages to other nodes, use the ``send`` method of the socket object associated with the target node.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py BobProgram
   :lines:  78

Receiving nodes use the ``recv`` method, and require adding ``yield from``, to receive messages.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py AliceProgram
   :lines:  27

The next action the AliceProgram makes, is to wait 1000 nanoseconds.
We do this in order to delay our next action, submitting the entanglement request,
to make sure Bob performs his end of the entanglement request submission before Alice makes hers.

.. note::
    The class :py:class:`netsquid_netbuilder.run.Program` is a child of :py:class:`netsquid.protocols.protocol.Protocol`.
    This means the various methods of ``Protocol`` such as ``await_timer`` and ``await_signal`` are inherited and can be used in your programs.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py AliceProgram
   :lines:  31-32

We initiate entanglement generation by submitting a :py:class:`qlink_interface.ReqCreateAndKeep` request at Alice with the ID of Bob.

.. warning::
    Not all parameters of the requests are supported in the current simulation.
    The most important parameter is ``remote_node_id``,
    this determines the target for entanglement generation.

.. note::
    There are currently two request types supported:

    #. :py:class:`qlink_interface.ReqCreateAndKeep` will create entangled qubits.
    #. :py:class:`qlink_interface.ReqMeasureDirectly` will create entangled qubits and measures the qubit as soon as it is created.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py BobProgram
   :lines:  81-82

Bob must submit a :py:class:`qlink_interface.ReqReceive` request
with the ID of Alice, otherwise it will refuse the entanglement request.
This is why we made Alice wait before sending her request for entanglement generation.

.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py AliceProgram
   :lines:  36-38

We will now wait until we receive a signal from the EGP protocol that a entangled qubit has been generated.
We do the exact same thing on Bob's side.

The entanglement generation will be executed by the EGP protocol independently from the Program,
but the EGP protocol will send a signal each time it has generated an entanglement.
These signals are qlink result objects (:py:class:`qlink_interface.ResCreateAndKeep` or :py:class:`qlink_interface.ResMeasureDirectly`),
these result objects importantly contain the location of the newly entangled qubit and the Bell index of the entangled pair.

.. note::
    The EGP protocol will send a signal for each generated entanglement,
    thus a single request for entanglement where multiple qubits are requested, will generate multiple signals.


.. literalinclude:: ../../../examples/tutorial/1_basics/programs.py
   :language: python
   :caption: examples/tutorial/1_Basics/application.py AliceProgram
   :lines:  42-62

Next we measure the entangled qubit on both Alice and Bob.
We also show how to create a local computation, with various gate operations, on multiple qubits.

Local quantum operations, such as measuring qubits, initializing qubits and applying gates,
can be made by creating a :py:class:`netsquid.components.QuantumProgram` and executing it on a quantum device.

The various gate operations are documented in the `Netsquid API documentation <https://docs.netsquid.org/latest-release/api_components/netsquid.components.qprocessor.html#netsquid.components.qprocessor.PhysicalInstruction>`_.
For a more exhaustive tutorial regarding the netsquid quantum processor, please visit the: `Netsquid Quantum Processor Tutorial <https://docs.netsquid.org/latest-release/tutorial.quantumprocessor.html>`_

.. warning::
    A qdevice model is completely free in what instructions it does or does not support,
    thus the QuantumProgram may not be executable if it uses instructions that the model does not support.


Config.yaml
-----------

The ``config.yaml`` file contains network configuration settings defining the network topology,
processing nodes, and links between nodes for both quantum and classical communication.

.. literalinclude:: ../../../examples/tutorial/1_basics/config.yaml
   :language: yaml
   :caption: examples/tutorial/1_basics/config.yaml


This file in this example represents a network with two nodes: Alice and Bob, we connect these nodes with a classical connection that has a delay of 400 nanoseconds
and a quantum connection that allows Alice and Bob to generate perfect entangled qubits with each other after 2000 nanoseconds.

The next :ref:`section <label_tutorial_network_config>` will describe the network configuration in more detail.

Run_simulation.py
-----------------
The ``run_simulation.py`` file that will use the ``config.yaml`` and ``programs.py`` files and run the simulation.

.. literalinclude:: ../../../examples/tutorial/1_basics/run_simulation.py
   :language: python
   :caption: examples/tutorial/1_basics/run_simulation.py

We start by loading the network configuration from the file ``config.yaml``.
This configuration serves as the blueprint for constructing the simulation network.
Using a network builder, the configuration is translated into the quantum network used for the simulation.

We initiate instances of the program classes that are defined in the ``programs.py`` file.
These programs are assigned to specific nodes using a dictionary ``programs``,
where we declare that the Alice node should run the ``alice_program`` and that the Bob node should run the ``bob_program``.

With the network created and programs assigned, the simulation is run using the ``run`` method.
In this method, the programs are assigned their context and the simulation is executed.