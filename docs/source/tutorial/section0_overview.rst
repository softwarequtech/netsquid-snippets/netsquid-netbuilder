.. _label_start_tutorial:

*****************
Overview
*****************
Welcome to the Netsquid-netbuilder tutorial.

In order to start the tutorial it is recommended to first install Netsquid-netbuilder and the required components.
This process is described in :ref:`label_installation`.

The tutorial sections are accompanied by code examples in the Netsquid-netbuilder package that are located in ``examples/tutorial`` folder of Netsquid-netbuilder.
The code examples shown in this tutorial are part of these examples.
It may be useful to browse through the examples when reading the tutorial to obtain the full context of the code in this tutorial.

Files
==========
A simulation of a quantum network has roughly three components: the network, the programs and the simulation.
In order to encourage modularity we split the code into three separate files, in line with the conceptual components:

``programs.py``
    This file contains the individual programs that run on end nodes.

    So, for example, for an application with three participants, it could contain: ``AliceProgram``, ``BobProgram`` and ``CharlieProgram``.

``config.yaml``
    The file that specifies the network.
    It controls the network layout and end node labels. Moreover, it specifies the link and node types and properties.

``run_simulation.py``
    The executable file that will run the simulation.
    In its most simple form it:

    1) Loads the network config from ``config.yaml``.
    2) Creates a Netsquid network from these configs using a builder.
    3) Loads the programs from ``programs.py``.
    4) Runs the simulation.

    In more advanced form it may specify various simulation settings, automate multiple simulation runs and handle the simulation output.


