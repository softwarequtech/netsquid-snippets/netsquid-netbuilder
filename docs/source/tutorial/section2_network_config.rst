.. _label_tutorial_network_config:

Network configuration
=======================
This tutorial section will introduce the more advanced networks that can be created in the ``config.yaml`` file.

The examples accompanying this tutorial are located in ``examples/tutorial/2_network_configs``.
We will introduce the various network configurations using three variations of the network configuration file, specifically:

#. ``three_node_network.yaml`` for basic multi node configuration.
#. ``metro_hub_network.yaml`` for metropolitan hub configuration.
#. ``two_connected_metro_hub_network.yaml`` for repeater chain configuration.

The ``run_simulation.py`` file has been adapted to run the simulation for each of these configurations with the same protocols.
The protocols in ``protocols.py`` have been modified from the previous section to generate and measure a configurable number of EPR pairs.


Basic network config
------------------------
We will first start with a basic multi node network configuration.
In this example we have created a network that consists of three nodes: Alice, Bob and Charlie.

.. image:: ../_static/Three_node_network.drawio.png
   :align: center

.. literalinclude:: ../../../examples/tutorial/2_network_configs/three_node_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/three_node_network.yaml

Processing nodes
+++++++++++++++++++
Processing nodes are nodes in the network that have a quantum device and are intended as "users" in the network.
The term "users" means that these nodes will request entanglement with each other in order to run an application,
so one user might act as a server and another as a client in a blind quantum computing application.

.. literalinclude:: ../../../examples/tutorial/2_network_configs/three_node_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/three_node_network.yaml
   :lines: 4-9

To add a processing node to the network, one has to specify three elements: ``name``, ``qdevice_typ`` and ``qdevice_cfg``.

The ``name`` specifies the name of the node. This name will be used in other components of the network configuration,
referring to a remote node inside programs and when assigning programs to a node.

The ``qdevice_typ`` specifies the model type to use for the quantum device on this node.
In this example we specify that we wish to use the "generic" model.
The generic quantum device is an idealized model.
It has models of noise, but lacks any peculiarities, such as certain operations and qubits experiencing more noise, that are found in most physical systems.

The other model names registered in the default network builder can be found in the API documentation :py:class:`netsquid_netbuilder.run.get_default_builder`.

The ``qdevice_cfg`` specifies the model configuration options.
In this example we specify that the device has three qubits and that measurement operations take a 100 nanoseconds.
This model has many more configuration options, but since we do not specify them, the default options will be used.

Each of the models has their own configuration options and defaults.
These options can be found in the configuration object itself or in the :ref:`module API documentation <label_api_modules>`.

QLinks
+++++++++++++++++++
QLinks represent the quantum connections in a network that allow the two connected nodes to generate entangled qubits.

.. literalinclude:: ../../../examples/tutorial/2_network_configs/three_node_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/three_node_network.yaml
   :lines: 21-28

To connect two nodes with a quantum connection, one has to specify:

#. ``node1`` The name of the first node being connected.
#. ``node2`` The name of the second node being connected.
#. ``typ`` The name of model used for entanglement generation.
#. ``cfg`` The configuration options for the model, the specific options can be found in the :ref:`module API documentation <label_api_modules>`.

.. note::
    It is possible to create networks where quantum links exist between A-B and B-C, but not A-C.
    Without an A-C qlink, A can not generate entanglement with C via the EGP protocols as introduced in the previous section.

    It is possible for A-C to generate entanglement if B generates entanglement with both parties and performs a swap.
    This is not natively supported, but it is possible to create a program for B to perform this action.

CLinks
+++++++++++++++++++

Clinks represent the classical connections in a network that allow the two connected nodes to send classical messages.

.. literalinclude:: ../../../examples/tutorial/2_network_configs/three_node_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/three_node_network.yaml
   :lines: 43-48

To connect two nodes with a classical connection, one has to specify:

#. ``node1`` The name of the first node being connected.
#. ``node2`` The name of the second node being connected.
#. ``typ`` The name of model used for entanglement generation.
#. ``cfg`` The configuration options for the model, the specific options can be found in the :ref:`module API documentation <label_api_modules>`.


.. note::
    Despite not linking Bob and Charlie with a classical link,
    Bob and Charlie will be able to send classical messages to each other.
    These messages will use the classical connection between Alice and Bob and Alice and Charlie.

    In the processes of building the network, the shortest paths between all nodes will be calculated
    and this information will be used for any classical message routing.


.. note::
    It is common in both link and clink models to have an option for distance, speed of light
    and a time delay option (for example: delay or t_cycle).
    These models require that one and only one of the options are given, so either distance and speed of light
    or the time delay are specified for these models.

    When these models are used as part of a metropolitan hub or repeater chain,
    only the speed of light is a configurable option.
    The network builder will assign distances to the various links as specified in the configuration of metropolitan hub or repeater chain.


Metropolitan hubs
------------------------
A metropolitan hub connects nodes via connections that go through the hub and acts like a switch.
In this network configuration we connect four nodes, Alice, Bob, Dummy1 and Dummy 2, with each other via a metropolitan hub.

The metropolitan hub has three main conceptual configuration options:

#. What processing nodes are connected to it and what is the distance between the hub and processing node.
#. The models for classical and quantum connections that connect the metropolitan hub to the connected nodes.
#. How does the metropolitan hub allocate resources for entanglement generation between the connected nodes.

.. image:: ../_static/Metrohub.drawio.png
   :align: center

.. literalinclude:: ../../../examples/tutorial/2_network_configs/metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/metro_hub_network.yaml

Metropolitan hub node
++++++++++++++++++++++
The metropolitan hub node is a node that acts like a switch in the network.
The hub itself does not have a quantum memory, but is envisioned to host one or more bell state detectors.
By connecting two nodes to the same bell state detector, the hub allows these nodes to generate entanglement with each other

.. literalinclude:: ../../../examples/tutorial/2_network_configs/metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/metro_hub_network.yaml
   :lines: 21-22

The metropolitan hub is added to the network by creating an entry in the ``hubs`` field.
The name of the metropolitan node is set via the parameter ``name``.

QLink models & Connections
++++++++++++++++++++++++++++++++

.. literalinclude:: ../../../examples/tutorial/2_network_configs/metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/metro_hub_network.yaml
   :lines: 23-41

To connect a processing node to the metropolitan hub, one must add an entry to the ``connections`` field
and specify the ``node`` name being connected as well as the ``length`` of the connection from the metropolitan hub to the processing node.

The quantum and classical connections will be created according to the distances given and the models for these connections
are specified by the ``qlink_typ``, ``qlink_cfg``, ``clink_typ`` and ``clink_cfg`` parameters.

.. note::
    QLinks model the behavior for generating entanglement between two quantum memory devices,
    thus in the simulation code, Qlinks are created between processing nodes, not metropolitan hub with processing node.
    The metropolitan hub only determines the qlink lengths and if this qlink can generate entanglement at a particular time via the scheduler.

    For QLink models there are three possible situations regarding how the qlinks lengths are set:

    1) In models that have a ``length_A`` and ``length_B`` parameter, these parameters will be set to the distance between the relevant node and the metropolitan hub.
    2) In models that only have a ``length`` parameter, this parameter will be set to the total length of the path between the two relevant nodes via the metropolitan hub.
    3) Models that do not have any length parameter will not be affected and therefore can't account for any effect of the distance of the qlink.


Scheduler
+++++++++++++
The scheduler is the component of the metropolitan hub that represents shared resources at the hub, like bell state detectors,
that are used to generate entanglement and the protocol that is used to allocate these shared resources.

.. literalinclude:: ../../../examples/tutorial/2_network_configs/metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/metro_hub_network.yaml
   :lines: 42-46

The model used in this example is the ``fifo`` model, it is an implementation of the first in first out basis (FIFO) strategy.
This strategy will give requests for entanglement generation unlimited time to complete their request.
Any subsequent requests that arrive while there is no more capacity are put into a queue that operates under the FIFO strategy.
We specify in the configuration options that the number of resources for entanglement generation is 1, thus only a single pair of nodes can generate entanglement at a time.
We also specify that it takes 100 ns to perform a switch of connected nodes, during this time the resource can not be used for entanglement generation.

Other models and their options can be found in the :ref:`module API documentation <label_api_modules>`.

Repeater chains
------------------------
A repeater chain connects metropolitan hubs with repeater nodes.
These nodes generate entanglement with each other and perform swap operations to create long distance entanglement.

The long distance interface provides a model to include the effects of a photon conversion
that may need to happen between repeater node an processing node.
This photon conversion is only required if the processing node and repeater node have quantum hardware with different frequency and waveform in emitted photons.
For the simulation it is not required to include the long distance interface, even if different qdevice models are used,
it exists to provide a way to introduce rate and fidelity loss associated with this process in the simulation.

In this example we have made two metropolitan hubs where one hub has an Alice node and the other the Bob node.
We connect the hubs by a repeater chain with two repeater nodes and we include models for photon conversion via the long distance interface models.

The repeater chain has 6 conceptual components:

#. The hubs being connected.
#. The distances between repeater nodes.
#. The specification of the repeater nodes and their models.
#. The classical and quantum models used inside the connections of the repeater chain.
#. If used, the model for the long distance interface.
#. The strategy and associated protocols used for generating long range entanglement.

.. image:: ../_static/Repeater1.drawio.png
   :align: center

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml

Connecting Metropolitan hubs
++++++++++++++++++++++++++++++++

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :lines: 76-78

The network configuration file incudes two metropolitan hubs with names Hub1 and Hub2.
The hubs are connected with the repeater chain by using the parameters ``metro_hub1`` and ``metro_hub2``.


Lengths
++++++++++

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :lines: 80-83

We specify the lengths of the connections in the repeater chain by providing a list of of lengths.
The first length is the length of the connection between Hub1 and repeater1,
the second length is between repeater1 and repeater2,
the last length is between repeater2 and Hub2.

Repeater nodes
+++++++++++++++++++

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :lines: 85-94

To add repeater nodes one adds an entry with:

* ``name`` The repeater node name.
* ``qdevice_typ`` The model type for the quantum device on the node.
* ``qdevice_cfg`` The configuration options for the quantum device.

.. warning::
    The amount of repeater nodes should be n-1 where n is the amount of lengths used.

QLink models
+++++++++++++++++++

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :lines: 96-102

Similar to the metropolitan hub the quantum and classical connections will be created according to the distances given and the models for these connections
are specified by the ``qlink_typ``, ``qlink_cfg``, ``clink_typ`` and ``clink_cfg`` parameters.

One important point of note, is that the models for qlink "exist" between nodes that have a quantum device.
Thus the entanglement generation between a repeater node and processing node is one qlink model,
despite being both part of the metropolitan hub and repeater chain.
The netsquid-netbuilder will use the qlink model of the metropolitan hub for this entanglment generation.
The image underneath gives a schematic representation of where in the network what model is used.

.. image:: ../_static/Repeater2.drawio.png
   :align: center

Long distance interface model
+++++++++++++++++++++++++++++++++++++

The long distance interface models emulate the photon conversion that may need to happen between two nodes.
The current long distance interface model modifies the existing qlink models that allows one to emulate a photon loss and fidelity loss in the conversion process.

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :lines: 104-107

If one wishes to include a long distance interface in the repeater chain then one can specify two parameters:

#. ``long_distance_interface_typ`` The model type used for the long distance interface.
#. ``long_distance_interface_cfg`` The configuration options for this long distance interface model.

Both of these parameters can be left empty if one does not wish to include a long distance interface.

The configuration options can be found in the :ref:`module API documentation <label_api_modules>`.


Quantum repeater chain control
+++++++++++++++++++++++++++++++++++++
The quantum repeater chain control allows one to configure the protocols and strategy used inside the repeater chain.

.. literalinclude:: ../../../examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :language: text
   :caption: examples/tutorial/2_network_configs/two_connected_metro_hub_network.yaml
   :lines: 108-112

We have configured the current model to use the "swapASAP" model.
The "swapASAP" model sets the repeater nodes to generate entanglement with two neighboring nodes and to perform a swap as soon as entanglements to both neighbors are ready.

The model used can be configured with the ``qrep_chain_control_typ`` parameter and any configuration options may be set using ``qrep_chain_control_cfg``.

The configuration options can be found in the :ref:`module API documentation <label_api_modules>`.
