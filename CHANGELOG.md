CHANGELOG
=========

2024-12-09 (0.1.3)
------------------
- Add support for random_bell_state option in depolarise qlink model

2024-11-28 (0.1.2)
------------------
- Bugfix: fix test_protocol_scheduler to return correct type for SchedulerCompletionEventInfo.epr_measure_result

2024-10-20 (0.1.1)
------------------
- Relaxed Pydantic requirement: Updated the Pydantic version constraint from <2.0 to <3.0 to allow support for Pydantic version 2.x.
- Minor fixes: Implemented necessary changes to ensure compatibility with Pydantic 2.x.

2024-09-25 (0.1.0)
------------------
- Created this snippet