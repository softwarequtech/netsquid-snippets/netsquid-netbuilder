Netsquid-netbuilder (0.1.3)
++++++++++++++++++++++++++++++++

Overview
==========
**Netsquid-netbuilder** is a framework that enables users to easily create and simulate quantum internet networks.
It features a modular network builder that constructs fully functional networks based on user-defined configurations.

Netsquid-netbuilder includes a set of models and a default network builder, allowing users to quickly create and simulate networks directly.
The framework is designed to be easily extendable, enabling users to incorporate new models for various components.

Netsquid-netbuilder supports the addition of custom models for:

* **Quantum devices (qdevices)**: Models of quantum hardware.
* **Classical links (clinks)**: Models for classical communication links.
* **Quantum links (qlinks)**: Models for quantum communication links.
* **Schedulers**: Protocols that manage entanglement generation between nodes connected by a metropolitan hub.
* **Quantum repeater chain control (qrep chain control)**: Protocols governing entanglement generation across nodes connected via a repeater chain.
* **Long-distance interfaces**: Models simulating the effects of photon conversion between a processing node and a repeater node.

Getting started
===============
To begin using **netsquid-netbuilder**, you can follow the tutorial provided in
the `netsquid-netbuilder documentation <https://docs.netsquid.org/snippets/netsquid-netbuilder/>`_.
Alternatively, you can explore the examples available in the `examples/tutorial` folder for hands-on learning.

The netsquid-netbuilder documentation includes `API documentation <https://docs.netsquid.org/snippets/netsquid-netbuilder/api_overview.html>`_.


.. installation-start-inclusion-marker-do-not-remove

Installation
============
Netsquid-netbuilder requires the `NetSquid <https://netsquid.org/>`_ Python package.
To install and use NetSquid, you need to first create an account for the `netsquid forum <https://forum.netsquid.org/ucp.php?mode=register>`_.
The username and password for this account are needed to install Netsquid-netbuilder.


Next the Netsquid-netbuilder needs to be cloned using git.

.. code-block:: bash

    git clone https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-netbuilder.git


The Netsquid-netbuilder install script requires the NetSquid user name and password to be set into environment variables.
This can be done by executing the following code, but with your own user name and password:

.. code-block:: bash

    export NETSQUIDPYPI_USER=user1234
    export NETSQUIDPYPI_PWD=password1234

Then, to install Netsquid-netbuilder execute the following command inside the newly created Netsquid-netbuilder folder:

.. code-block:: bash

   make install

To verify the installation, run:

.. code-block:: bash

   make verify

If this commands completes without errors, it means that Netsquid-netbuilder has been successfully installed and should work properly.

.. installation-end-inclusion-marker-do-not-remove

Known issues
===============

* Simultaneously initiating entanglement generation between nodes within the same hub and across hubs connected via a repeater chain may lead to a deadlock scenario if the FIFO scheduler is used.
* NV center qdevice models are not supported for repeater nodes.
* Using the `cutoff_time` in the swapASAP repeater chain control protocol can cause a deadlock due to an edge case.
  The edge case is caused by one party discarding the link before it receives the information that end to end entanglement
  has been established with that link while the other party registers a successful end to end entanglement and terminates his interaction.

Contributors
===============
In alphabetical order:

* Michał van Hooft (M.K.vanHooft@tudelft.nl)


This repository has copied significant sections of the netsquid-qrepchain repository.
The contributors of that repository at the time of copy where, in alphabetical order:

* Guus Avis
* Tim Coopmans
* Axel Dahlberg
* Francisco Ferreira da Silva
* Hana Jirovská
* David Maier
* Julian Rabbie