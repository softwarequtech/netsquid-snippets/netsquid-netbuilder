import itertools
import random
import unittest
from dataclasses import dataclass, field
from math import isclose
from typing import Dict, List, Tuple

import netsquid as ns
import numpy as np

from netsquid_netbuilder.modules.clinks.default import DefaultCLinkConfig
from netsquid_netbuilder.modules.clinks.instant import InstantCLinkConfig
from netsquid_netbuilder.modules.qdevices import GenericQDeviceConfig, NVQDeviceConfig
from netsquid_netbuilder.modules.qlinks import (
    DepolariseQLinkConfig,
    HeraldedDoubleClickQLinkConfig,
    HeraldedSingleClickQLinkConfig,
)
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkConfig
from netsquid_netbuilder.modules.qrep_chain_control import SwapASAPConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.fidelity import calculate_fidelity_epr
from netsquid_netbuilder.util.network_generation import create_two_connected_metro_hubs_network
from netsquid_netbuilder.util.test_protocol_clink import (
    ClassicalMessageEventRegistration,
    ClassicalReceiverProtocol,
    ClassicalSenderProtocol,
)
from netsquid_netbuilder.util.test_protocol_qlink import (
    CreateAndKeepEventRegistration,
    CreateAndKeepReceiveProtocol,
    CreateAndKeepSenderProtocol,
    MeasureDirectlyEventRegistration,
    MeasureDirectlyReceiveProtocol,
    MeasureDirectlySenderProtocol,
)


@dataclass
class QIANetworkDescription:
    distances_h1: List[float] = field(default_factory=list)
    distances_h2: List[float] = field(default_factory=list)
    repeater_distances: List[float] = field(default_factory=list)
    sender_names: List[str] = field(default_factory=list)
    receiver_names: List[str] = field(default_factory=list)


class TestRepeaterChain(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        ns.set_random_state(seed=42)
        np.random.seed(seed=42)
        random.seed(42)

        self.builder = get_default_builder()

    def tearDown(self) -> None:
        pass

    def _perform_classical_delay_test(
        self,
        network_cfg: NetworkConfig,
        sender_names: List[str],
        receiver_names: List[str],
        distances: Dict[Tuple[str, str], List[float]],
    ):
        network = self.builder.build(network_cfg)

        messages = ["test"]
        message_times = [0]
        protocols = {}
        expected_delays = {}
        event_register = ClassicalMessageEventRegistration()
        assert len(sender_names) == len(receiver_names)

        for i, (sender_name, receiver_name) in enumerate(zip(sender_names, receiver_names)):
            sender = ClassicalSenderProtocol(receiver_name, event_register, messages, message_times)
            receiver = ClassicalReceiverProtocol(sender_name, event_register)
            expected_delay = sum(distances[sender_name, receiver_name])

            protocols[sender_name] = sender
            protocols[receiver_name] = receiver

            expected_delays[sender_name] = expected_delay
            expected_delays[receiver_name] = expected_delay

        run(network, protocols)

        for classical_message_info in event_register.received:
            self.assertEqual(classical_message_info.msg, messages[0])
            self.assertAlmostEqual(
                classical_message_info.time,
                expected_delays[classical_message_info.peer],
                delta=1e-9,
            )

    @staticmethod
    def _calculate_end_node_to_end_node_distances(network: QIANetworkDescription):
        """Calculate the total distances between end nodes. (Only between end nodes in different hubs)"""
        distances_dict = {}
        for idx_h1, idx_h2 in itertools.product(
            range(len(network.sender_names)), range(len(network.receiver_names))
        ):
            # Calculate distance from sender to receiver
            distances_dict[(network.sender_names[idx_h1], network.receiver_names[idx_h2])] = (
                [network.distances_h1[idx_h1]]
                + network.repeater_distances
                + [network.distances_h2[idx_h2]]
            )
            # Use symetry to set receiver to sender distance from earlier calculation
            distances_dict[
                (network.receiver_names[idx_h2], network.sender_names[idx_h1])
            ] = distances_dict[(network.sender_names[idx_h1], network.receiver_names[idx_h2])]

        return distances_dict

    def test_classical_delay(self):
        num_end_nodes = 6
        network = QIANetworkDescription()
        network.distances_h1 = [12, 53, 38]
        network.distances_h2 = [81, 1, 8]
        network.repeater_distances = [213, 2, 51, 89]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
        )

        distances_dict = self._calculate_end_node_to_end_node_distances(network)

        self._perform_classical_delay_test(
            network_cfg, network.sender_names, network.receiver_names, distances_dict
        )

    def _calculate_fidelities(self, event_register: CreateAndKeepEventRegistration) -> List[float]:
        received_egp_with_full_dm = [
            received_egp
            for received_egp in event_register.received_ck
            if received_egp.dm.shape[0] > 2
        ]
        # The protocol will discard qubits after registering the results, thereby destroying half of the state.
        # The second party to look at the qubit state, will thus see a DM with only one qubit.
        self.assertEqual(len(received_egp_with_full_dm), len(event_register.received_ck) / 2)

        fidelities = []
        for received_egp in received_egp_with_full_dm:
            fid = calculate_fidelity_epr(received_egp.dm, received_egp.result.bell_state)
            fidelities.append(fid)
        return fidelities

    def _check_fidelity(
        self,
        event_register: CreateAndKeepEventRegistration,
        expected_fidelity=1.0,
        tolerated_error=0.01,
    ):
        fidelities = self._calculate_fidelities(event_register)
        for fid in fidelities:
            self.assertAlmostEqual(fid, expected_fidelity, delta=tolerated_error)

    def _check_timing_parallel(
        self,
        event_register: CreateAndKeepEventRegistration,
        distance_dict: Dict[Tuple[str, str], List[float]],
    ):
        current_time = 0
        for received_egp in event_register.received_ck[::2]:
            distances = distance_dict[(received_egp.node_name, received_egp.peer_name)]

            # Calculate the distances of all the qlinks, that requires combining distances
            # from metro hub to first repeater.
            distances_qlinks = [distances[0] + distances[1]]
            distances_qlinks += distances[2:-2]
            distances_qlinks += [distances[-2] + distances[-1]]

            # Assumption here is that all links generate entanglement at the same time, so total process completes
            # after longest link is done.
            expected_time = max(distances_qlinks) + current_time
            current_time = expected_time
            self.assertAlmostEqual(expected_time, received_egp.time)

    def _check_timing_non_parallel(
        self,
        event_register: CreateAndKeepEventRegistration,
        distance_dict: Dict[Tuple[str, str], List[float]],
        max_exceptions=0,
    ):
        """Non-parallel repeater chain allows for scenario's that are non-optimal and do not match this calculation.
        `max_exceptions` gives a maximum for how many non-optimal scenario's can occur."""
        exceptions = []
        current_time = 0
        for received_egp in event_register.received_ck[::2]:
            distances = distance_dict[(received_egp.node_name, received_egp.peer_name)]

            # Calculate the distances of all the qlinks, that requires combining distances
            # from metro hub to first repeater.
            distances_qlinks = [distances[0] + distances[1]]
            distances_qlinks += distances[2:-2]
            distances_qlinks += [distances[-2] + distances[-1]]

            # Assumption here is that there will be two phases in entanglement generation.
            # One where all even links can generate entanglement and one where odd links can generate entanglement
            # Each phase is expected to take as long as the longest link in the group.
            expected_time = max(distances_qlinks[::2]) + max(distances_qlinks[1::2]) + current_time
            current_time = received_egp.time
            if not isclose(expected_time, received_egp.time):
                exceptions.append({"expected_time": expected_time, "time": received_egp.time})

        self.assertLessEqual(len(exceptions), max_exceptions, msg=str(exceptions))

    def _check_create_id(self, event_register: CreateAndKeepEventRegistration):
        results = event_register.received_ck

        for i, submission in enumerate(event_register.submitted_ck):
            assert isinstance(submission.create_id, int)
            self.assertEqual(submission.create_id, i)
            self.assertEqual(results[2 * i].result.create_id, submission.create_id)

    def _check_measure_directly(self, event_register: MeasureDirectlyEventRegistration):
        self.assertEqual(len(event_register.submitted_md) * 2, len(event_register.received_md))

        for event in event_register.received_md:
            mirror_events = [
                temp_evt
                for temp_evt in event_register.received_md
                if temp_evt.peer_name == event.node_name
                and temp_evt.node_name == event.peer_name
                and temp_evt.result.create_id == event.result.create_id
            ]
            self.assertEqual(len(mirror_events), 1)
            mirror_event = mirror_events[0]

            if event.result.bell_state in [ns.BellIndex.PHI_PLUS, ns.BellIndex.PHI_MINUS]:
                self.assertEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )
            else:
                assert event.result.bell_state in [ns.BellIndex.PSI_PLUS, ns.BellIndex.PSI_MINUS]
                self.assertNotEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )

    def _perform_epr_test_run(
        self,
        network_cfg: NetworkConfig,
        sender_names: List[str],
        receiver_names: List[str],
        minimum_fidelity: float = 0,
        num_epr: int = 1,
        end_time: float = None,
    ):
        network = self.builder.build(network_cfg)

        protocols = {}
        event_register = CreateAndKeepEventRegistration()
        assert len(sender_names) == len(receiver_names)

        for sender_name, receiver_name in zip(sender_names, receiver_names):
            sender = CreateAndKeepSenderProtocol(
                receiver_name, event_register, minimum_fidelity=minimum_fidelity, n_epr=num_epr
            )
            receiver = CreateAndKeepReceiveProtocol(sender_name, event_register, n_epr=num_epr)

            protocols[sender_name] = sender
            protocols[receiver_name] = receiver

        run(network, protocols, end_time=end_time)

        return event_register

    def _perform_measure_directly_test_run(
        self,
        network_cfg: NetworkConfig,
        sender_names: List[str],
        receiver_names: List[str],
        minimum_fidelity: float = 0,
        num_epr: int = 1,
        end_time: float = None,
    ):
        network = self.builder.build(network_cfg)

        protocols = {}
        event_register = MeasureDirectlyEventRegistration()
        assert len(sender_names) == len(receiver_names)

        for sender_name, receiver_name in zip(sender_names, receiver_names):
            sender = MeasureDirectlySenderProtocol(
                receiver_name, event_register, minimum_fidelity=minimum_fidelity, n_epr=num_epr
            )
            receiver = MeasureDirectlyReceiveProtocol(sender_name, event_register, n_epr=num_epr)

            protocols[sender_name] = sender
            protocols[receiver_name] = receiver

        run(network, protocols, end_time=end_time)

        return event_register

    def _check_delays(
        self, event_register: CreateAndKeepEventRegistration, distances: Dict[str, float]
    ):
        for received_egp in event_register.received_ck:
            dist1 = distances[received_egp.node_name]
            dist2 = distances[received_egp.peer_name]

            single_com_time = dist1 + dist2
            classical_handshake = single_com_time
            epr_round_time = single_com_time
            expected_delay = classical_handshake + epr_round_time

            self.assertAlmostEqual(expected_delay, received_egp.time, delta=1e-9)

    def test_repeater_perfect_delay_long_hubs(self):
        num_end_nodes = 6
        num_epr = 4
        network = QIANetworkDescription()
        network.distances_h1 = [12, 53, 38]
        network.distances_h2 = [81, 1, 8]
        network.repeater_distances = [213, 2, 51, 89]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        distances_dict = self._calculate_end_node_to_end_node_distances(network)

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        self._check_fidelity(event_register)
        self._check_timing_parallel(event_register, distances_dict)

    def test_repeater_create_id(self):
        num_end_nodes = 6
        num_epr = 10
        network = QIANetworkDescription()
        network.distances_h1 = [12, 53, 38]
        network.distances_h2 = [81, 1, 8]
        network.repeater_distances = [213, 2, 51, 89]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, [network.sender_names[0]], [network.receiver_names[0]], num_epr=num_epr
        )

        self._check_create_id(event_register)

    def test_repeater_measure_directly(self):
        num_end_nodes = 6
        num_epr = 10
        network = QIANetworkDescription()
        network.distances_h1 = [12, 53, 38]
        network.distances_h2 = [81, 1, 8]
        network.repeater_distances = [213, 2, 51, 89]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )
        event_register = self._perform_measure_directly_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        self._check_measure_directly(event_register)

    def test_repeater_perfect_delay_long_chain(self):
        num_end_nodes = 6
        num_epr = 4
        network = QIANetworkDescription()
        network.distances_h1 = [10, 5, 12]
        network.distances_h2 = [2, 1, 8]
        network.repeater_distances = [1, 378, 58, 4]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        distances_dict = self._calculate_end_node_to_end_node_distances(network)

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        self._check_fidelity(event_register)
        self._check_timing_parallel(event_register, distances_dict)

    def test_repeater_perfect_non_parallel(self):
        num_end_nodes = 6
        num_epr = 4
        network = QIANetworkDescription()
        network.distances_h1 = [50, 45, 67]
        network.distances_h2 = [6, 6, 64]
        network.repeater_distances = [7, 10, 12, 7, 5]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=False),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        distances_dict = self._calculate_end_node_to_end_node_distances(network)

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        self._check_fidelity(event_register)
        self._check_timing_non_parallel(event_register, distances_dict, max_exceptions=1)

    def test_repeater_depolarise(self):
        num_end_nodes = 6
        num_epr = 4
        repeater_link_fidelity = 0.9
        network = QIANetworkDescription()
        network.distances_h1 = [10, 5, 12]
        network.distances_h2 = [2, 1, 8]
        network.repeater_distances = [6, 378, 58, 4]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="depolarise",
            qlink_cfg=DepolariseQLinkConfig(fidelity=repeater_link_fidelity, prob_success=0.0001),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        # Set link fidelity in the hubs to 1
        for hub_cfg in network_cfg.hubs:
            hub_cfg.qlink_cfg = DepolariseQLinkConfig(fidelity=1, prob_success=1)

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        num_repeater_links = len(network.repeater_distances) - 2
        # Expected fidelity is a rough calculation, not valid for low fidelity
        expected_fidelity = repeater_link_fidelity**num_repeater_links
        self._check_fidelity(
            event_register, expected_fidelity=expected_fidelity, tolerated_error=0.02
        )

    def test_repeater_cutoff(self):
        """Situation to test that the cutoff ensures a minimum fidelity of generated EPR pairs in
         the presence of depolarisation due to qubit time in memory.

        Long distance link has a low success rate, MH links have a significantly higher success rate.
        Cutoff time ensures that MH links will keep getting regenerated.

        This test has two parts and will test that with cutoff, the fidelity remains above the minimum.
        The second part will verify that without cutoff the fidelity dips below the minimum.
        """
        num_end_nodes = 4
        num_epr = 4
        network = QIANetworkDescription()
        network.distances_h1 = [5, 5]
        network.distances_h2 = [5, 5]
        network.repeater_distances = [5, 10, 5]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        speed_of_light = 200_000
        cycle_time = 10 / speed_of_light * 1e9
        cutoff_time = 100.2 * cycle_time
        depolarization_time = 500 * cycle_time

        mh_link_prob_success = 0.1
        ld_link_prob_success = 0.01

        # Value of 0.65 for fidelity was established via empirical testing of this scenario
        minimum_fid = 0.65

        qdevice_cfg = GenericQDeviceConfig.perfect_config(num_qubits=2)
        qdevice_cfg.T1 = depolarization_time
        qdevice_cfg.T2 = depolarization_time

        # Make a method of this code as we will use this code twice, with and without cutoff
        def _create_test_network_cfg(cutoff_time_):
            network_cfg_ = create_two_connected_metro_hubs_network(
                hub1_node_names=network.sender_names,
                hub1_node_distances=network.distances_h1,
                hub2_node_names=network.receiver_names,
                hub2_node_distances=network.distances_h2,
                repeater_chain_distances=network.repeater_distances,
                qlink_typ="depolarise",
                qlink_cfg=DepolariseQLinkConfig(fidelity=1, prob_success=ld_link_prob_success),
                clink_typ="default",
                clink_cfg=DefaultCLinkConfig(),
                qdevice_typ="generic",
                qdevice_cfg=qdevice_cfg,
                qrep_chain_control_typ="swapASAP",
                qrep_chain_control_cfg=SwapASAPConfig(
                    parallel_link_generation=True, cutoff_time=cutoff_time_
                ),
            )

            # Set prob_success of mh links to a different value
            for hub_cfg in network_cfg_.hubs:
                hub_cfg.qlink_cfg = DepolariseQLinkConfig(
                    fidelity=1, prob_success=mh_link_prob_success
                )
            return network_cfg_

        network_cfg = _create_test_network_cfg(cutoff_time_=cutoff_time)
        event_register = self._perform_epr_test_run(
            network_cfg,
            network.sender_names,
            network.receiver_names,
            num_epr=num_epr,
            end_time=1000 * cutoff_time,
        )

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        fidelities = self._calculate_fidelities(event_register)
        for fid in fidelities:
            self.assertGreater(fid, minimum_fid)

        # Run a verification that without cutoff time, fidelity can dip below minimum fidelity
        network_cfg = _create_test_network_cfg(cutoff_time_=None)
        event_register = self._perform_epr_test_run(
            network_cfg,
            network.sender_names,
            network.receiver_names,
            num_epr=num_epr,
        )

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        fidelities = self._calculate_fidelities(event_register)
        fidelity_below_minimum = [fid < minimum_fid for fid in fidelities]
        self.assertIn(
            True,
            fidelity_below_minimum,
            msg=f"Verification without cutoff: No fidelity below {minimum_fid} in {fidelities}",
        )

    def test_repeater_single_click_alpha_from_link(self):
        """Test to check that the alpha parameter will be fetched from the qlink model.

        The calculations for expected fidelity and alpha parameter value were made up with some logical reasoning
        and verified with the tests, but are not expected to be accurate
         and will be fully incorrect for smaller fidelity values.
        """
        num_end_nodes = 6
        num_epr = 4
        link_fidelity = 0.97
        alpha = (1 - link_fidelity) * 2
        network = QIANetworkDescription()
        network.distances_h1 = [10, 35, 10]
        network.distances_h2 = [46, 10, 12]
        network.repeater_distances = [10, 30, 53, 10]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="heralded-single-click",
            qlink_cfg=HeraldedSingleClickQLinkConfig(
                p_loss_init=0, p_loss_length=0, bright_state_parameter=alpha
            ),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        num_links = len(network.repeater_distances)
        # Expected fidelity is a rough calculation, not valid for low fidelity
        expected_fidelity = link_fidelity**num_links
        self._check_fidelity(
            event_register, expected_fidelity=expected_fidelity, tolerated_error=0.02
        )

    def test_repeater_single_click_alpha_from_qdevice(self):
        """Test to check that the alpha parameter will be fetched from the qdevice external parameters.

        The calculations for expected fidelity and alpha parameter value were made up with some logical reasoning
        and verified with the tests, but are not expected to be accurate
         and will be fully incorrect for smaller fidelity values.
        """
        num_end_nodes = 6
        num_epr = 4
        link_fidelity = 0.97
        alpha = (1 - link_fidelity) * 2
        network = QIANetworkDescription()
        network.distances_h1 = [10, 4, 4]
        network.distances_h2 = [10, 6, 7]
        network.repeater_distances = [31, 30, 53, 8]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="heralded-single-click",
            qlink_cfg=HeraldedSingleClickQLinkConfig(p_loss_init=0, p_loss_length=0),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )
        for node in network_cfg.processing_nodes:
            node.qdevice_cfg.external_params = {"bright_state_parameter": alpha}

        for node in network_cfg.repeater_chains[0].repeater_nodes:
            node.qdevice_cfg.external_params = {"bright_state_parameter": alpha}

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        num_links = len(network.repeater_distances)
        # Expected fidelity is a rough calculation, not valid for low fidelity
        expected_fidelity = link_fidelity**num_links
        self._check_fidelity(
            event_register, expected_fidelity=expected_fidelity, tolerated_error=0.02
        )

    def test_single_click_no_alpha(self):
        qlink_cfg = HeraldedSingleClickQLinkConfig(p_loss_init=0, p_loss_length=0)

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=["Alice"],
            hub1_node_distances=[21],
            hub2_node_names=["Bob"],
            hub2_node_distances=[10],
            repeater_chain_distances=[10, 41, 5],
            qlink_typ="heralded-single-click",
            qlink_cfg=qlink_cfg,
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        with self.assertRaises(ValueError) as context:
            run(network, {"Alice": alice_program, "Bob": bob_program})

        self.assertTrue(
            "Single click model requires a bright state parameter value for A and B sides."
            in context.exception.args
        )

    def test_repeater_heralded_double_click(self):
        num_end_nodes = 6
        num_epr = 4
        network = QIANetworkDescription()
        network.distances_h1 = [10, 55, 3]
        network.distances_h2 = [10, 56, 2]
        network.repeater_distances = [10, 30, 53, 22]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="heralded-double-click",
            qlink_cfg=HeraldedDoubleClickQLinkConfig(),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(),
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=True),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names, num_epr=num_epr
        )

        self.assertEqual(len(event_register.received_ck), num_end_nodes * num_epr)
        self._check_fidelity(event_register)

    @unittest.expectedFailure
    def test_repeater_nv(self):
        num_end_nodes = 6
        network = QIANetworkDescription()
        network.distances_h1 = [33, 11, 88]
        network.distances_h2 = [22, 66, 8]
        network.repeater_distances = [1, 6, 64, 212]
        network.sender_names = [f"sender_{i}" for i in range(num_end_nodes // 2)]
        network.receiver_names = [f"receiver_{i}" for i in range(num_end_nodes // 2)]

        qdevice_cfg = NVQDeviceConfig.perfect_config()

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=network.sender_names,
            hub1_node_distances=network.distances_h1,
            hub2_node_names=network.receiver_names,
            hub2_node_distances=network.distances_h2,
            repeater_chain_distances=network.repeater_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            qdevice_typ="nv",
            qdevice_cfg=qdevice_cfg,
            qrep_chain_control_typ="swapASAP",
            qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=False),
        )

        event_register = self._perform_epr_test_run(
            network_cfg, network.sender_names, network.receiver_names
        )

        distances_dict = self._calculate_end_node_to_end_node_distances(network)

        self._check_fidelity(event_register)
        self._check_timing_parallel(event_register, distances_dict)
