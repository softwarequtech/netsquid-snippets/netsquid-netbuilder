import unittest
from typing import Dict, List, Tuple

import netsquid as ns
import numpy as np

from netsquid_netbuilder.modules.clinks.instant import InstantCLinkConfig
from netsquid_netbuilder.modules.long_distance_interface.depolarise import (
    DepolariseLongDistanceInterfaceConfig,
)
from netsquid_netbuilder.modules.qlinks.depolarise import DepolariseQLinkConfig
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.fidelity import (
    calculate_fidelity_epr,
    fidelity_to_prob_max_mixed,
    prob_max_mixed_to_fidelity,
)
from netsquid_netbuilder.util.network_generation import create_two_connected_metro_hubs_network
from netsquid_netbuilder.util.test_protocol_qlink import (
    CreateAndKeepEventRegistration,
    CreateAndKeepReceiveProtocol,
    CreateAndKeepSenderProtocol,
)


class TestLongDistanceInterfaceChain(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_random_state(seed=42)
        np.random.seed(seed=42)

        ns.set_qstate_formalism(ns.QFormalism.DM)

        self.builder = get_default_builder()

    def tearDown(self) -> None:
        pass

    def _check_fidelity(self, event_register: CreateAndKeepEventRegistration):
        received_egp_with_full_dm = [
            received_egp
            for received_egp in event_register.received_ck
            if received_egp.dm.shape[0] > 2
        ]
        # The protocol will discard qubits after registering the results, thereby destroying half of the state.
        # The second party to look at the qubit state, will thus see a DM with only one qubit.
        self.assertEqual(len(received_egp_with_full_dm), len(event_register.received_ck) / 2)

        for received_egp in received_egp_with_full_dm:
            fid = calculate_fidelity_epr(received_egp.dm, received_egp.result.bell_state)
            self.assertGreater(fid, 0.99)

    def _check_timing(
        self,
        event_register: CreateAndKeepEventRegistration,
        distance_dict: Dict[Tuple[str, str], List[float]],
    ):
        for received_egp in event_register.received_ck:
            distances = distance_dict[(received_egp.node_name, received_egp.peer_name)]

            # Calculate the distances of all the qlinks, that requires combining distances
            # from metro hub to first repeater.
            distances_qlinks = [distances[0] + distances[1]]
            distances_qlinks += distances[2:-2]
            distances_qlinks += [distances[-2] + distances[-1]]

            expected_time = max(distances_qlinks)
            self.assertAlmostEqual(expected_time, received_egp.time)

    def _perform_epr_test_run(
        self,
        network_cfg: NetworkConfig,
        num_epr: int,
        minimum_fidelity: float = 0,
    ):
        network = self.builder.build(network_cfg)

        protocols = {}
        event_register = CreateAndKeepEventRegistration()

        sender_name = "sender"
        receiver_name = "receiver"
        sender = CreateAndKeepSenderProtocol(
            receiver_name, event_register, minimum_fidelity=minimum_fidelity, n_epr=num_epr
        )
        receiver = CreateAndKeepReceiveProtocol(sender_name, event_register, n_epr=num_epr)

        protocols[sender_name] = sender
        protocols[receiver_name] = receiver

        run(network, protocols)

        received_egp_with_full_dm = [
            received_egp
            for received_egp in event_register.received_ck
            if received_egp.dm.shape[0] > 2
        ]

        fid_list = [
            calculate_fidelity_epr(r.dm, r.result.bell_state) for r in received_egp_with_full_dm
        ]
        fid = np.average(fid_list)

        times = sorted([r.time for r in received_egp_with_full_dm])
        completion_times = [times[0] - event_register.received_classical[0].time]
        completion_times += [times[i + 1] - times[i] for i in range(len(times) - 1)]

        completion_time = np.average(completion_times)
        return fid, completion_time

    def _create_network(
        self,
        repeater_distances: list,
        node_mh_distance: float,
        qlink_typ: str,
        qlink_cfg,
        long_distance_interface_typ: str,
        long_distance_interface_cfg,
    ) -> NetworkConfig:
        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=["sender"],
            hub1_node_distances=[node_mh_distance],
            hub2_node_names=["receiver"],
            hub2_node_distances=[node_mh_distance],
            repeater_chain_distances=repeater_distances,
            qlink_typ=qlink_typ,
            qlink_cfg=qlink_cfg,
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            long_distance_interface_typ=long_distance_interface_typ,
            long_distance_interface_cfg=long_distance_interface_cfg,
        )
        return network_cfg

    def test_long_distance_interface_perfect_link_perfect_interface(self):
        repeater_distances = [10, 20, 20, 10]
        node_mh_distance = 10

        qlink_cfg = PerfectQLinkConfig(speed_of_light=1e9)
        long_distance_interface_cfg = DepolariseLongDistanceInterfaceConfig(fidelity=1, p_loss=0)
        network_cfg = self._create_network(
            repeater_distances,
            node_mh_distance,
            qlink_typ="perfect",
            qlink_cfg=qlink_cfg,
            long_distance_interface_typ="depolarise",
            long_distance_interface_cfg=long_distance_interface_cfg,
        )

        fid, completion_time = self._perform_epr_test_run(network_cfg, num_epr=1)

        self.assertAlmostEqual(fid, 1, delta=1e-9)
        self.assertAlmostEqual(completion_time, 20, delta=1e-9)

    def test_long_distance_interface_perfect_link(self):
        repeater_distances = [10, 20, 20, 10]
        node_mh_distance = 10
        prob_max_mixed_single_interface = 0.3
        prob_loss_single_interface = 0.7

        qlink_cfg = DepolariseQLinkConfig(speed_of_light=1e9, fidelity=1, prob_success=1)
        long_distance_interface_cfg = DepolariseLongDistanceInterfaceConfig(
            prob_max_mixed=prob_max_mixed_single_interface, p_loss=prob_loss_single_interface
        )

        network_cfg = self._create_network(
            repeater_distances,
            node_mh_distance,
            qlink_typ="depolarise",
            qlink_cfg=qlink_cfg,
            long_distance_interface_typ="depolarise",
            long_distance_interface_cfg=long_distance_interface_cfg,
        )

        fid, completion_time = self._perform_epr_test_run(network_cfg, num_epr=100)

        # Find expected fidelity by computing probability that the state is not maximally mixed
        prob_max_mixed = 1 - (1 - prob_max_mixed_single_interface) ** 2

        # Find the expected completion time
        assert repeater_distances[0] == repeater_distances[-1]
        single_epr_attempt_time = node_mh_distance + repeater_distances[0]
        expected_completion_time = (
            3 / 2 * single_epr_attempt_time / (1 - prob_loss_single_interface)
        )

        self.assertAlmostEqual(fid, prob_max_mixed_to_fidelity(prob_max_mixed), delta=1e-9)
        self.assertAlmostEqual(
            completion_time, expected_completion_time, delta=expected_completion_time * 0.05
        )

    def test_long_distance_interface(self):
        repeater_distances = [10, 20, 20, 10]
        node_mh_distance = 10
        prob_max_mixed_single_interface = 0.2
        link_fidelity = 0.9
        prob_loss_single_interface = 1 - 0.001
        prob_success_link = 0.1

        qlink_cfg = DepolariseQLinkConfig(
            speed_of_light=1e9, fidelity=link_fidelity, prob_success=prob_success_link
        )
        long_distance_interface_cfg = DepolariseLongDistanceInterfaceConfig(
            prob_max_mixed=prob_max_mixed_single_interface, p_loss=prob_loss_single_interface
        )

        network_cfg = self._create_network(
            repeater_distances,
            node_mh_distance,
            qlink_typ="depolarise",
            qlink_cfg=qlink_cfg,
            long_distance_interface_typ="depolarise",
            long_distance_interface_cfg=long_distance_interface_cfg,
        )

        fid, completion_time = self._perform_epr_test_run(network_cfg, num_epr=100)

        # Find expected fidelity by computing probability that the state is not maximally mixed
        prob_max_mixed_link = fidelity_to_prob_max_mixed(link_fidelity)
        prob_max_mixed = (
            1 - (1 - prob_max_mixed_single_interface) ** 2 * (1 - prob_max_mixed_link) ** 4
        )

        # Find the expected completion time, the contribution to the expected completion time from the links without
        # photonic interface is ignored
        assert repeater_distances[0] == repeater_distances[-1]
        single_epr_attempt_time = node_mh_distance + repeater_distances[0]
        expected_completion_time = (
            3 / 2 * single_epr_attempt_time / ((1 - prob_loss_single_interface) * prob_success_link)
        )

        self.assertAlmostEqual(fid, prob_max_mixed_to_fidelity(prob_max_mixed), delta=1e-9)
        self.assertAlmostEqual(
            completion_time, expected_completion_time, delta=expected_completion_time * 0.06
        )
