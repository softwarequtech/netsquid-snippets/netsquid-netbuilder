import pathlib
import unittest
from typing import Dict, List

import netsquid as ns

from netsquid_netbuilder.modules.clinks.default import DefaultCLinkConfig
from netsquid_netbuilder.modules.qlinks.depolarise import DepolariseQLinkConfig
from netsquid_netbuilder.modules.qlinks.heralded_double_click import HeraldedDoubleClickQLinkConfig
from netsquid_netbuilder.modules.qlinks.heralded_single_click import HeraldedSingleClickQLinkConfig
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkConfig
from netsquid_netbuilder.modules.scheduler.fifo import FIFOScheduleConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.fidelity import calculate_fidelity_epr
from netsquid_netbuilder.util.network_generation import create_metro_hub_network
from netsquid_netbuilder.util.test_protocol_clink import (
    ClassicalMessageEventRegistration,
    ClassicalReceiverProtocol,
    ClassicalSenderProtocol,
)
from netsquid_netbuilder.util.test_protocol_qlink import (
    CreateAndKeepEventRegistration,
    CreateAndKeepReceiveProtocol,
    CreateAndKeepSenderProtocol,
    MeasureDirectlyEventRegistration,
    MeasureDirectlyReceiveProtocol,
    MeasureDirectlySenderProtocol,
)


class TestMetropolitanHub(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)

        self.builder = get_default_builder()

    def tearDown(self) -> None:
        pass

    def _perform_classical_delay_test(
        self,
        network_cfg: NetworkConfig,
        sender_names: List[str],
        receiver_names: List[str],
        distances: List[float],
    ):
        network = self.builder.build(network_cfg)

        messages = ["test"]
        message_times = [0]
        protocols = {}
        expected_delays = {}
        event_register = ClassicalMessageEventRegistration()
        assert len(sender_names) == len(receiver_names)

        for i, (sender_name, receiver_name) in enumerate(zip(sender_names, receiver_names)):
            sender = ClassicalSenderProtocol(receiver_name, event_register, messages, message_times)
            receiver = ClassicalReceiverProtocol(sender_name, event_register)
            expected_delay = distances[i] + distances[i + len(sender_names)]

            protocols[sender_name] = sender
            protocols[receiver_name] = receiver

            expected_delays[sender_name] = expected_delay
            expected_delays[receiver_name] = expected_delay

        run(network, protocols)

        for classical_message_info in event_register.received:
            self.assertEqual(classical_message_info.msg, messages[0])
            self.assertAlmostEqual(
                classical_message_info.time,
                expected_delays[classical_message_info.peer],
                delta=1e-9,
            )

    def test_classical_delay(self):
        num_nodes = 6
        distances = [12, 53, 38, 81, 1, 8]
        sender_names = [f"sender_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
        )

        self._perform_classical_delay_test(network_cfg, sender_names, receiver_names, distances)

    def test_classical_delay_yaml(self):
        num_nodes = 6
        distances = [10.4, 23, 1, 4, 213.3e8, 15]
        sender_names = [f"sender_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        test_dir = pathlib.Path(__file__).parent.resolve()
        network_cfg = NetworkConfig.from_file(f"{test_dir}/yaml_configs/metro_hub.yaml")

        self._perform_classical_delay_test(network_cfg, sender_names, receiver_names, distances)

    def test_egp_perfect(self):
        num_nodes = 6
        distances = [5, 9, 32, 23, 213.3e8, 15]
        sender_names = [f"creator_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=0, max_multiplexing=3),
        )

        distance_dict = {
            node_name: dist for node_name, dist in zip(sender_names + receiver_names, distances)
        }

        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names
        )
        self._check_delays(event_register, distance_dict)
        self._check_fidelity(event_register)

    def test_egp_create_id(self):
        num_nodes = 6
        distances = [5, 9, 32, 23, 213.3e8, 15]
        sender_names = [f"creator_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=0, max_multiplexing=3),
        )

        event_register = self._perform_create_keep_test_run(
            network_cfg, [sender_names[0]], [receiver_names[0]], n_epr=10
        )
        self._check_create_id(event_register)

    def test_measure_directly(self):
        num_nodes = 6
        distances = [5, 9, 32, 23, 213.3e8, 15]
        sender_names = [f"creator_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=0, max_multiplexing=3),
        )

        evt_register = self._perform_measure_directly_test_run(
            network_cfg, sender_names, receiver_names, n_epr=10
        )
        self._check_measure_directly(evt_register)

    def test_egp_depolarizing(self):
        num_nodes = 6
        distances = [10.4, 23, 1, 4, 213.3e8, 15]
        sender_names = [f"creator_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="depolarise",
            qlink_cfg=DepolariseQLinkConfig(fidelity=1.0, prob_success=1, speed_of_light=1e9),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=0, max_multiplexing=3),
        )

        distance_dict = {
            node_name: dist for node_name, dist in zip(sender_names + receiver_names, distances)
        }

        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names
        )
        self._check_delays(event_register, distance_dict)
        self._check_fidelity(event_register)

    def test_egp_depolarizing_yaml(self):
        num_nodes = 6
        distances = [10.4, 23, 1, 4, 213.3e8, 15]
        sender_names = [f"sender_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        test_dir = pathlib.Path(__file__).parent.resolve()
        network_cfg = NetworkConfig.from_file(f"{test_dir}/yaml_configs/metro_hub_depolarise.yaml")

        distance_dict = {
            node_name: dist for node_name, dist in zip(sender_names + receiver_names, distances)
        }

        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names
        )
        self._check_delays(event_register, distance_dict)
        self._check_fidelity(event_register)

    def test_egp_heralded_single_click(self):
        num_nodes = 6
        distances = [100, 23, 1e6, 1e3, 213.3e5, 15e6]
        sender_names = [f"creator_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=1e9,
            emission_fidelity=1,
        )

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="heralded-single-click",
            qlink_cfg=qlink_cfg,
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=0, max_multiplexing=3),
        )

        distance_dict = {
            node_name: dist for node_name, dist in zip(sender_names + receiver_names, distances)
        }

        # Check delays, minimum_fidelity=0 causes instant success, but terrible fidelity
        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names, minimum_fidelity=0
        )
        self._check_delays_with_mid_point(event_register, distance_dict)

        # Check fidelity, minimum_fidelity=0.99 causes good fidelity, but non-instant success
        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names, minimum_fidelity=0.99
        )
        self._check_fidelity(event_register)

    def test_egp_heralded_single_click_yaml(self):
        num_nodes = 6
        distances = [10.4, 23, 1, 4, 213.3e4, 15]
        sender_names = [f"sender_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        test_dir = pathlib.Path(__file__).parent.resolve()
        network_cfg = NetworkConfig.from_file(
            f"{test_dir}/yaml_configs/metro_hub_single-click-heralded.yaml"
        )

        distance_dict = {
            node_name: dist for node_name, dist in zip(sender_names + receiver_names, distances)
        }

        # Check delays, minimum_fidelity=0 causes instant success, but terrible fidelity
        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names, minimum_fidelity=0
        )
        self._check_delays_with_mid_point(event_register, distance_dict)

        # Check fidelity, minimum_fidelity=0.99 causes good fidelity, but non-instant success
        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names, minimum_fidelity=0.99
        )
        self._check_fidelity(event_register)

    def test_egp_heralded_double_click(self):
        num_nodes = 6
        distances = [100, 23, 1e6, 1e3, 213.3e5, 15e6]
        sender_names = [f"creator_{i}" for i in range(num_nodes // 2)]
        receiver_names = [f"receiver_{i}" for i in range(num_nodes // 2)]

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=1e9,
            emission_fidelity=1,
        )

        network_cfg = create_metro_hub_network(
            node_names=sender_names + receiver_names,
            node_distances=distances,
            qlink_typ="heralded-double-click",
            qlink_cfg=qlink_cfg,
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=1e9),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=0, max_multiplexing=3),
        )

        distance_dict = {
            node_name: dist for node_name, dist in zip(sender_names + receiver_names, distances)
        }

        event_register = self._perform_create_keep_test_run(
            network_cfg, sender_names, receiver_names
        )
        self._check_delays_with_mid_point(event_register, distance_dict, modulo_epr_round_time=True)
        self._check_fidelity(event_register)

    def _perform_create_keep_test_run(
        self,
        network_cfg: NetworkConfig,
        sender_names: List[str],
        receiver_names: List[str],
        minimum_fidelity: float = 0,
        n_epr: int = 1,
    ) -> CreateAndKeepEventRegistration:
        network = self.builder.build(network_cfg)

        protocols = {}
        event_register = CreateAndKeepEventRegistration()
        assert len(sender_names) == len(receiver_names)

        for sender_name, receiver_name in zip(sender_names, receiver_names):
            sender = CreateAndKeepSenderProtocol(
                receiver_name, event_register, minimum_fidelity=minimum_fidelity, n_epr=n_epr
            )
            receiver = CreateAndKeepReceiveProtocol(sender_name, event_register, n_epr=n_epr)

            protocols[sender_name] = sender
            protocols[receiver_name] = receiver

        run(network, protocols)

        return event_register

    def _perform_measure_directly_test_run(
        self,
        network_cfg: NetworkConfig,
        sender_names: List[str],
        receiver_names: List[str],
        minimum_fidelity: float = 0,
        n_epr: int = 1,
    ) -> MeasureDirectlyEventRegistration:
        network = self.builder.build(network_cfg)

        protocols = {}
        event_register = MeasureDirectlyEventRegistration()
        assert len(sender_names) == len(receiver_names)

        for sender_name, receiver_name in zip(sender_names, receiver_names):
            sender = MeasureDirectlySenderProtocol(
                receiver_name, event_register, minimum_fidelity=minimum_fidelity, n_epr=n_epr
            )
            receiver = MeasureDirectlyReceiveProtocol(sender_name, event_register, n_epr=n_epr)

            protocols[sender_name] = sender
            protocols[receiver_name] = receiver

        run(network, protocols)

        return event_register

    def _check_delays(
        self, event_register: CreateAndKeepEventRegistration, distances: Dict[str, float]
    ):
        for received_egp in event_register.received_ck:
            dist1 = distances[received_egp.node_name]
            dist2 = distances[received_egp.peer_name]

            single_com_time = dist1 + dist2
            classical_handshake = single_com_time
            epr_round_time = single_com_time
            expected_delay = classical_handshake + epr_round_time

            self.assertAlmostEqual(expected_delay, received_egp.time, delta=1e-9)

    def _check_delays_with_mid_point(
        self,
        event_register: CreateAndKeepEventRegistration,
        distances: Dict[str, float],
        modulo_epr_round_time=False,
    ):
        for received_egp in event_register.received_ck:
            dist1 = distances[received_egp.node_name]
            dist2 = distances[received_egp.peer_name]

            single_com_time = dist1 + dist2
            classical_handshake = single_com_time
            epr_round_time = 2 * max(dist1, dist2)
            expected_delay = classical_handshake + epr_round_time

            if not modulo_epr_round_time:
                self.assertAlmostEqual(expected_delay, received_egp.time, delta=1e-9)
            else:
                self.assertAlmostEqual(
                    0,
                    (received_egp.time - classical_handshake) % epr_round_time,
                    delta=1e-9,
                )

    def _check_fidelity(self, event_register: CreateAndKeepEventRegistration):
        received_egp_with_full_dm = [
            received_egp
            for received_egp in event_register.received_ck
            if received_egp.dm.shape[0] > 2
        ]
        # The protocol will discard qubits after registering the results, thereby destroying half of the state.
        # The second party to look at the qubit state, will thus see a DM with only one qubit.
        self.assertEqual(len(received_egp_with_full_dm), len(event_register.received_ck) / 2)

        for received_egp in received_egp_with_full_dm:
            fid = calculate_fidelity_epr(received_egp.dm, received_egp.result.bell_state)
            self.assertGreater(fid, 0.99)

    def _check_create_id(self, event_register: CreateAndKeepEventRegistration):
        results = event_register.received_ck

        for i, submission in enumerate(event_register.submitted_ck):
            assert isinstance(submission.create_id, int)
            self.assertEqual(submission.create_id, i)
            self.assertEqual(results[2 * i].result.create_id, submission.create_id)

    def _check_measure_directly(self, event_register: MeasureDirectlyEventRegistration):
        self.assertEqual(len(event_register.submitted_md) * 2, len(event_register.received_md))

        for event in event_register.received_md:
            mirror_events = [
                temp_evt
                for temp_evt in event_register.received_md
                if temp_evt.peer_name == event.node_name
                and temp_evt.node_name == event.peer_name
                and temp_evt.result.create_id == event.result.create_id
            ]
            self.assertEqual(len(mirror_events), 1)
            mirror_event = mirror_events[0]

            if event.result.bell_state in [ns.BellIndex.PHI_PLUS, ns.BellIndex.PHI_MINUS]:
                self.assertEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )
            else:
                assert event.result.bell_state in [ns.BellIndex.PSI_PLUS, ns.BellIndex.PSI_MINUS]
                self.assertNotEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )


if __name__ == "__main__":
    unittest.main()
