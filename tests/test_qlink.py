import math
import pathlib
import random
import unittest
import warnings

import netsquid as ns
import numpy as np
from qlink_interface import ResCreateAndKeep

from netsquid_netbuilder.modules.qdevices import GenericQDeviceConfig
from netsquid_netbuilder.modules.qlinks.depolarise import DepolariseQLinkConfig
from netsquid_netbuilder.modules.qlinks.heralded_double_click import HeraldedDoubleClickQLinkConfig
from netsquid_netbuilder.modules.qlinks.heralded_single_click import HeraldedSingleClickQLinkConfig
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.fidelity import calculate_fidelity_epr
from netsquid_netbuilder.util.network_generation import create_2_node_network
from netsquid_netbuilder.util.test_protocol_qlink import (
    CreateAndKeepEventRegistration,
    CreateAndKeepReceiveProtocol,
    CreateAndKeepSenderProtocol,
)


def _emission_fidelity_calculator(emission_fid_1: float, emission_fid_2: float) -> float:
    """Method to calculate expected fidelity of a double-click model if only sources of noise are emission fidelity."""
    fid = 1 / 4 + 1 / 12 * (4 * emission_fid_1 - 1) * (4 * emission_fid_2 - 1)
    return fid


class TestQlinkBase(unittest.TestCase):
    def setUp(self):
        self.builder = get_default_builder()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        ns.set_random_state(seed=42)
        np.random.seed(seed=42)
        random.seed(42)

    def tearDown(self):
        pass

    def check_epr_pairs(
        self,
        results_reg: CreateAndKeepEventRegistration,
        n_epr: int,
        cycle_time: float,
        prob_success: float,
        tolerated_delta_avg_num_attempts: float,
        expected_fidelity: float,
        tolerated_delta_fidelity: float,
    ):

        self.assertEqual(len(results_reg.received_classical), 1)
        self.assertEqual(len(results_reg.received_ck), n_epr * 2)

        res_clas = results_reg.received_classical[0]

        time = res_clas.time
        num_attempts_list = []

        for i in range(n_epr):
            res_egp_1 = results_reg.received_ck[i * 2]
            res_egp_2 = results_reg.received_ck[i * 2 + 1]

            self.assertEqual(res_egp_1.time, res_egp_2.time)

            # Calculate how long and how many attempts the entanglement generation took.
            attempt_time = res_egp_1.time - time
            num_attempts = attempt_time / cycle_time

            time = res_egp_1.time

            # Check that num_attempts is (almost) an integer. This is de-facto check that attempt_time = N * cycle_time
            self.assertTrue(math.isclose(num_attempts, round(num_attempts), abs_tol=0.001))

            num_attempts_list.append(num_attempts)

            # Check fidelity
            res_create_keep: ResCreateAndKeep = res_egp_1.result
            fid = calculate_fidelity_epr(res_egp_1.dm, res_create_keep.bell_state)
            self.assertAlmostEqual(fid, expected_fidelity, delta=tolerated_delta_fidelity)

        avg_num_attempts = np.average(num_attempts_list)

        self.assertAlmostEqual(
            avg_num_attempts, 1 / prob_success, delta=tolerated_delta_avg_num_attempts
        )


class TestPerfectLinkModel(TestQlinkBase):
    def test_1(self):
        delay = 5325.73

        qlink_cfg = PerfectQLinkConfig(state_delay=delay)
        network_cfg = create_2_node_network(qlink_typ="perfect", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=delay,
            prob_success=1,
            tolerated_delta_avg_num_attempts=1e-9,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )


class TestDepolariseLinkModel(TestQlinkBase):
    def test_perfect(self):
        t_cycle = 3246.34

        qlink_cfg = DepolariseQLinkConfig(fidelity=1.0, prob_success=1, t_cycle=t_cycle)
        network_cfg = create_2_node_network(qlink_typ="depolarise", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=1,
            tolerated_delta_avg_num_attempts=1e-9,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )

    def test_random_bell_state(self):
        t_cycle = 245.34

        qlink_cfg = DepolariseQLinkConfig(
            fidelity=1.0, prob_success=1, t_cycle=t_cycle, random_bell_state=True
        )
        network_cfg = create_2_node_network(qlink_typ="depolarise", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=1,
            tolerated_delta_avg_num_attempts=1e-9,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )

    def test_fidelity(self):
        t_cycle = 3246.34
        for fidelity in np.arange(0.25, 1, 0.1):

            qlink_cfg = DepolariseQLinkConfig(fidelity=fidelity, prob_success=1, t_cycle=t_cycle)
            network_cfg = create_2_node_network(qlink_typ="depolarise", qlink_cfg=qlink_cfg)
            results_reg = CreateAndKeepEventRegistration()
            n_epr = 1

            alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
            bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

            network = self.builder.build(network_cfg)
            run(network, {"Alice": alice_program, "Bob": bob_program})

            self.check_epr_pairs(
                results_reg,
                n_epr,
                cycle_time=t_cycle,
                prob_success=1,
                tolerated_delta_avg_num_attempts=1e-8,
                expected_fidelity=fidelity,
                tolerated_delta_fidelity=0.1,
            )

    def test_prob_success(self):
        t_cycle = 3246.34
        prob_success = 0.2

        qlink_cfg = DepolariseQLinkConfig(fidelity=1.0, prob_success=prob_success, t_cycle=t_cycle)
        network_cfg = create_2_node_network(qlink_typ="depolarise", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )


class TestDoubleClickLinkModel(TestQlinkBase):
    def test_perfect(self):
        length = 10
        speed_of_light = 20000
        t_cycle = length / speed_of_light * 1e9

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
            emission_fidelity=1,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 / 2
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )

    def test_loss(self):
        length = 10
        speed_of_light = 20000
        p_loss = 0.9
        t_cycle = length / speed_of_light * 1e9

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=length,
            p_loss_init=p_loss,
            p_loss_length=0,
            speed_of_light=speed_of_light,
            emission_fidelity=1,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 / 2 * (1 - p_loss) * (1 - p_loss)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )

    def test_emission_fidelity_link(self):
        length = 10
        speed_of_light = 20000
        emission_fidelity = 0.95
        t_cycle = length / speed_of_light * 1e9

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
            emission_fidelity=emission_fidelity,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 / 2
        expected_fidelity = _emission_fidelity_calculator(emission_fidelity, emission_fidelity)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=expected_fidelity,
            tolerated_delta_fidelity=0.02,
        )

    def test_emission_fidelity_nodes(self):
        length = 10
        speed_of_light = 20000
        emission_fidelity_node0 = 0.9
        emission_fidelity_node1 = 0.8
        t_cycle = length / speed_of_light * 1e9

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg)

        network_cfg.processing_nodes[0].qdevice_cfg.external_params = {
            "emission_fidelity": emission_fidelity_node0
        }
        network_cfg.processing_nodes[1].qdevice_cfg.external_params = {
            "emission_fidelity": emission_fidelity_node1
        }

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 / 2
        expected_fidelity = _emission_fidelity_calculator(
            emission_fidelity_node0, emission_fidelity_node1
        )
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=expected_fidelity,
            tolerated_delta_fidelity=0.02,
        )

    def test_unequal_sides(self):
        length_a = 10
        length_b = 30
        speed_of_light = 20000
        t_cycle = 2 * max(length_a, length_b) / speed_of_light * 1e9

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length_A=length_a,
            length_B=length_b,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
            emission_fidelity=1,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 / 2
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=1,
            tolerated_delta_fidelity=1e-9,
        )

    def test_qdevice_params_yaml_load(self):
        length = 2
        speed_of_light = 200000
        emission_duration_a = 100
        emission_duration_b = 200
        collection_efficiency_a = 0.5
        collection_efficiency_b = 0.5

        emission_fidelity_node0 = 0.9
        emission_fidelity_node1 = 0.7
        t_cycle = length / speed_of_light * 1e9 + max(emission_duration_a, emission_duration_b)

        test_dir = pathlib.Path(__file__).parent.resolve()
        network_cfg = NetworkConfig.from_file(
            f"{test_dir}/yaml_configs/qlink_qdevice_params_double-click.yaml"
        )

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 / 2 * collection_efficiency_a * collection_efficiency_b
        expected_fidelity = _emission_fidelity_calculator(
            emission_fidelity_node0, emission_fidelity_node1
        )
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=expected_fidelity,
            tolerated_delta_fidelity=0.02,
        )


class TestSingleClickLinkModel(TestQlinkBase):
    def test_perfect_rate(self):
        length = 10
        speed_of_light = 20000
        t_cycle = length / speed_of_light * 1e9
        minimum_fidelity = 0
        alpha = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length=length, p_loss_init=0, p_loss_length=0, speed_of_light=speed_of_light
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol(
            "Bob", results_reg, n_epr, minimum_fidelity=minimum_fidelity
        )
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 - (1 - alpha) * (1 - alpha)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=1e-9,
        )

    def test_perfect_fidelity(self):
        length = 10
        speed_of_light = 20000
        t_cycle = length / speed_of_light * 1e9
        minimum_fidelity = 0.95
        alpha = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length=length, p_loss_init=0, p_loss_length=0, speed_of_light=speed_of_light
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol(
            "Bob", results_reg, n_epr, minimum_fidelity=minimum_fidelity
        )
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 - (1 - alpha) * (1 - alpha)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=0.03,
        )

    def test_perfect_fidelity_set_alpha_link(self):
        length = 10
        speed_of_light = 20000
        t_cycle = length / speed_of_light * 1e9
        minimum_fidelity = 0.95
        alpha_A = 1 - minimum_fidelity
        alpha_B = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
            bright_state_parameter_A=alpha_A,
            bright_state_parameter_B=alpha_B,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)

        with warnings.catch_warnings(record=True) as raised_warnings:
            run(network, {"Alice": alice_program, "Bob": bob_program})

        self.assertEqual(len(raised_warnings), 2)
        for warning in raised_warnings:
            assert "Overwriting the value of alpha" in warning.message.args[0]

        prob_success = 1 - (1 - alpha_A) * (1 - alpha_B)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=0.03,
        )

    def test_perfect_fidelity_set_alpha_nodes(self):
        length = 8
        speed_of_light = 20000
        t_cycle = length / speed_of_light * 1e9
        minimum_fidelity = 0.954
        alpha = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length=length, p_loss_init=0, p_loss_length=0, speed_of_light=speed_of_light
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        for node in network_cfg.processing_nodes:
            assert isinstance(node.qdevice_cfg, GenericQDeviceConfig)
            node.qdevice_cfg.external_params = {"bright_state_parameter": alpha}

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)

        with warnings.catch_warnings(record=True) as raised_warnings:
            run(network, {"Alice": alice_program, "Bob": bob_program})

        self.assertEqual(len(raised_warnings), 2)
        for warning in raised_warnings:
            assert "Overwriting the value of alpha" in warning.message.args[0]

        prob_success = 1 - (1 - alpha) * (1 - alpha)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=0.03,
        )

    def test_loss(self):
        length = 10
        speed_of_light = 20000
        p_loss = 0.9
        t_cycle = length / speed_of_light * 1e9
        minimum_fidelity = 0.95
        alpha = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length=length, p_loss_init=p_loss, p_loss_length=0, speed_of_light=speed_of_light
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol(
            "Bob", results_reg, n_epr, minimum_fidelity=minimum_fidelity
        )
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_single_photon_arrive = alpha * (1 - p_loss)
        prob_success = 1 - (1 - prob_single_photon_arrive) * (1 - prob_single_photon_arrive)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=0.03,
        )

    def test_visibility(self):
        length = 10
        speed_of_light = 20000
        visibility = 0.5
        t_cycle = length / speed_of_light * 1e9
        minimum_fidelity = 0.95
        alpha = 1 - minimum_fidelity

        # The effects of visibility are non-trivial, so the expected fidelity is based on simulation run of
        # minimum_fidelity = 0.95, visibility = 0.5
        expected_fidelity = 0.8370330023882424

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
            visibility=visibility,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol(
            "Bob", results_reg, n_epr, minimum_fidelity=minimum_fidelity
        )
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 - (1 - alpha) * (1 - alpha)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=expected_fidelity,
            tolerated_delta_fidelity=0.02,
        )

    def test_unequal_sides_low_minimum_fidelity(self):
        length_a = 10
        length_b = 30
        speed_of_light = 20000
        t_cycle = 2 * max(length_a, length_b) / speed_of_light * 1e9
        minimum_fidelity = 0.0
        alpha = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length_A=length_a,
            length_B=length_b,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 - (1 - alpha) * (1 - alpha)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=1e-9,
        )

    def test_unequal_sides_high_minimum_fidelity(self):
        length_a = 10
        length_b = 30
        speed_of_light = 20000
        t_cycle = 2 * max(length_a, length_b) / speed_of_light * 1e9
        minimum_fidelity = 0.95
        alpha = 1 - minimum_fidelity

        qlink_cfg = HeraldedSingleClickQLinkConfig(
            length_A=length_a,
            length_B=length_b,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
        )

        network_cfg = create_2_node_network(qlink_typ="heralded-single-click", qlink_cfg=qlink_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol(
            "Bob", results_reg, n_epr, minimum_fidelity=minimum_fidelity
        )
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = 1 - (1 - alpha) * (1 - alpha)
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=3 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=0.03,
        )

    def test_qdevice_params_yaml_load(self):
        length = 2
        speed_of_light = 200000
        minimum_fidelity = 0
        emission_duration_a = 100
        emission_duration_b = 200
        collection_efficiency_a = 0.5
        collection_efficiency_b = 0.5
        alpha = 1 - minimum_fidelity

        t_cycle = length / speed_of_light * 1e9 + max(emission_duration_a, emission_duration_b)

        test_dir = pathlib.Path(__file__).parent.resolve()
        network_cfg = NetworkConfig.from_file(
            f"{test_dir}/yaml_configs/qlink_qdevice_params_single-click.yaml"
        )

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        alice_program = CreateAndKeepSenderProtocol(
            "Bob", results_reg, n_epr, minimum_fidelity=minimum_fidelity
        )
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        prob_success = (
            1 - (1 - alpha) * collection_efficiency_a * (1 - alpha) * collection_efficiency_b
        )
        self.check_epr_pairs(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
            expected_fidelity=minimum_fidelity,
            tolerated_delta_fidelity=1e-9,
        )


if __name__ == "__main__":
    unittest.main()
