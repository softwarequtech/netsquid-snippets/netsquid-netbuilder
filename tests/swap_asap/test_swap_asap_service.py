import netsquid as ns
from netsquid import BellIndex
from netsquid.nodes import Node
from netsquid.protocols.nodeprotocols import NodeProtocol

from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_service import (
    ReqSwapASAP,
    ReqSwapASAPAbort,
    ResSwapASAPError,
    ResSwapASAPFinished,
    SwapASAPService,
)


class SimpleSwapASAP(SwapASAPService):
    """Implementation of :class:`swap_asap.SwapASAPService` meant to test request and response handling."""

    def __init__(self, node, name="simple_swap_asap"):
        super().__init__(node=node, name=name)
        self.performed_swap_asap = False
        self.performed_abort = False
        self.performed_update = False
        self.request_id = 0

    def swap_asap(self, req):
        super().swap_asap(req)
        self.performed_swap_asap = True
        self.request_id = req.request_id

    def abort(self, req):
        super().abort(req)
        self.performed_abort = True

    def update(self, req):
        super().update()
        self.performed_update = True


class SwapAsapTestProtocol(NodeProtocol):
    """Protocol to test responses from swap asap service.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node` or None, optional
        Node this protocol runs on. If None, a node should be set later before starting
        this protocol.
    swap_asap : :class:`swap_asap.SwapASAPService`.
        Service to be tested for responses.
    name : str or None, optional
        Name of protocol. If None, the name of the class is used.

    """

    def __init__(self, node, swap_asap, name="swap_asap_test_protocol"):
        super().__init__(node=node, name=name)
        self.swap_asap = swap_asap
        self.finished = False

    def run(self):

        signal_res_finish = ResSwapASAPFinished.__name__
        signal_res_error = ResSwapASAPError.__name__

        # First, the test function makes the protocol send a "finish" signal
        yield self.await_signal(sender=self.swap_asap, signal_label=signal_res_finish)
        res_finish = self.swap_asap.get_signal_result(signal_res_finish, self)
        assert isinstance(res_finish, ResSwapASAPFinished)

        # Next, the test function makes the protocol send a "finish" signal with unequal list lengths,
        # which should result in an "error" signal.
        yield self.await_signal(sender=self.swap_asap, signal_label=signal_res_error)
        res_error = self.swap_asap.get_signal_result(signal_res_error, self)
        assert isinstance(res_error, ResSwapASAPError)
        assert res_error.error_code == 1

        # Lastly, the test function makes the protocol send an "error" signal.
        yield self.await_signal(sender=self.swap_asap, signal_label=signal_res_error)
        res_error = self.swap_asap.get_signal_result(signal_res_error, self)
        assert isinstance(res_error, ResSwapASAPError)
        assert res_error.error_code == 0

        self.finished = True


def test_swap_asap_service():
    """Test if requests and responses are handled correctly by :class:`swap_asap.SwapASAPService`."""
    node = Node("test_simple_swap_asap_node")
    swap_asap = SimpleSwapASAP(node=node, name="test_simple_swap_asap_protocol")
    test_protocol = SwapAsapTestProtocol(node=node, swap_asap=swap_asap)
    ns.sim_reset()
    test_protocol.start()
    request_id = 14

    # First, put a "swap ASAP" request
    req_swap_asap = ReqSwapASAP(
        upstream_node_name="node1",
        downstream_node_name="node2",
        request_id=request_id,
        num=3,
        cutoff_time=0,
    )
    swap_asap.put(req_swap_asap)
    assert swap_asap.performed_swap_asap
    assert swap_asap.request_id == request_id
    assert not swap_asap.performed_abort

    # Second, put an "abort" request
    req_abort = ReqSwapASAPAbort(request_id=request_id)
    swap_asap.put(req_abort)
    assert swap_asap.performed_abort

    # Now, make it send a proper "finish" signal
    res_finished_good = ResSwapASAPFinished(
        request_id=request_id,
        swap_bell_indices=[BellIndex(0), BellIndex(3)],
        upstream_bell_indices=[BellIndex(2), BellIndex(1)],
        downstream_bell_indices=[BellIndex(0), BellIndex(2)],
        goodness=[0, 0],
    )
    swap_asap.send_response(res_finished_good)
    ns.sim_run()

    # Make it send an invalid "finish" signal
    res_finished_bad = ResSwapASAPFinished(
        request_id=request_id,
        swap_bell_indices=[BellIndex(0), BellIndex(3)],
        upstream_bell_indices=[BellIndex(2)],
        downstream_bell_indices=[BellIndex(0), BellIndex(2)],
        goodness=[0, 0],
    )
    swap_asap.send_response(res_finished_bad)
    ns.sim_run()

    # Lastly, make it send an "error" signal
    res_error = ResSwapASAPError()
    swap_asap.send_response(res_error)
    assert not test_protocol.finished
    ns.sim_run()

    assert test_protocol.finished
