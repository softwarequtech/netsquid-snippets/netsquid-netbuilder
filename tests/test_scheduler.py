import random
import unittest
from abc import ABCMeta
from typing import List

import netsquid as ns
import numpy as np

from netsquid_netbuilder.modules.clinks import InstantCLinkConfig
from netsquid_netbuilder.modules.clinks.default import DefaultCLinkConfig
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkConfig
from netsquid_netbuilder.modules.scheduler.fifo import FIFOScheduleConfig
from netsquid_netbuilder.modules.scheduler.interface import IScheduleConfig
from netsquid_netbuilder.modules.scheduler.static import StaticScheduleConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.network_generation import (
    create_metro_hub_network,
    create_two_connected_metro_hubs_network,
)
from netsquid_netbuilder.util.test_protocol_scheduler import (
    SchedulerEventRegistration,
    SchedulerRequest,
    SchedulerTestProtocol,
)


class BaseSchedulerTestFramework(unittest.TestCase, metaclass=ABCMeta):
    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_random_state(seed=42)
        np.random.seed(seed=42)
        random.seed(42)
        self.builder = get_default_builder()
        self.event_register = SchedulerEventRegistration()
        self.requests: List[SchedulerRequest] = []

    def tearDown(self) -> None:
        pass

    @staticmethod
    def generate_requests(
        node_names: List[str], num_requests: int, delta_func: callable, qubits_per_request: int = 1
    ) -> List[SchedulerRequest]:
        requests = []
        submit_time = 0
        num_nodes = len(node_names)
        for _ in range(num_requests):
            r2 = r1 = random.randint(0, num_nodes - 1)
            while r1 == r2:
                r2 = random.randint(0, num_nodes - 1)

            request = SchedulerRequest(
                submit_time=submit_time,
                sender_name=node_names[r1],
                receiver_name=node_names[r2],
                number=qubits_per_request,
            )
            submit_time += delta_func()
            requests.append(request)
        return requests

    @staticmethod
    def filter_requests_for_node(
        node_name: str, requests: List[SchedulerRequest]
    ) -> List[SchedulerRequest]:
        return [req for req in requests if req.sender_name == node_name]

    @staticmethod
    def generate_requests_two_parties(
        node_names_1: List[str], node_names_2: List[str], num_requests: int, delta_func: callable
    ) -> List[SchedulerRequest]:
        requests = []
        submit_time = 0
        for _ in range(num_requests):
            node1 = random.choice(node_names_1)
            node2 = random.choice(node_names_2)

            request = SchedulerRequest(
                submit_time=submit_time,
                sender_name=node1,
                receiver_name=node2,
            )
            submit_time += delta_func()
            requests.append(request)
        return requests

    def check_all_requests_finished(self):
        results = self.event_register.completions
        num_qubits = sum([req.number for req in self.requests])
        self.assertEqual(num_qubits * 2, len(results))

    def check_all_requests_finished_after_exact_delay(self, delay):
        results = self.event_register.completions
        for i, request in enumerate(self.requests):
            result = results[2 * i]
            self.assertAlmostEqual(
                request.submit_time + delay,
                result.completion_time,
                delta=delay * 1e-9,
            )

    def check_result_measurements_identical(self):
        results = self.event_register.completions
        for i, request in enumerate(self.requests):
            result = results[2 * i]
            self.assertEqual(result.epr_measure_result, results[2 * i + 1].epr_measure_result)

    def check_all_requests_finished_sequentially(self, period, delay):
        results = self.event_register.completions
        for i in range(int(len(results) / 2)):
            result = results[2 * i]
            self.assertAlmostEqual(
                period * i + delay,
                result.completion_time,
                delta=delay * 1e-9,
            )

    def check_create_id(self, submit_node: str, receive_node: str):

        submissions = [
            submission
            for submission in self.event_register.submissions
            if submission.node_1 == submit_node and submission.node_2 == receive_node
        ]
        completions = [
            completion
            for completion in self.event_register.completions
            if completion.node_1 == submit_node
            and completion.node_2 == receive_node
            and completion.response.directionality_flag == 0
        ]

        self.assertGreater(len(submissions), 3)
        self.assertEqual(len(submissions), len(completions))

        used_create_ids = []

        for i in range(len(submissions)):
            submission = submissions[i]
            completion = completions[i]
            assert isinstance(submission.create_id, int)
            self.assertNotIn(submission.create_id, used_create_ids)
            used_create_ids.append(submission.create_id)
            self.assertEqual(completion.response.create_id, submission.create_id)

    def run_simulation(self, network_cfg: NetworkConfig, node_names: List[str]):
        network = self.builder.build(network_cfg)

        assert len(self.requests)

        protocols = {
            node_name: SchedulerTestProtocol(
                self.event_register, self.filter_requests_for_node(node_name, self.requests)
            )
            for node_name in node_names
        }

        run(network, protocols)


class TestFIFOScheduler(BaseSchedulerTestFramework):
    @staticmethod
    def perfect_config() -> IScheduleConfig:
        return FIFOScheduleConfig(switch_time=0)

    def test_1_metro_hub(self):
        """Test that with multiple nodes with overlapping requests all requests will eventually be delivered"""
        num_requests = 100
        num_nodes = 5
        distance = 100
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_2_cross_chain(self):
        """Test if all requests are completed when sending requests from one hub to another hub"""
        num_requests = 100
        num_nodes_per_hub = 4
        hub_distance = 100
        repeater_distance = 50
        num_repeaters = 2
        speed_of_light = 1e9
        delay = (hub_distance + repeater_distance) / speed_of_light * 1e9
        hub1_nodes = [f"node_hub1_{i}" for i in range(num_nodes_per_hub)]
        hub2_nodes = [f"node_hub2_{i}" for i in range(num_nodes_per_hub)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_nodes,
            hub1_node_distances=[hub_distance] * num_nodes_per_hub,
            hub2_node_names=hub2_nodes,
            hub2_node_distances=[hub_distance] * num_nodes_per_hub,
            repeater_chain_distances=[repeater_distance] * num_repeaters,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests_two_parties(
            hub1_nodes, hub2_nodes, num_requests, delta_func=delta_func
        )

        self.run_simulation(network_cfg, node_names=hub1_nodes + hub2_nodes)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    @unittest.expectedFailure
    def test_3_qia_random(self):
        """Test if all requests are completed in a QIA like network"""
        num_requests = 100
        num_nodes_per_hub = 4
        hub_distance = 434
        repeater_distance = 100
        num_repeaters = 2
        speed_of_light = 1e9
        delay = (hub_distance + repeater_distance) / speed_of_light * 1e9
        hub1_nodes = [f"node_hub1_{i}" for i in range(num_nodes_per_hub)]
        hub2_nodes = [f"node_hub2_{i}" for i in range(num_nodes_per_hub)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_nodes,
            hub1_node_distances=[hub_distance] * num_nodes_per_hub,
            hub2_node_names=hub2_nodes,
            hub2_node_distances=[hub_distance] * num_nodes_per_hub,
            repeater_chain_distances=[repeater_distance] * num_repeaters,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(
            hub1_nodes + hub2_nodes, num_requests, delta_func=delta_func
        )

        self.run_simulation(network_cfg, node_names=hub1_nodes + hub2_nodes)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    @unittest.skip
    def test_4_qia_random_instant_classical(self):
        """Test if all requests are completed in a QIA like network, with instant classical communication"""
        num_requests = 100
        num_nodes_per_hub = 4
        hub_distance = 42
        repeater_distance = 50
        num_repeaters = 3
        speed_of_light = 1e9
        delay = (hub_distance + repeater_distance) / speed_of_light * 1e9
        hub1_nodes = [f"node_hub1_{i}" for i in range(num_nodes_per_hub)]
        hub2_nodes = [f"node_hub2_{i}" for i in range(num_nodes_per_hub)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_nodes,
            hub1_node_distances=[hub_distance] * num_nodes_per_hub,
            hub2_node_names=hub2_nodes,
            hub2_node_distances=[hub_distance] * num_nodes_per_hub,
            repeater_chain_distances=[repeater_distance] * num_repeaters,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=1e9),
            clink_typ="instant",
            clink_cfg=InstantCLinkConfig(),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(
            hub1_nodes + hub2_nodes, num_requests, delta_func=delta_func
        )

        self.run_simulation(network_cfg, node_names=hub1_nodes + hub2_nodes)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_5_multiple_qubits_request(self):
        """Test that with multiple nodes with overlapping requests all requests will eventually be delivered"""
        num_requests = 20
        qubits_per_request = 5
        num_nodes = 5
        distance = 100
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(
            node_names, num_requests, delta_func=delta_func, qubits_per_request=qubits_per_request
        )

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_5_no_overlap(self):
        """Test if all requests are completed in the expected time frame"""
        num_requests = 100
        num_nodes = 2
        distance = 100
        switch_time = 200
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9 + switch_time
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=switch_time),
        )

        def delta_func():
            return delay + 1

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_all_requests_finished_after_exact_delay(delay)
        self.check_result_measurements_identical()

    def test_6_two_nodes_request_overlap(self):
        """Test if all requests are completed in the expected time frame"""
        num_requests = 200
        num_nodes = 2
        distance = 100
        switch_time = 457
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9 + switch_time
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(switch_time=switch_time),
        )

        def delta_func():
            return 0.3 * delay

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_all_requests_finished_sequentially(period=delay, delay=delay)
        self.check_result_measurements_identical()

    def test_7_multiplexing(self):
        """Test if all requests are completed in the expected time frame when we have multiplexing enabled"""
        num_requests = 50
        num_nodes = 4
        max_multiplexing = 2
        distance = 100
        switch_time = 40
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9 + switch_time
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=FIFOScheduleConfig(
                switch_time=switch_time, max_multiplexing=max_multiplexing
            ),
        )

        def delta_func():
            return 1.2 * delay

        requests_pair1 = self.generate_requests(
            node_names[0:2], num_requests, delta_func=delta_func
        )
        requests_pair2 = self.generate_requests(
            node_names[2:4], num_requests, delta_func=delta_func
        )
        self.requests = requests_pair1 + requests_pair2
        self.requests.sort(key=lambda x: x.submit_time)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_all_requests_finished_after_exact_delay(delay)
        self.check_result_measurements_identical()

    def test_8_metro_hub_create_id(self):
        """Test that with multiple nodes in a metro hub and multiple requests between nodes,
        the create_id is a unique incrementing number.
        This test is not specific for FIFO"""
        num_requests = 100
        num_nodes = 5
        distance = 100
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_create_id(node_names[0], node_names[1])

    def test_9_cross_chain_create_id(self):
        """Test that with multiple nodes in a repeater chain and multiple requests between nodes,
        the create_id is a unique incrementing number.
        This test is not specific for FIFO"""
        num_requests = 100
        num_nodes_per_hub = 4
        hub_distance = 100
        repeater_distance = 50
        num_repeaters = 2
        speed_of_light = 1e9
        delay = (hub_distance + repeater_distance) / speed_of_light * 1e9
        hub1_nodes = [f"node_hub1_{i}" for i in range(num_nodes_per_hub)]
        hub2_nodes = [f"node_hub2_{i}" for i in range(num_nodes_per_hub)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_nodes,
            hub1_node_distances=[hub_distance] * num_nodes_per_hub,
            hub2_node_names=hub2_nodes,
            hub2_node_distances=[hub_distance] * num_nodes_per_hub,
            repeater_chain_distances=[repeater_distance] * num_repeaters,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="fifo",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests_two_parties(
            hub1_nodes, hub2_nodes, num_requests, delta_func=delta_func
        )

        self.run_simulation(network_cfg, node_names=hub1_nodes + hub2_nodes)

        self.check_create_id(hub1_nodes[0], hub2_nodes[0])


class TestStaticScheduler(BaseSchedulerTestFramework):
    def perfect_config(self) -> IScheduleConfig:
        return StaticScheduleConfig(switch_time=0, time_window=1000)

    def test_1_metro_hub(self):
        """Test that with multiple nodes with overlapping requests all requests will eventually be delivered"""
        num_requests = 100
        num_nodes = 5
        distance = 100
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="static",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_2_cross_chain(self):
        """Test if all requests are completed when sending requests from one hub to another hub"""
        num_requests = 100
        num_nodes_per_hub = 4
        hub_distance = 100
        repeater_distance = 50
        num_repeaters = 2
        speed_of_light = 1e9
        delay = (hub_distance + repeater_distance) / speed_of_light * 1e9
        hub1_nodes = [f"node_hub1_{i}" for i in range(num_nodes_per_hub)]
        hub2_nodes = [f"node_hub2_{i}" for i in range(num_nodes_per_hub)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_nodes,
            hub1_node_distances=[hub_distance] * num_nodes_per_hub,
            hub2_node_names=hub2_nodes,
            hub2_node_distances=[hub_distance] * num_nodes_per_hub,
            repeater_chain_distances=[repeater_distance] * num_repeaters,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="static",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests_two_parties(
            hub1_nodes, hub2_nodes, num_requests, delta_func=delta_func
        )

        self.run_simulation(network_cfg, node_names=hub1_nodes + hub2_nodes)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_3_qia_random(self):
        """Test if all requests are completed in a QIA like network"""
        num_requests = 100
        num_nodes_per_hub = 4
        hub_distance = 434
        repeater_distance = 100
        num_repeaters = 2
        speed_of_light = 1e9
        delay = (hub_distance + repeater_distance) / speed_of_light * 1e9
        hub1_nodes = [f"node_hub1_{i}" for i in range(num_nodes_per_hub)]
        hub2_nodes = [f"node_hub2_{i}" for i in range(num_nodes_per_hub)]

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_nodes,
            hub1_node_distances=[hub_distance] * num_nodes_per_hub,
            hub2_node_names=hub2_nodes,
            hub2_node_distances=[hub_distance] * num_nodes_per_hub,
            repeater_chain_distances=[repeater_distance] * num_repeaters,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="static",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(
            hub1_nodes + hub2_nodes, num_requests, delta_func=delta_func
        )

        self.run_simulation(network_cfg, node_names=hub1_nodes + hub2_nodes)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_4_multiple_qubits_request(self):
        """Test that with multiple nodes with overlapping requests all requests will eventually be delivered"""
        num_requests = 20
        qubits_per_request = 5
        num_nodes = 5
        distance = 100
        speed_of_light = 1e9
        delay = 2 * distance / speed_of_light * 1e9
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="static",
            schedule_cfg=self.perfect_config(),
        )

        def delta_func():
            return random.randint(0, int(delay) * 2)

        self.requests = self.generate_requests(
            node_names, num_requests, delta_func=delta_func, qubits_per_request=qubits_per_request
        )

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_result_measurements_identical()

    def test_5_no_overlap(self):
        """Test if all requests are completed in the expected time frame"""
        num_requests = 40
        num_nodes = 2
        distance = 100
        switch_time = 200
        speed_of_light = 1e9
        request_completion_time = 2 * distance / speed_of_light * speed_of_light
        time_window = request_completion_time * 1.5
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="static",
            schedule_cfg=StaticScheduleConfig(switch_time=switch_time, time_window=time_window),
        )

        def delta_func():
            return time_window + switch_time

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_all_requests_finished_after_exact_delay(request_completion_time)
        self.check_result_measurements_identical()

    def test_6_two_nodes_request_overlap(self):
        """Test if all requests are completed in the expected time frame"""
        num_requests = 200
        num_nodes = 2
        distance = 100
        switch_time = 200
        speed_of_light = 1e9
        request_completion_time = 2 * distance / speed_of_light * speed_of_light
        time_window = request_completion_time * 1.5
        node_names = [f"node_{i}" for i in range(num_nodes)]

        network_cfg = create_metro_hub_network(
            node_names=node_names,
            node_distances=distance,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(speed_of_light=speed_of_light),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(speed_of_light=speed_of_light),
            schedule_typ="static",
            schedule_cfg=StaticScheduleConfig(switch_time=switch_time, time_window=time_window),
        )

        def delta_func():
            return 0.3 * request_completion_time

        self.requests = self.generate_requests(node_names, num_requests, delta_func=delta_func)

        self.run_simulation(network_cfg, node_names)

        self.check_all_requests_finished()
        self.check_all_requests_finished_sequentially(
            period=time_window + switch_time, delay=request_completion_time
        )
        self.check_result_measurements_identical()


if __name__ == "__main__":
    unittest.main()
