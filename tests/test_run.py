import unittest

import netsquid as ns

from netsquid_netbuilder.modules.clinks.default import DefaultCLinkConfig
from netsquid_netbuilder.modules.qlinks.perfect import PerfectQLinkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.network_generation import create_2_node_network
from netsquid_netbuilder.util.test_protocol_clink import (
    ClassicalMessageEventRegistration,
    ClassicalReceiverProtocol,
    ClassicalSenderProtocol,
)


def create_test_network(delay):
    return create_2_node_network(
        "perfect", PerfectQLinkConfig(), "default", DefaultCLinkConfig(delay=delay)
    )


class TestDefaultCLink(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        self.builder = get_default_builder()
        self.event_register = ClassicalMessageEventRegistration()

    def tearDown(self) -> None:
        pass

    def test_1_indefinite_run(self):
        delay = 2030.3
        send_time = 10
        network_cfg = create_test_network(delay)
        network = self.builder.build(network_cfg)

        messages = ["hi"]
        message_times = [send_time]

        alice = ClassicalSenderProtocol("Bob", self.event_register, messages, message_times)
        bob = ClassicalReceiverProtocol("Alice", self.event_register)

        run(network, {"Alice": alice, "Bob": bob}, end_time=None)

        self.assertAlmostEqual(ns.sim_time(), send_time + delay)

    def test_2_finite_end_time(self):
        delay = 200
        send_time = 10
        end_time = 100
        network_cfg = create_test_network(delay)
        network = self.builder.build(network_cfg)

        messages = ["hi"]
        message_times = [send_time]

        alice = ClassicalSenderProtocol("Bob", self.event_register, messages, message_times)
        bob = ClassicalReceiverProtocol("Alice", self.event_register)

        run(network, {"Alice": alice, "Bob": bob}, end_time=end_time)

        self.assertAlmostEqual(ns.sim_time(), end_time)
        self.assertEqual(len(self.event_register.received), 0)

    def test_3_magnitude(self):
        delay = 200
        send_time = 10
        end_time = 3.4
        magnitude = 1e3
        network_cfg = create_test_network(delay)
        network = self.builder.build(network_cfg)

        messages = ["hi"]
        message_times = [send_time]

        alice = ClassicalSenderProtocol("Bob", self.event_register, messages, message_times)
        bob = ClassicalReceiverProtocol("Alice", self.event_register)

        run(network, {"Alice": alice, "Bob": bob}, end_time=end_time, magnitude=magnitude)

        self.assertAlmostEqual(ns.sim_time(), end_time * magnitude)


if __name__ == "__main__":
    unittest.main()
