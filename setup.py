import configparser
import os
import subprocess
import sys

import setuptools
from setuptools import Command

# Get config parameters
config = configparser.ConfigParser()
config.read("setup.cfg")
pkg_name = config["metadata"]["name"]
pypi_server = config["netsquid"]["pypi-server"]
docs_server = "docs.netsquid.org"


class DeployCommand(Command):
    """Run command for uploading binary wheel files to NetSquid PyPi index."""

    description = "Deploy binary wheel files to NetSquid PyPi index."
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        print(
            f"Uploading binary snippet {pkg_name} wheels to {pypi_server} (requires authentication)"
        )
        if "NETSQUIDCI_USER" not in os.environ:
            sys.exit("ERROR: environment variable NETSQUIDCI_USER is not defined.")
        if "NETSQUIDCI_BASEDIR" not in os.environ:
            sys.exit("ERROR: environment variable NETSQUIDCI_BASEDIR is not defined.")
        # Upload wheel files
        pkg_dir = f"{os.environ['NETSQUIDCI_BASEDIR']}/pypi/{pkg_name}/"
        sftp_args = ["/usr/bin/sftp", f"{os.environ['NETSQUIDCI_USER']}@{pypi_server}"]
        if len([f for f in os.listdir("dist/") if f.endswith(".whl")]) > 0:
            subprocess.run(
                sftp_args, input="put -p dist/*.whl {}".format(pkg_dir).encode()
            ).check_returncode()
        else:
            sys.exit("ERROR: no wheel files in dist/ to upload.")


class DeployDocsCommand(Command):
    """Run command for uploading documentation files to NetSquid doc server.

    Requires authentication.
    Requires documentation to have been built.
    """

    description = f"Deploy documentation files to Snippet documentation server {docs_server}."
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        if "NETSQUIDCI_USER" not in os.environ:
            sys.exit("ERROR: environment variable NETSQUIDCI_USER is not defined.")
        if "NETSQUIDCI_BASEDIR" not in os.environ:
            sys.exit("ERROR: environment variable NETSQUIDCI_BASEDIR is not defined.")
        sftp_args = ["/usr/bin/sftp", f"{os.environ['NETSQUIDCI_USER']}@{docs_server}"]
        docs_dir = f"{os.environ['NETSQUIDCI_BASEDIR']}/docs/snippets/{pkg_name}/"
        # Create directory if it doesn't exist:
        subprocess.run(sftp_args, input=f"mkdir {docs_dir}".encode()).check_returncode()
        # Upload docs:
        subprocess.run(
            sftp_args, input=f"put -pR docs/build/html/. {docs_dir}".encode()
        ).check_returncode()


setuptools.setup(
    cmdclass={"deploy": DeployCommand, "deploy_docs": DeployDocsCommand},
)
