import netsquid as ns
from programs import AliceProgram, BobProgram

from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.network_generation import create_complete_graph_network_simplified

# Set the simulation to use density matrix formalism
ns.set_qstate_formalism(ns.QFormalism.DM)

# Create the network from the network configuration
builder = get_default_builder()

# Create programs for the processing nodes
num_epr_pairs = 20
alice_program = AliceProgram(num_epr_pairs)
bob_program = BobProgram(num_epr_pairs)
programs = {"Alice": alice_program, "Bob": bob_program}

# instead of loading the network config from YAML file, a utility method can be used to create it programmatically
cfg = create_complete_graph_network_simplified(
    node_names=["Alice", "Bob", "Charlie", "Dan", "Eve", "Frank"],
    qlink_noise=0.1,
    link_delay=1000,
    clink_delay=100,
)
network = builder.build(cfg)
sim_stats = run(network, programs)
