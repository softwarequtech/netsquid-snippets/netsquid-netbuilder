import netsquid as ns
from netsquid.components import INSTR_MEASURE, QuantumProgram
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService
from qlink_interface import ReqCreateAndKeep, ReqReceive, ResCreateAndKeep

from netsquid_netbuilder.run import Program


class AliceProgram(Program):
    PEER_NAME = "Bob"

    def __init__(self, num_epr: int):
        super().__init__()
        self._num_epr = num_epr

    def run(self):
        node = self.context.node
        egp = self.context.egp[self.PEER_NAME]
        qdevice = self.context.node.qdevice
        peer_id = self.context.node_id_mapping[self.PEER_NAME]

        # Wait 1 nanoseconds
        yield self.await_timer(1)

        for _ in range(self._num_epr):
            # create and submit a request for entanglement
            request = ReqCreateAndKeep(remote_node_id=peer_id, number=1)
            egp.put(request)

            # Await request completion
            yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
            response = egp.get_signal_result(label=ResCreateAndKeep.__name__, receiver=self)
            qubit_mem_pos = response.logical_qubit_id

            # measure the qubit
            program = QuantumProgram()
            program.apply(instruction=INSTR_MEASURE, qubit_indices=[qubit_mem_pos], output_key="m")
            qdevice.execute_program(program)
            yield self.await_program(qdevice)

            # Discard the qubit, inform EntanglementTrackerService of discard if it is being used
            qdevice.discard(qubit_mem_pos)
            if EntanglementTrackerService in node.driver.services:
                node.driver[EntanglementTrackerService].register_local_discard_mem_pos(
                    qubit_mem_pos
                )

        print(
            f"{ns.sim_time()} ns: Alice completed {self._num_epr} entanglement generations. "
            f"Average time: {ns.sim_time()/self._num_epr} ns"
        )


class BobProgram(Program):
    PEER_NAME = "Alice"

    def __init__(self, num_epr: int):
        super().__init__()
        self._num_epr = num_epr

    def run(self):
        node = self.context.node
        egp = self.context.egp[self.PEER_NAME]
        qdevice = self.context.node.qdevice
        peer_id = self.context.node_id_mapping[self.PEER_NAME]

        egp.put(ReqReceive(remote_node_id=peer_id))

        for _ in range(self._num_epr):
            # Await request completion
            yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
            response = egp.get_signal_result(label=ResCreateAndKeep.__name__, receiver=self)
            qubit_mem_pos = response.logical_qubit_id

            # measure the qubit
            program = QuantumProgram()
            program.apply(instruction=INSTR_MEASURE, qubit_indices=[qubit_mem_pos], output_key="m")
            qdevice.execute_program(program)
            yield self.await_program(qdevice)

            # Discard the qubit, inform EntanglementTrackerService of discard if it is being used
            qdevice.discard(qubit_mem_pos)
            if EntanglementTrackerService in node.driver.services:
                node.driver[EntanglementTrackerService].register_local_discard_mem_pos(
                    qubit_mem_pos
                )

        print(
            f"{ns.sim_time()} ns: Bob completed {self._num_epr} entanglement generations. "
            f"Average time: {ns.sim_time()/self._num_epr} ns"
        )
