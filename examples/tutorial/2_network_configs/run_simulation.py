import netsquid as ns
from programs import AliceProgram, BobProgram

from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run

# Set the simulation to use density matrix formalism
ns.set_qstate_formalism(ns.QFormalism.DM)

# Create the network from the network configuration
builder = get_default_builder()

# Create programs for the processing nodes
num_epr_pairs = 20
alice_program = AliceProgram(num_epr_pairs)
bob_program = BobProgram(num_epr_pairs)
programs = {"Alice": alice_program, "Bob": bob_program}

# The network configuration files
network_config_files = [
    "three_node_network.yaml",
    "metro_hub_network.yaml",
    "two_connected_metro_hub_network.yaml",
    "per_side_params.yaml",
]

# Create the network for each network config file and run the simulation with the same programs for each network
for network_config_file in network_config_files:
    print(f"\nRunning simulation with network config: {network_config_file}")
    ns.sim_reset()
    cfg = NetworkConfig.from_file(network_config_file)
    network = builder.build(cfg)
    sim_stats = run(network, programs)
