import netsquid as ns
import numpy as np
from matplotlib import pyplot
from protocols import AliceProtocol, BobProtocol

from netsquid_netbuilder.modules.qlinks.depolarise import DepolariseQLinkConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.network_generation import create_2_node_network

# The purpose of this example is to generate a plot of link fidelity vs epr fidelity.
# This example demonstrates how one can create the network configuration inside python and
# dynamically change the link fidelity in the network configuration in order to easily generate the data for the plot

ns.set_qstate_formalism(ns.QFormalism.DM)
builder = get_default_builder()


# Method run a simulation for a network config and return the fidelity of the EPR pair generated
def run_simulation(cfg: NetworkConfig) -> float:
    network = builder.build(cfg)

    alice = AliceProtocol()
    bob = BobProtocol()

    run(network, {"Alice": alice, "Bob": bob})

    qubit_alice = network.end_nodes["Alice"].qdevice.peek(0)[0]
    qubit_bob = network.end_nodes["Bob"].qdevice.peek(0)[0]

    reference_state = ns.qubits.ketstates.b00
    fidelity = ns.qubits.qubitapi.fidelity([qubit_alice, qubit_bob], reference_state)
    builder.protocol_controller.stop_all()
    return fidelity


# The various config objects can be loaded independently,
# in this case we have the parameters for DepolariseQLinkConfig in the config.yaml file
# Using the from_file() method, we create a DepolariseQLinkConfig object with the values as specified in the yaml file
link_config = DepolariseQLinkConfig.from_file("config.yaml")
link_fidelities = np.arange(0.5, 1, 0.1)
measured_fidelity = []
num_average = 10

# Run a for loop where the link fidelity is modified before each batch of simulation runs
for link_fidelity in link_fidelities:
    link_config.fidelity = link_fidelity
    config = create_2_node_network("depolarise", link_config)

    # Run the simulation multiple times that each returns the fidelity of the EPR pair, then average the result
    measure_list = [run_simulation(config) for _ in range(num_average)]
    measured_fidelity.append(np.average(measure_list))

# plot the measured epr fidelity as a function of link fidelity
pyplot.plot(link_fidelities, measured_fidelity)
pyplot.savefig("out.png")
