import netsquid as ns
from netsquid.components import INSTR_CNOT, INSTR_H, INSTR_INIT, INSTR_MEASURE, QuantumProgram
from qlink_interface import ReqCreateAndKeep, ReqReceive, ResCreateAndKeep

from netsquid_netbuilder.run import Program


class AliceProgram(Program):
    PEER_NAME = "Bob"

    def run(self):
        # get classical socket to peer
        socket = self.context.sockets[self.PEER_NAME]
        # get entanglement generation protocol for peer
        egp = self.context.egp[self.PEER_NAME]
        # get quantum device on the node
        qdevice = self.context.node.qdevice
        # get the node ID of the peer
        peer_id = self.context.node_id_mapping[self.PEER_NAME]

        # Send a classical message to bob
        message = "Hello from Alice"
        socket.send(message)
        print(f"{ns.sim_time()} ns: Alice sends: {message}")

        # Wait 1000 nanoseconds
        yield self.await_timer(1000)
        print(f"{ns.sim_time()} ns: Alice finishes waiting")

        # create and submit a request for entanglement
        request = ReqCreateAndKeep(remote_node_id=peer_id, number=1)
        egp.put(request)
        print(f"{ns.sim_time()} ns: Alice submits entanglement request.")

        # Await request completion
        yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
        response = egp.get_signal_result(label=ResCreateAndKeep.__name__, receiver=self)
        qubit_mem_pos = response.logical_qubit_id
        print(f"{ns.sim_time()} ns: Alice completes entanglement generation")

        # measure the qubit
        program = QuantumProgram()
        program.apply(instruction=INSTR_MEASURE, qubit_indices=[qubit_mem_pos], output_key="m")
        qdevice.execute_program(program)
        yield self.await_program(qdevice)
        print(f"{ns.sim_time()} ns: Alice measures the qubit: {program.output['m'][0]}")

        # Create local qubits and perform a program
        program = QuantumProgram()
        program.apply(instruction=INSTR_INIT, qubit_indices=[0])
        program.apply(instruction=INSTR_INIT, qubit_indices=[1])
        program.apply(instruction=INSTR_H, qubit_indices=[0])
        program.apply(instruction=INSTR_CNOT, qubit_indices=[0, 1])
        program.apply(instruction=INSTR_MEASURE, qubit_indices=[0], output_key="m0")
        program.apply(instruction=INSTR_MEASURE, qubit_indices=[1], output_key="m1")

        qdevice.execute_program(program)
        yield self.await_program(qdevice)
        print(
            f"{ns.sim_time()} ns: Alice performs local quantum operations and measures qubits:"
            f" m0={program.output['m0'][0]}, m1={program.output['m1'][0]}"
        )


class BobProgram(Program):
    PEER_NAME = "Alice"

    def run(self):
        # get classical socket to peer
        socket = self.context.sockets[self.PEER_NAME]
        # get entanglement generation protocol for peer
        egp = self.context.egp[self.PEER_NAME]
        # get quantum device on the node
        qdevice = self.context.node.qdevice
        # get the node ID of the peer
        peer_id = self.context.node_id_mapping[self.PEER_NAME]

        message = yield from socket.recv()
        print(f"{ns.sim_time()} ns: Bob receives: {message}")

        egp.put(ReqReceive(remote_node_id=peer_id))
        print(f"{ns.sim_time()} ns: Bob will now accept entanglement requests from Alice")

        # Wait for a signal from the EGP.
        yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
        response = egp.get_signal_result(label=ResCreateAndKeep.__name__, receiver=self)
        qubit_mem_pos = response.logical_qubit_id
        print(f"{ns.sim_time()} ns: Bob completes entanglement generation")

        # measure the qubit
        program = QuantumProgram()
        program.apply(instruction=INSTR_MEASURE, qubit_indices=[qubit_mem_pos], output_key="m")
        qdevice.execute_program(program)
        yield self.await_program(qdevice)
        print(f"{ns.sim_time()} ns: Bob measures the qubit: {program.output['m'][0]}")
