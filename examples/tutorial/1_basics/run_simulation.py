import netsquid as ns
from programs import AliceProgram, BobProgram

from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run

# Set the simulation to use density matrix formalism
ns.set_qstate_formalism(ns.QFormalism.DM)
# Load network config from the config.yaml file
cfg = NetworkConfig.from_file("config.yaml")

# Create the network from the network configuration
builder = get_default_builder()
network = builder.build(cfg)

# Create programs for the processing nodes
alice_program = AliceProgram()
bob_program = BobProgram()

# Specify that the Alice node will run the alice program instance, similar for Bob
programs = {"Alice": alice_program, "Bob": bob_program}

# Run the simulation
sim_stats = run(network, programs)
