from typing import Dict, Tuple

from netsquid_magic.qlink import IQLink
from qlink_interface.interface import ResCreate

from netsquid_netbuilder.modules.scheduler.fifo import FIFOScheduleConfig, FIFOScheduleProtocol
from netsquid_netbuilder.modules.scheduler.interface import IScheduleBuilder
from netsquid_netbuilder.network import Network

# This file creates a trivial extensions of the FIFO scheduler.
# This extension prints a configurable message to console each time an entanglement
# generation completes and the scheduler is informed of this
# The purpose of this scheduler is only to demonstrate how to include a new model in the builder


class ExampleNewScheduleConfig(FIFOScheduleConfig):
    result_print_msg: str


class ExampleNewScheduleProtocol(FIFOScheduleProtocol):
    """Part of example showing how to create a new protocol"""

    def register_result(self, node_id: int, res: ResCreate):
        assert isinstance(self.params, ExampleNewScheduleConfig)
        print(f"{self.params.result_print_msg} {res.create_id}")
        super().register_result(node_id, res)


class ExampleNewScheduleBuilder(IScheduleBuilder):
    @classmethod
    def build(
        cls,
        name: str,
        network: Network,
        qlinks: Dict[Tuple[str, str], IQLink],
        schedule_config: FIFOScheduleConfig,
    ) -> ExampleNewScheduleProtocol:

        if isinstance(schedule_config, dict):
            schedule_config = ExampleNewScheduleConfig(**schedule_config)

        scheduler = ExampleNewScheduleProtocol(
            name, schedule_config, qlinks, network.node_name_id_mapping
        )
        return scheduler
