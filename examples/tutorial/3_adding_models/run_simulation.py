import sys

import programs
from example_new_scheduler import ExampleNewScheduleBuilder, ExampleNewScheduleConfig
from programs import AliceProgram, BobProgram

from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging

# --test_run is a flag included in test runs and will prevent the console being flooded with log messages
if "--test_run" not in sys.argv:
    # setup_default_console_logging is a convenience method is useful for showing the timeline of simulation events
    # in the console. Its API documentation provides more details on what it does.
    setup_default_console_logging(magnitude="ms", precision=3, additional_loggers=[programs.logger])


cfg = NetworkConfig.from_file("config.yaml")

# In order to models not included in the default netsquid-netbuilder, the new models must be registered under
# a model_name, the builder class and the config class.
builder = get_default_builder()
builder.register(
    model_name="new_scheduler", builder=ExampleNewScheduleBuilder, config=ExampleNewScheduleConfig
)
network = builder.build(cfg)

# Set up the protocols
num_epr_pairs = 20
alice_program = AliceProgram(peer="Bob", num_epr_pairs=num_epr_pairs)
bob_program = BobProgram(peer="Alice", num_epr_pairs=num_epr_pairs)
programs_dict = {"Alice": alice_program, "Bob": bob_program}

# Run the simulation
sim_stats = run(network, programs_dict)
