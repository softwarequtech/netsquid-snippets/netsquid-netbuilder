import sys

import netsquid as ns

from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging
from netsquid_netbuilder.util.test_protocol_qlink import (
    CreateAndKeepEventRegistration,
    CreateAndKeepReceiveProtocol,
    CreateAndKeepSenderProtocol,
)

ns.set_qstate_formalism(ns.QFormalism.DM)

if "--test_run" not in sys.argv:
    setup_default_console_logging(magnitude="ms")


cfg = NetworkConfig.from_file("config_repeater_double_click.yaml")
builder = get_default_builder()
network = builder.build(cfg)

# Event registration to register information of generated EPR pairs
event_reg = CreateAndKeepEventRegistration()

sim_stats = run(
    network,
    {
        "h1n0": CreateAndKeepReceiveProtocol("h2n0", event_reg=event_reg, n_epr=50),
        "h2n0": CreateAndKeepSenderProtocol("h1n0", event_reg=event_reg, n_epr=50),
    },
)

assert len(event_reg.received_ck) == 100
