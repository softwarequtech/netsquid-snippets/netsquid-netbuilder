import sys

import netsquid as ns

from netsquid_netbuilder.modules.clinks.default import DefaultCLinkConfig
from netsquid_netbuilder.modules.long_distance_interface.depolarise import (
    DepolariseLongDistanceInterfaceConfig,
)
from netsquid_netbuilder.modules.qdevices.generic import GenericQDeviceConfig
from netsquid_netbuilder.modules.qlinks.depolarise import DepolariseQLinkConfig
from netsquid_netbuilder.modules.qrep_chain_control.swap_asap.swap_asap_builder import (
    SwapASAPConfig,
)
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.network_generation import create_two_connected_metro_hubs_network
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging
from netsquid_netbuilder.util.test_protocol_qlink import (
    CreateAndKeepEventRegistration,
    CreateAndKeepReceiveProtocol,
    CreateAndKeepSenderProtocol,
)

# Example showing how to create a repeater chain using the create_two_connected_metro_hubs_network method

if "--test_run" not in sys.argv:
    setup_default_console_logging(magnitude="ms")

ns.set_qstate_formalism(ns.QFormalism.DM)

# Create configurations for components in network first
qdevice_cfg = GenericQDeviceConfig(
    T1=0,
    T2=0,
    two_qubit_gate_depolar_prob=0,
    init_time=1,
    single_qubit_gate_time=1,
    two_qubit_gate_time=1,
    measure_time=1,
)

qlink_cfg = DepolariseQLinkConfig(fidelity=1, prob_success=0.1)

long_distance_interface_cfg = DepolariseLongDistanceInterfaceConfig(prob_max_mixed=0.2, p_loss=0.5)

# Create network configuration
# Total distance between processing nodes (in different hubs) is 70 km
cfg = create_two_connected_metro_hubs_network(
    hub1_node_names=["hub1_node_0", "hub1_node_1", "hub1_node_2"],
    hub2_node_names=["hub2_node_0", "hub2_node_1", "hub2_node_2"],
    hub1_node_distances=[5, 5, 3],
    hub2_node_distances=[5, 5, 5],
    repeater_chain_distances=[10, 20, 20, 10],
    qlink_typ="depolarise",
    qlink_cfg=qlink_cfg,
    clink_typ="default",
    clink_cfg=DefaultCLinkConfig(),
    qdevice_typ="generic",
    qdevice_cfg=qdevice_cfg,
    long_distance_interface_typ="depolarise",
    long_distance_interface_cfg=long_distance_interface_cfg,
    qrep_chain_control_typ="swapASAP",
    qrep_chain_control_cfg=SwapASAPConfig(parallel_link_generation=False),
)

builder = get_default_builder()
network = builder.build(cfg)

# Event registration to register information of generated EPR pairs
event_reg = CreateAndKeepEventRegistration()

sim_stats = run(
    network,
    {
        "hub1_node_0": CreateAndKeepReceiveProtocol("hub2_node_0", event_reg=event_reg, n_epr=50),
        "hub2_node_0": CreateAndKeepSenderProtocol("hub1_node_0", event_reg=event_reg, n_epr=50),
    },
)

assert len(event_reg.received_ck) == 100
