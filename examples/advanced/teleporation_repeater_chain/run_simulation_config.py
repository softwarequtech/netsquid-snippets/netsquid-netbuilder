import sys

import netsquid as ns
from protocols import TeleportationReceiverProtocol, TeleportationSenderProtocol

from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging

ns.set_qstate_formalism(ns.QFormalism.DM)

if "--test_run" not in sys.argv:
    setup_default_console_logging(magnitude="ms")

builder = get_default_builder()

cfg = NetworkConfig.from_file("config_repeater.yaml")
network = builder.build(cfg)


sim_stats = run(
    network,
    {
        "h1n0": TeleportationSenderProtocol(peer_name="h2n0"),
        "h2n0": TeleportationReceiverProtocol(peer_name="h1n0"),
    },
)
print(sim_stats)
