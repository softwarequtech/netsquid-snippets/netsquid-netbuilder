import sys

import protocols
from protocols import AliceProtocol, BobProtocol

from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.data_collectors import collect_schedule_events
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging

if "--test_run" not in sys.argv:
    setup_default_console_logging(
        magnitude="μs", precision=2, additional_loggers=[protocols.logger]
    )

cfg = NetworkConfig.from_file("config_heralded_fifo.yaml")
num_epr_pairs = 10

builder = get_default_builder()
network = builder.build(cfg)

alice_A = AliceProtocol(peer="Node2", num_epr_pairs=num_epr_pairs)
bob_A = BobProtocol(peer="Node1", num_epr_pairs=num_epr_pairs)
alice_B = AliceProtocol(peer="Node4", num_epr_pairs=num_epr_pairs)
bob_B = BobProtocol(peer="Node3", num_epr_pairs=num_epr_pairs)
metro_hub = list(network.hubs.values())[0]

# use the collect_schedule_events method to register events, such as open and close, by this scheduler
schedule_events = collect_schedule_events(metro_hub.scheduler)

sim_stats = run(network, {"Node1": alice_A, "Node2": bob_A, "Node3": alice_B, "Node4": bob_B})

for key, val in schedule_events.items():
    print(key)
    print(val)
print(sim_stats)
